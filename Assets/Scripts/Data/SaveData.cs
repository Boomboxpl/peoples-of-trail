﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

/// <summary>
/// Dane zapisu gry.
/// </summary>
[Serializable]
public class SaveData
{
    /// <summary>
    /// Identyfikator.
    /// </summary>
    public int id;
    /// <summary>
    /// Nazwa gracza.
    /// </summary>
    public string name;
    /// <summary>
    /// Data utworzenia.
    /// </summary>
    public DateTime creationDate;
    /// <summary>
    /// Data ostatniego zapisu.
    /// </summary>
    public DateTime lastSaveDate;
    /// <summary>
    /// Dane gracza.
    /// </summary>
    public PlayerData playerData;
    /// <summary>
    /// Lista poziomów. Ostatni poziom na liście jest aktualnym poziomem.
    /// </summary>
    public List<MapLevel> levels;
    /// <summary>
    /// Zapis z punktu kontrolnego.
    /// </summary>
    public SaveData checkpointSave;

    /// <summary>
    /// Utworzenie nowego zapisu.
    /// </summary>
    /// <param name="name">Nazwa gracza.</param>
    /// <param name="skillProviderManager">Zarządca aktywnych konfiguracji umiejętności.</param>
    /// <returns>Utworzony gracz.</returns>
    public static async UniTask<SaveData> CreateNewSave(string name, SkillProviderManager skillProviderManager)
    {
        SaveData newSave = new SaveData
        {
            id = await PlayerSaver.GetUnusedId(),
            name = name,
            creationDate = DateTime.Now,
            lastSaveDate = DateTime.Now,
            playerData = PlayerData.GetDefaultNewPlayer(skillProviderManager),
            levels = new List<MapLevel>()
        };
        await newSave.playerData.RefreshPassiveEffect();

        await PlayerSaver.Save(newSave);

        return newSave;
    }

    /// <summary>
    /// Ustawienie zapisu jako zapisu w punkcie kontrolnym.
    /// </summary>
    public void SetAsCheckPoint()
    {
        checkpointSave = null;
        checkpointSave = DeepClone(this);
    }

    /// <summary>
    /// Tworzenie kopii głębokiej zapisu.
    /// Tworzona jest kopia wszystkich obiektów i elementów kolekcji składających się na zapis.
    /// </summary>
    /// <param name="save">Zapis do skopiowania</param>
    /// <returns>Kopiazapisu.</returns>
    public static SaveData DeepClone(SaveData save)
    {
        using (var ms = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            formatter.Serialize(ms, save);
            ms.Position = 0;

            return (SaveData)formatter.Deserialize(ms);
        }
    }
}
