﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Dane o szacunku grupy do dowódcy grupy.
/// </summary>
[Serializable]
public class RespectData
{
    /// <summary>
    /// Delegat wywoływany po zmianie wartości właściwości obiektu.
    /// </summary>
    [NonSerialized] public Action stateChanged;

    /// <summary>
    /// Baza funkcji wykładniczej użytej podczas obliczania punktów potrzebnych do osiągnięcia następnego poziomu,
    /// </summary>
    const float powerBase = 1.1f;
    /// <summary>
    /// Liczba punktów potrzebna do uzyskania poziomu. Skalowana z każdym kolejnym poziomem.
    /// </summary>
    const int pointsNeededPerLevel = 2000;
    /// <summary>
    /// Maksymalny poziom.
    /// </summary>
    const int maxLevel = 20;

    /// <summary>
    /// Aktualny poziom.
    /// </summary>
    int repsectLevel;
    /// <summary>
    /// Aktualny poziom.
    /// </summary>
    public int RespectLevel {
        get => repsectLevel;
        set
        {
            repsectLevel = value;
            stateChanged?.Invoke();
        }
    }

    /// <summary>
    /// Liczba wolnych punktów umiejętności.
    /// </summary>
    int freeSkillPoints;

    /// <summary>
    /// Liczba wolnych punktów umiejętności.
    /// </summary>
    public int FreeSkillPoints
    {
        get => freeSkillPoints;
        set
        {
            freeSkillPoints = value;
            stateChanged?.Invoke();
        }
    }

    /// <summary>
    /// Aktualna liczba punktów szacunku.
    /// </summary>
    int currentPoints;

    /// <summary>
    /// Aktualna liczba punktów szacunku.
    /// </summary>
    public int CurrentPoints
    {
        get => currentPoints;
        set
        {
            currentPoints = value;
            int pointsForNextLevel = PointsForNextLevel;
            if(currentPoints >= pointsForNextLevel)
            {
                int levelGain = currentPoints / pointsForNextLevel;
                if(RespectLevel + levelGain > maxLevel)
                {
                    levelGain = maxLevel - RespectLevel;
                }
                RespectLevel += levelGain;
                FreeSkillPoints += levelGain;
                currentPoints %= pointsForNextLevel;
            }
            stateChanged?.Invoke();
        }
    }

    /// <summary>
    /// Konstruktor.
    /// </summary>
    public RespectData()
    {
        RespectLevel = 0;
        FreeSkillPoints = 0;
        currentPoints = 0;
    }

    /// <summary>
    /// Informacja, czy został osiągnięty maksymalny poziom.
    /// </summary>
    public bool isMaxLevel => RespectLevel >= maxLevel;

    /// <summary>
    /// Liczba punktów potrzebna do osiągnięcia następnego poziomu.
    /// </summary>
    /// <returns>Liczba punktów uzyskiwana z wzoru powerBase ^ x * pointsNeededPerLevel, gdzie x to aktualny poziom.</returns>
    public int PointsForNextLevel => !isMaxLevel ? (int)(Mathf.Pow(powerBase, RespectLevel) * pointsNeededPerLevel) : int.MaxValue;
}
