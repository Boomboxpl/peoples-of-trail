﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.AddressableAssets;
using System.Collections.ObjectModel;
using Cysharp.Threading.Tasks;


/// <summary>
/// Dane gracza.
/// </summary>
[Serializable]
public class PlayerData
{
    /// <summary>
    /// Podstawowa liczba ludzi.
    /// </summary>
    const int basePeople = 10;
    /// <summary>
    /// Liczba ludzi za każdy poziom dowódcy.
    /// </summary>
    const int peoplePerLevel = 10;
    /// <summary>
    /// Liczba zasobów ujemnych potrzebnych do utraty jednego człowieka.
    /// </summary>
    const int resourcesShortagePerPerson = 10;

    /// <summary>
    /// Bierny efekt;
    /// </summary>
    PassiveEffectData passiveEffect;
    /// <summary>
    /// Liczba morali.
    /// </summary>
    float morales;
    /// <summary>
    /// Liczba zasobów.
    /// </summary>
    int resources;
    /// <summary>
    /// Liczba ludności.
    /// </summary>
    int peoples;
    /// <summary>
    /// Pozycja na mapie względem osi X.
    /// </summary>
    int mapPositionX;
    /// <summary>
    /// Pozycja na mapie względem osi Y.
    /// </summary>
    int mapPositionY;
    /// <summary>
    /// Generator liczb pseudolosowych, używany podczas zdarzeń.
    /// </summary>
    System.Random eventDiceRandom;
    /// <summary>
    /// Lista umiejętności bohatera.
    /// </summary>
    List<HeroSkill> heroSkills;
    /// <summary>
    /// Dane o poziomie rozwoju postaci.
    /// </summary>
    RespectData respect;

    /// <summary>
    /// Utworzenie nowego gracza.
    /// </summary>
    /// <returns>Utworzony gracz.</returns>
    public static PlayerData GetDefaultNewPlayer(SkillProviderManager skillProviderManager)
    {
        ReadOnlyCollection<SkillProviderReference> skillReferences = skillProviderManager.Skills;
        List<HeroSkill> newHeroSkills = new List<HeroSkill>(skillReferences.Count);
        foreach (SkillProviderReference skillReference in skillReferences)
        {
            newHeroSkills.Add(new HeroSkill(skillReference));
        }

        PassiveEffectData startPassiveEffect = new PassiveEffectData();
        startPassiveEffect.Reset();

        return new PlayerData
        {
            passiveEffect = startPassiveEffect,
            Morales = 1f,
            Resources = 100,
            Peoples = int.MaxValue,
            MapPosition = Vector2Int.zero,
            eventDiceRandom = new System.Random((int)DateTime.Now.Ticks),
            heroSkills = newHeroSkills,
            respect = new RespectData()
        };
    }

    /// <summary>
    /// Liczba morali.
    /// </summary>
    public float Morales
    {
        get => morales;
        set
        {
            morales += passiveEffect.ApplyMorales(value - morales);
            morales = Mathf.Clamp01(morales);
            Peoples = peoples;
        }
    }
    /// <summary>
    /// Liczba zasobów.
    /// </summary>
    public int Resources { get => resources; set => resources += passiveEffect.ApplyResources(value - resources); }
    /// <summary>
    /// Liczba ludności.
    /// </summary>
    public int Peoples { get => peoples; set => peoples = Mathf.Clamp(value, 0, MaxPeoples); }
    /// <summary>
    /// Maksymalna liczba ludności.
    /// </summary>
    public int MaxPeoples => basePeople + (int)((respect?.RespectLevel ?? 0) * peoplePerLevel * Morales);
    /// <summary>
    /// Pozycja na mapie.
    /// </summary>
    public Vector2Int MapPosition
    {
        get => new Vector2Int(mapPositionX, mapPositionY);
        set
        {
            mapPositionX = value.x;
            mapPositionY = value.y;
        }
    }
    /// <summary>
    /// Generator liczb pseudolosowych, używany podczas zdarzeń.
    /// </summary>
    public System.Random EventDiceRandom => eventDiceRandom;
    /// <summary>
    /// Lista umiejętności bohatera.
    /// </summary>
    public List<HeroSkill> HeroSkills => heroSkills;
    /// <summary>
    /// Dane o poziomie rozwoju postaci.
    /// </summary>
    public RespectData Respect => respect;
    /// <summary>
    /// Bierny efekt.
    /// </summary>
    public PassiveEffectData PassiveEffect => passiveEffect;

    /// <summary>
    /// Odświeżenie wartości biernego efektu używanego podczas obliczania zmiany atrybutów gracza.
    /// </summary>
    /// <returns>Zadanie asynchroniczne,</returns>
    public async UniTask RefreshPassiveEffect() => passiveEffect = await PassiveEffectData.CalculateFromSkills(this);

    /// <summary>
    /// Pobranie poziomu umiejętności.
    /// </summary>
    /// <param name="skillToFind">Konfiguracja umiejętności do znalezienia.</param>
    /// <returns>Zadanie asynchroniczne zwracające poziom szukanej umiejętności lub -1 w przypadku braku możliwości znalezienia umiejętności.</returns>
    public async UniTask<int> GetHeroSkillLevel(SkillProvider skillToFind)
    {
        foreach (HeroSkill heroSkill in heroSkills)
        {
            if (await heroSkill.GetSkill() == skillToFind)
            {
                return heroSkill.level;
            }
        }
        return -1;
    }

    /// <summary>
    /// Usunięcie odpowiedniej liczby ludzi proporcjonalnie do liczby ujemnych zasobów.
    /// </summary>
    public void RemovePeopleFromResourcesShortage()
    {
        int peopleLost = (-resources) / resourcesShortagePerPerson;
        if(peopleLost > 0)
        {
            Peoples -= peopleLost;
        }
    }
}
