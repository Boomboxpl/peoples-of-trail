﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using Cysharp.Threading.Tasks;

/// <summary>
/// Umiejętność dowódcy grupy. Określa poziom przypisanej umiejętności.
/// </summary>
[Serializable]
public class HeroSkill
{
    /// <summary>
    /// Poziom umiejętności.
    /// </summary>
    public int level;
    /// <summary>
    /// Przypisana konfiguracja umiejętności.
    /// </summary>
    SerializableAsset<SkillProvider> skillProvider;

    /// <summary>
    /// Pobranie konfiguracji umiejętności.
    /// </summary>
    /// <returns>Zadanie asynchroniczne zwracające umiejętność.</returns>
    public async UniTask<SkillProvider> GetSkill()
    {
        SkillProvider result = await skillProvider.GetAsync();
        return result;
    }

    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="skillReference">Odwołanie do umiejętności.</param>
    public HeroSkill(SkillProviderReference skillReference)
    {
        level = 0;
        skillProvider = new SerializableAsset<SkillProvider>();
        skillProvider.Set(skillReference);
    }
}
