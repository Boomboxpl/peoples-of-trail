﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Cysharp.Threading.Tasks;

/// <summary>
/// Bierny efekt umiejętności.
/// </summary>
[Serializable]
public struct PassiveEffectData
{
    /// <summary>
    /// Zwiększenie przyrostu zasobów.
    /// </summary>
    public float resourcesGatherIncrease;
    /// <summary>
    /// Zmniejszenie strat zasobów.
    /// </summary>
    public float resourcesLoseDecrease;
    /// <summary>
    /// Zwiększenie przyrostu morali.
    /// </summary>
    public float moralesGainIncrease;
    /// <summary>
    /// Zmniejszenie strat morali.
    /// </summary>
    public float moralesLoseDecrease;

    /// <summary>
    /// Operator sumowania efektów.
    /// </summary>
    /// <param name="first">Pierwszy efekt.</param>
    /// <param name="second">Drgui efekt.</param>
    /// <returns>Efekt, którego parametry są sumą pierwszego i drugiego.</returns>
    public static PassiveEffectData operator +(PassiveEffectData first, PassiveEffectData second)
    {
        first.resourcesGatherIncrease += second.resourcesGatherIncrease;
        first.resourcesLoseDecrease += second.resourcesLoseDecrease;
        first.moralesGainIncrease += second.moralesGainIncrease;
        first.moralesLoseDecrease += second.moralesLoseDecrease;
        return first;
    }

    /// <summary>
    /// Obliczenie efektu bedącego sumą każdego z efektów umiejętności dowódcy grupy.
    /// </summary>
    /// <param name="playerData">Dane gracza.</param>
    /// <returns>Bierny efekt wykorzystywany podczas zmian atrybutów.</returns>
    public static async UniTask<PassiveEffectData> CalculateFromSkills(PlayerData playerData)
    {
        PassiveEffectData result = new PassiveEffectData();
        result.Reset();
        foreach (HeroSkill heroSkill in playerData.HeroSkills)
        {
            SkillProvider skillProvider = await heroSkill.GetSkill();
            PassiveEffectData effect = skillProvider.GetPassiveEffectData(heroSkill.level, playerData);
            result += effect;
        }
        return result;
    }

    /// <summary>
    /// Przywrócenie wartości domyślnych parametrów.
    /// </summary>
    public void Reset()
    {
        resourcesGatherIncrease = 1f;
        resourcesLoseDecrease = 1f;
        moralesGainIncrease = 1f;
        moralesLoseDecrease = 1f;
    }

    /// <summary>
    /// Skalowanie zmiany ilości zasobów. Parametr skalujący jest zależny od znaku zmiany.
    /// </summary>
    /// <param name="delta">Zmiana ilości zasobów.</param>
    /// <returns>Efektywna zmiana ilości zasobów.</returns>
    public int ApplyResources(int delta)
    {
        if (delta > 0)
        {
            return Mathf.CeilToInt(delta * resourcesGatherIncrease);
        }
        else
        {
            return Mathf.CeilToInt(delta * (1f / resourcesLoseDecrease));
        }
    }

    /// <summary>
    /// Skalowanie zmiany poziomu morali. Parametr skalujący jest zależny od znaku zmiany.
    /// </summary>
    /// <param name="delta">Zmiana poziomu morali.</param>
    /// <returns>Efektywna zmiana poziomu morali.</returns>
    public float ApplyMorales(float delta)
    {
        if (delta > 0)
        {
            return delta * moralesGainIncrease;
        }
        else
        {
            return delta * (1f / moralesLoseDecrease);
        }
    }
}
