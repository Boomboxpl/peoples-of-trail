﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Przycisk wywołujący dźwięk wciśnięcia.
/// </summary>
[RequireComponent(typeof(Button))]
public class SoundButton : MonoBehaviour
{
    /// <summary>
    /// Przycisk.
    /// </summary>
    Button button;

    /// <summary>
    /// Inicjalizacja. Pobranie przycisku spośród komponentów przypisanych do obiektu na scenie. 
    /// </summary>
    void Awake()
    {
        button = GetComponent<Button>();
    }

    /// <summary>
    /// Obsługa aktywacji. Ustawienie reakcji na wciśnięcie.
    /// </summary>
    void OnEnable()
    {
        button.onClick.AddListener(Play);
    }

    /// <summary>
    /// Obsługa dezaktywacji. Usunięcie reakcji na wciśnięcie.
    /// </summary>
    void OnDisable()
    {
        button.onClick.RemoveListener(Play);
    }

    /// <summary>
    /// Odtworzenie dźwięku wciśnięcia.
    /// </summary>
    void Play()
    {
        SoundManager.Instance.PlayButtonSound();
    }
}
