﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System.Linq;

/// <summary>
/// Zarządca dźwięku.
/// </summary>
public class SoundManager : MonoBehaviour
{
    /// <summary>
    /// Klucz do ustawiania wartości głośności muzyki.
    /// </summary>
    const string musicVolume = "MUSIC_V";
    /// <summary>
    /// Klucz do ustawiania wartości głośności efektów dźwiękowych.
    /// </summary>
    const string sfxVolume = "SFX_V";

    /// <summary>
    /// Dźwięk naciśnięcia przycisku.
    /// </summary>
    public AudioClip buttonClick;

    /// <summary>
    /// Lista ścieżek dźwiękowych z muzyką.
    /// </summary>
    public List<AudioClip> musicClips;

    /// <summary>
    /// Mikser dźwięku.
    /// </summary>
    public AudioMixer audioMixer;

    /// <summary>
    /// Głośność efektów dźwiękowych.
    /// </summary>
    public float SfxVolume
    {
        get => PlayerPrefs.GetFloat(sfxVolume, 1f);
        set
        {
            audioMixer.SetFloat(sfxVolume, ScaleVolume(value));
            PlayerPrefs.SetFloat(sfxVolume, value);
        }
    }

    /// <summary>
    /// Głośność muzyki.
    /// </summary>
    public float MusicVolume
    {
        get => PlayerPrefs.GetFloat(musicVolume, 1f);
        set
        {
            float debug = ScaleVolume(value);
            audioMixer.SetFloat(musicVolume, ScaleVolume(value));
            PlayerPrefs.SetFloat(musicVolume, value);
        }
    }

    /// <summary>
    /// Instancja wzorca singleton.
    /// </summary>
    public static SoundManager Instance
    {
        get
        {
            if(instance == null)
            {
                instance = FindObjectOfType<SoundManager>();
                DontDestroyOnLoad(instance.gameObject);
            }
            return instance;
        }
    }

    /// <summary>
    /// Instancja wzorca singleton.
    /// </summary>
    static SoundManager instance;

    /// <summary>
    /// Źródło dźwięku efektów dźwiękowych.
    /// </summary>
    [SerializeField] AudioSource musicAudioSource;
    /// <summary>
    /// Źródło dźwięku muzyki.
    /// </summary>
    [SerializeField] AudioSource sfxAudioSource;

    /// <summary>
    /// Inicjalizacja. Obsługa wzorca singleton. Rozpoczęcie odgrywania muzyki.
    /// </summary>
    void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        if(musicClips.Count > 0)
        {
            StartCoroutine(MusicRoutine());
        }
    }

    /// <summary>
    /// Inicjalizacja. Ustawienie watości dźwięku.
    /// </summary>
    void Start()
    {
        SfxVolume = SfxVolume;
        MusicVolume = MusicVolume;
    }

    /// <summary>
    /// Odtworzenie dźwięku wciśnięcia przycisku.
    /// </summary>
    public void PlayButtonSound() => PlaySound(buttonClick);

    /// <summary>
    /// Odtworzenie pojedynczego dźwięku.
    /// </summary>
    /// <param name="sound">Ścieżka dźwiękowa.</param>
    public void PlaySound(AudioClip sound)
    {
        if(sound != null)
        {
            sfxAudioSource.Stop();
            sfxAudioSource.clip = sound;
            sfxAudioSource.Play();
        }
    }

    /// <summary>
    /// Skalowanie dźwięku z przedziału (0,1) na przedział głośności od braku zauważalnego dźwięku do pełnej głośności. 
    /// </summary>
    /// <param name="soundVolume">Głośność w przedziale (0,1).</param>
    /// <returns>Głośność w decybelach.</returns>
    float ScaleVolume(float soundVolume)
    {
        return Mathf.Log10(Mathf.Clamp(soundVolume, 0.0001f, 1f)) * 20f;
    }

    /// <summary>
    /// Korutyna odtwarzająca muzykę w losowej kolejności.
    /// </summary>
    /// <returns>Korutyna.</returns>
    IEnumerator MusicRoutine()
    {
        while (true)
        {
            IOrderedEnumerable<AudioClip> shuffled = musicClips.OrderBy(clip => Random.Range(int.MinValue, int.MaxValue));
            foreach(AudioClip clip in shuffled)
            {
                musicAudioSource.clip = clip;
                musicAudioSource.Play();
                yield return new WaitForSeconds(clip.length);
                yield return new WaitWhile(() => musicAudioSource.isPlaying);
            }
            yield return null;
        }
    }
}
