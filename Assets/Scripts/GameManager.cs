﻿using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Główny zarządca gry.
/// </summary>
public class GameManager : MonoBehaviour
{
    /// <summary>
    /// Informacja, czy w ostatniej turze dotarto do punktu kontrolnego.
    /// </summary>
    [HideInInspector] public bool reachedCheckpoint;
    /// <summary>
    /// Odwołanie do obiektu gracza.
    /// </summary>
    public Player player;
    /// <summary>
    /// Zarządca mapy.
    /// </summary>
    public MapManager mapManager;
    /// <summary>
    /// Zarządca interfejsu użytkownika.
    /// </summary>
    public UIManager uiManager;
    /// <summary>
    /// Zarządca ruchu kamery.
    /// </summary>
    public CameraMoveManager cameraMoveManager;

    /// <summary>
    /// Instancja wzorca Singleton.
    /// </summary>
    public static GameManager Instance { get { return _instance; } }
    /// <summary>
    /// Instancja wzorca Singleton.
    /// </summary>
    static GameManager _instance;
    /// <summary>
    /// Aktualny zapis gry.
    /// </summary>
    SaveData currentSave;

    /// <summary>
    /// Inicjalizacja. Obsługa wzorca singleton. Ustawienie reakcji na zakończenie rozwiązywania zdarzeń losowych.
    /// </summary>
    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
        reachedCheckpoint = false;
        uiManager.eventSolver.eventEnded += EventEnded;
    }

    /// <summary>
    /// Przygotowanie gry.
    /// </summary>
    /// <param name="saveData">Zapis gry.</param>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async UniTask SetGame(SaveData saveData)
    {
        player.data = saveData.playerData;
        await mapManager.SetLevels(saveData.levels);
        player.RefreshPosition();
        uiManager.heroInfoManager.SetPlayerData();
        cameraMoveManager.SetCameraOnPlayer();
        currentSave = saveData;
        if(currentSave.checkpointSave == null)
        {
            currentSave.SetAsCheckPoint();
        }
        await SaveCurrentGame();
    }

    /// <summary>
    /// Zapisanie przydzielonych poziomów umiejętności.
    /// </summary>
    public void ApplyAllLevels() => uiManager.heroInfoManager.ApplyAllLevels();

    /// <summary>
    /// Załadowanie ostatniego punktu kontrolnego.
    /// </summary>
    public void LoadLastCheckPoint()
    {
        GameLoadManager.LoadGame(currentSave, true);
    }

    /// <summary>
    /// Zapisanie aktualnej gry.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async UniTask SaveCurrentGame()
    {
        if (reachedCheckpoint)
        {
            reachedCheckpoint = false;
            currentSave.SetAsCheckPoint();
        }
        await PlayerSaver.Save(currentSave);
    }

    /// <summary>
    /// Usunięcie aktualnego zapisu gry.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async UniTask DeleteCurrentGame()
    {
        await PlayerSaver.RemovePlayerData(currentSave.id);
    }

    /// <summary>
    /// Utworzenie nowego poziomu z ekranem przejściowym.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async UniTask GenerateLevelWithTransition()
    {
        await uiManager.levelTransitionScreen.Show();
        await mapManager.CreateNewLevel();
        await uiManager.levelTransitionScreen.Hide();
    }

    /// <summary>
    /// Obsługa zakończenia rozwiązywania zdarzenia losowego lub zakończenia tury bez wywołania zdarzenia losowego.
    /// </summary>
    async void EventEnded() {
        if (player.data.Peoples == 0)
        {
            uiManager.endgameScreen.Show().Forget();
        }
        else
        {
            uiManager.SetBarButtonsActive(true);
            if (reachedCheckpoint)
            {
                await GenerateLevelWithTransition();
            }
            await SaveCurrentGame();
        }
    }
}
