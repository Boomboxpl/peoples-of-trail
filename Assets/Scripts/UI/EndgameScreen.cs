﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Cysharp.Threading.Tasks;
using DG.Tweening;

/// <summary>
/// Ekran końca gry.
/// </summary>
public class EndgameScreen : MonoBehaviour
{
    /// <summary>
    /// Informacja, czy ekran jest wyświetlany.
    /// </summary>
    public bool isShown { get; private set; }

    /// <summary>
    /// Czas animacji pojawiania się ekranu w sekundach.
    /// </summary>
    public float screenFadeAnimationDuration = 1f;
    /// <summary>
    /// Opóźnienie pomiędzy pojawieniem się kolejnych tekstów w sekundach, 
    /// </summary>
    public float delayBetweenText = 1f;
    /// <summary>
    /// Czas animacji pojawiania się pojedynczego tekstu w sekundach.
    /// </summary>
    public float textFadeAnimationDuration = 0.5f;
    /// <summary>
    /// Czas animacji pojawiania się przycisków w sekundach.
    /// </summary>
    public float buttonsFadeAnimationDuration = 0.5f;

    /// <summary>
    /// Obraz tła dla ekranu.
    /// </summary>
    public Image image;
    /// <summary>
    /// Tekst z opisem fabularnym zachęcającym do wczytania punktu kontrolnego.
    /// </summary>
    public TextMeshProUGUI factText;
    /// <summary>
    /// Tekst z opisem fabularnym zachęcającym do zakończenia gry.
    /// </summary>
    public TextMeshProUGUI doubtText;
    /// <summary>
    /// Komponent kontrolujący przeźroczystość i interakcje przycisków na ekranie.
    /// </summary>
    public CanvasGroup buttons;

    /// <summary>
    /// Inicjalizacja. Ukrycie ekranu oraz wyłączenie interakcji.
    /// </summary>
    void Awake()
    {
        image.raycastTarget = false;
        image.color = image.color.SetAlpha(0f);
        factText.color = factText.color.SetAlpha(0f);
        doubtText.color = doubtText.color.SetAlpha(0f);
        buttons.blocksRaycasts = false;
        buttons.alpha = 0f;
    }

    /// <summary>
    /// Animacja pojawienia się ekranu.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async UniTask Show()
    {
        image.raycastTarget = true;

        await image.DOFade(1, screenFadeAnimationDuration);
        await factText.DOFade(1f, textFadeAnimationDuration);
        await doubtText.DOFade(1f, textFadeAnimationDuration).SetDelay(delayBetweenText);
        await buttons.DOFade(1f, buttonsFadeAnimationDuration).OnComplete(() => buttons.blocksRaycasts = true);
        isShown = true;
    }

    /// <summary>
    /// Animacja ukrywania ekranu.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async UniTask Hide()
    {
        buttons.blocksRaycasts = false;
        factText.DOFade(0f, textFadeAnimationDuration).AwaitForComplete().Forget();
        doubtText.DOFade(0f, textFadeAnimationDuration).AwaitForComplete().Forget();
        buttons.DOFade(0f, buttonsFadeAnimationDuration).AwaitForComplete().Forget();
        await image.DOFade(0f, screenFadeAnimationDuration);
        isShown = false;
        image.raycastTarget = false;
    }
}
