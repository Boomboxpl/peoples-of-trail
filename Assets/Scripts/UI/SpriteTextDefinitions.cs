﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa z definicjami obrazów.
/// </summary>
public static class SpriteTextDefinitions
{
    /// <summary>
    /// Obraz ludzi.
    /// </summary>
    public const string peoplesSprite = "<sprite=50>";
    /// <summary>
    /// Obraz morali.
    /// </summary>
    public const string moralesSprite = "<sprite=12>";
    /// <summary>
    /// Obraz zysku zasobów.
    /// </summary>
    public const string resourcesGatherSprite = "<sprite=54>";
    /// <summary>
    /// Obraz utraty zasobów.
    /// </summary>
    public const string resourcesLoseSprite = "<sprite=56>";
    /// <summary>
    /// Strzałka skierowana w górę.
    /// </summary>
    public const string arrowUp = "<rotate=-90.0><sprite=22></rotate>";
    /// <summary>
    /// Strzałka skierowana w dół.
    /// </summary>
    public const string arrowDown = "<rotate=-90.0><sprite=15></rotate>";
    /// <summary>
    /// Obraz punktów szacunku.
    /// </summary>
    public const string respectSprite = "<sprite=37>";
    /// <summary>
    /// Obraz dla kostki do gry. Używany do reprezentowania prawdopodobieństwa powodzenia zdarzeń losowych.
    /// </summary>
    public const string diceSprite = "<sprite=53>";
    /// <summary>
    /// Obraz czaszki. Używany do reprezentowania poziomu niebezpieczeństwa.
    /// </summary>
    public const string skullSprite = "<sprite=42>";
}
