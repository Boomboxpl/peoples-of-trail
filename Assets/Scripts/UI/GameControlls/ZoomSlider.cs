﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Suwak przybliżania planszy.
/// </summary>
[RequireComponent(typeof(Slider))]
public class ZoomSlider : MonoBehaviour
{
    /// <summary>
    /// Zarządca ruchu kamery.
    /// </summary>
    public CameraMoveManager cameraMoveManager;
    /// <summary>
    /// Komponent obsługujący suwak.
    /// </summary>
    Slider slider;

    /// <summary>
    /// Inicjalizacja. Ustawienie reakcji na zmiane wartości suwaka.
    /// </summary>
    void Awake()
    {
        slider = GetComponent<Slider>();
        slider.onValueChanged.AddListener(OnValueChanged);
    }

    /// <summary>
    /// Inicjalizacja. Ustawienie reakcji na ruch kamery.
    /// </summary>
    void Start()
    {
        cameraMoveManager.cameraMoved += RefreshSliderPosition;
        RefreshSliderPosition();
    }

    /// <summary>
    /// Inicjalizacja. Usunięcie reakcji na ruch kamery.
    /// </summary>
    void OnDestroy()
    {
        cameraMoveManager.cameraMoved -= RefreshSliderPosition;
    }

    /// <summary>
    /// Obsługa zmiany wartości suwaka. Ustawienie przybliżenia.
    /// </summary>
    /// <param name="value">Nowa wartość suwaka.</param>
    public void OnValueChanged(float value)
    {
        cameraMoveManager.SetCameraZoom(value, true);
    }

    /// <summary>
    /// Odświeżenie wartości suwaka z aktualnego przybliżenia. Odświeżenie nastąpi tyko kiedy wystąpi zmiana przybliżenia.
    /// </summary>
    /// <param name="delta">Zmiana pozycji kamery.</param>
    void RefreshSliderPosition(CameraDelta delta)
    {
        if (!Mathf.Approximately(delta.scaleDelta, 0f))
        {
            RefreshSliderPosition();
        }
    }

    /// <summary>
    /// Odświeżenie wartości suwaka z aktualnego przybliżenia.
    /// </summary>
    void RefreshSliderPosition()
    {
        slider.SetValueWithoutNotify(cameraMoveManager.CameraNormalizedZoom);
    }
}
