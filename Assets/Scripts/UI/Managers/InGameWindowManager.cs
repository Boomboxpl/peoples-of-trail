﻿using Cysharp.Threading.Tasks;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Zarządca okna w grze.
/// </summary>
public class InGameWindowManager : MonoBehaviour
{
    /// <summary>
    /// Czas trwania animacji wyświetlania i ukrywania w sekundach.
    /// </summary>
    public float animationDuration = 1f;
    /// <summary>
    /// obiekt okna.
    /// </summary>
    public RectTransform windowTransform;

    /// <summary>
    /// Stan wyświetlenia okna.
    /// </summary>
    /// <value></value>
    public bool isShown { get; private set; }
    /// <summary>
    /// Komponent obsługujący naciśnięcie poza oknem.
    /// </summary>
    /// <value></value>
    protected RaycastBlocker raycastBlocker { get; private set; }

    /// <summary>
    /// Inicjalizacja. Przpisanie reakcji na naciśnięcie poza oknem. Ustawienie wyświetlania zgodnie z stanem wyświetlenia.
    /// </summary>
    protected virtual void Awake()
    {
        raycastBlocker = GetComponentInChildren<RaycastBlocker>();
        raycastBlocker.onClick += Hide;

        gameObject.SetActive(isShown);
        if (!isShown)
        {
            windowTransform.anchoredPosition = new Vector2(-this.GetCanvasSize().x, windowTransform.anchoredPosition.y);
        }
    }

    /// <summary>
    /// Ukrywanie i wyświetlanie w zależności od aktualnego stanu wyświetlenia.
    /// Stan, po wykonaniu animacji, zostanie ustawiony na przeciwny.
    /// </summary>
    public void ShowHide()
    {
        if (isShown)
        {
            Hide();
        }
        else
        {
            Show();
        }
    }

    /// <summary>
    /// Wyświetlanie okna.
    /// </summary>
    public void Show()
    {
        ShowAsync().Forget();
    }

    /// <summary>
    /// Ukrywanie okna.
    /// </summary>
    public void Hide()
    {
        HideAsync().Forget();
    }

    /// <summary>
    /// Wyświetlanie okna.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async UniTask ShowAsync()
    {
        windowTransform.DOKill();
        gameObject.SetActive(true);
        GameManager.Instance.uiManager.SetBarButtonsActive(false);
        isShown = true;
        raycastBlocker.Show();
        await GetShowTween();
    }

    /// <summary>
    /// Ukrywanie okna.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async UniTask HideAsync()
    {
        if (isShown)
        {
            windowTransform.DOKill();
            isShown = false;
            gameObject.SetActive(true);
            GameManager.Instance.uiManager.SetBarButtonsActive(true);
            raycastBlocker.Hide();
            await GetHideTween().OnComplete(() => gameObject.SetActive(false));
        }
    }

    /// <summary>
    /// Wygenerowanie animacji wyświetlania okna.
    /// </summary>
    /// <returns>Animacja.</returns>
    protected virtual Tween GetShowTween() => windowTransform.DOAnchorPosX(0, animationDuration);

    /// <summary>
    /// Wygenerowanie animacji ukrywania okna.
    /// </summary>
    /// <returns>Animacja.</returns>
    protected virtual Tween GetHideTween() => windowTransform.DOAnchorPosX(-this.GetCanvasSize().x, animationDuration);
}
