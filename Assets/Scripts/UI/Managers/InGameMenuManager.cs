﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Zarządca menu w grze.
/// </summary>
public class InGameMenuManager : InGameWindowManager
{
    /// <summary>
    /// Zarządca paneli menu.
    /// </summary>
    MenuPanelManager panelManager;

    /// <summary>
    /// Inicjalizacja. Przyisanie zarządcy paneli.
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
        windowTransform.anchoredPosition = Vector2.zero;
        panelManager = GetComponentInChildren<MenuPanelManager>();
    }

    /// <summary>
    /// Wygenerowanie animacji wyświetlania okna.
    /// </summary>
    /// <returns>Animacja.</returns>
    protected override Tween GetShowTween()
    {
        InputManager.Instance.SetPressCallback(InputAction.Back, TryHide);
        InputManager.Instance.SetPressCallback(InputAction.Menu, TryHide);
        return windowTransform.transform.DOScale(1f, animationDuration);
    }

    /// <summary>
    /// Wygenerowanie animacji ukrywania okna.
    /// </summary>
    /// <returns>Animacja.</returns>
    protected override Tween GetHideTween()
    {
        InputManager.Instance.ClearPressCallback(InputAction.Back, TryHide);
        InputManager.Instance.ClearPressCallback(InputAction.Menu, TryHide);
        return windowTransform.transform.DOScale(0f, animationDuration);
    }

    /// <summary>
    /// Próba ukrycia menu.
    /// Zmiany zostaną wykonane tylko, jeżeli aktywny panel będzie panelem początkowym.
    /// </summary>
    public void TryHide()
    {
        if (panelManager.isOnFirstPanel)
        {
            GameManager.Instance.uiManager.HideMenu();
        }
    }
}
