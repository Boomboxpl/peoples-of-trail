﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Cysharp.Threading.Tasks;

/// <summary>
/// Główny zarządca interfejsu użytkownika.
/// </summary>
public class UIManager : MonoBehaviour
{
    /// <summary>
    /// Zarządca informacji o atrybutach gracza wyświetlanych podczas rozgrywki.
    /// </summary>
    public InGameInfo gameInfo;
    /// <summary>
    /// Zarządca menu w grze.
    /// </summary>
    public InGameMenuManager inGameMenu;
    /// <summary>
    /// Zarządca informacji o atrybutach gracza wyświetlanych podczas rozgrywki. 
    /// </summary>
    public HeroInfoManager heroInfoManager;
    /// <summary>
    /// Ekran przejścia pomiędzy poziomami.
    /// </summary>
    public LevelTransitionScreen levelTransitionScreen;
    /// <summary>
    /// Ekran końca gry.
    /// </summary>
    public EndgameScreen endgameScreen;
    /// <summary>
    /// Okno rozwiązywania zdarzeń losowych.
    /// </summary>
    public EventSolver eventSolver;

    /// <summary>
    /// Pasek górny.
    /// </summary>
    [SerializeField] InfoBar barTop;
    /// <summary>
    /// Pasek dolny.
    /// </summary>
    [SerializeField] InfoBar barBot;
    /// <summary>
    /// Informacja, czy menu jest w trakcie animacji.
    /// </summary>
    bool isAnimating = false;

    /// <summary>
    /// Informacja, czy wszystkie okna są zamknięte.
    /// </summary>
    public bool IsClear => !inGameMenu.isShown && !heroInfoManager.isShown && !levelTransitionScreen.IsShown 
        && !endgameScreen.isShown && !eventSolver.isShown;

    /// <summary>
    /// Inicjalizacja. Ustawienie reakcji na naciśnięcie przycisku powrotu.
    /// </summary>
    void Awake()
    {
        InputManager.Instance.SetPressCallback(InputAction.Back, TryShowMenu);
    }

    /// <summary>
    /// Obsługa usunięcia obiektu. Usunięcie reakcji na naciśnięcie przycisku powrotu.
    /// </summary>
    void OnDestroy()
    {
        if(InputManager.Instance != null)
        {
            InputManager.Instance.ClearPressCallback(InputAction.Back, TryShowMenu);
        }
    }

    /// <summary>
    /// Ustawienie interakcji przycisków na paskach górnym i dolnym.
    /// </summary>
    /// <param name="active">Informacja, czy przyciski powinny być aktywne.</param>
    public void SetBarButtonsActive(bool active)
    {
        barTop.canvasGroup.interactable = active;
        barBot.canvasGroup.interactable = active;
    }

    /// <summary>
    /// Wyświetlenie menu. Ukrywa paski górny i dolny.
    /// </summary>
    public void ShowMenu() => ShowMenuAsync().Forget();

    /// <summary>
    /// Ukrycie menu. Wyświetla paski górny i dolny.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public void HideMenu() => HideMenuAsync().Forget();

    /// <summary>
    /// Odświeżenie interfejsu użytkownika.
    /// </summary>
    public void RefreshUI()
    {
        Player player = GameManager.Instance.player;
        gameInfo.Refresh(player);
    }

    /// <summary>
    /// Wyświetlenie menu. Ukrywa paski górny i dolny.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async UniTaskVoid ShowMenuAsync()
    {
        if (isAnimating)
        {
            return;
        }
        isAnimating = true;
        barTop.Hide();
        barBot.Hide();
        await inGameMenu.ShowAsync();
        isAnimating = false;
    }

    /// <summary>
    /// Ukrycie menu. Wyświetla paski górny i dolny.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async UniTaskVoid HideMenuAsync()
    {
        if (isAnimating)
        {
            return;
        }
        isAnimating = true;
        barTop.Show();
        barBot.Show();
        await inGameMenu.HideAsync();
        isAnimating = false;
    }

    /// <summary>
    /// Próba wyświetlenia menu w grze.
    /// Zostaje wyświetlone tylko, jeżeli wszystkie inne okna są ukryte.
    /// </summary>
    void TryShowMenu()
    {
        if (IsClear)
        {
            ShowMenu();
        }
    }
}
