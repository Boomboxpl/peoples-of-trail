﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Komponent dostarczający informacji o elementach interfejsu.
/// </summary>
public class UIInfoProvider : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    /// <summary>
    /// Aktualnie wybrany obiekt.
    /// </summary>
    public static UIInfoProvider Selected { get; private set; }

    /// <summary>
    /// Treść do wyświetlenia w podpowiedzi.
    /// </summary>
    public string Message => LocalizationManager.Localize(messageKey);

    /// <summary>
    /// Przesunięcie podpowiedzi względem obiektu.
    /// </summary>
    public Vector2 offset;
    /// <summary>
    /// Preferowane ustawienie punktu piwotalnego okna podpowiedzi.
    /// </summary>
    public Vector2 preferedPivotPosition;

    /// <summary>
    /// Klucz systemu tłumaczącego dla treści podpowiedzi.
    /// </summary>
    public string messageKey;

    /// <summary>
    /// Obsługa wejścia kursora nad element interfejsu.
    /// </summary>
    /// <param name="eventData">Informacje o zdarzeniu.</param>
    public void OnPointerEnter(PointerEventData eventData)
    {
        Selected = this;
    }

    /// <summary>
    /// Obsługa wyjścia kursora z nad element interfejsu.
    /// </summary>
    /// <param name="eventData">Informacje o zdarzeniu.</param>
    public void OnPointerExit(PointerEventData eventData)
    {
        if(Selected == this)
        {
            Selected = null;
        }
    }
}
