﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Przestrzeń zarządzająca wyświetlaniem informacji o interfejsie użytkownika.
/// </summary>
public class UIInfoSpace : TooltipSpace
{
    /// <summary>
    /// Pokazanie podpowiedzi.
    /// </summary>
    protected override void TryShow()
    {
        UIInfoProvider info = UIInfoProvider.Selected;
        if(info != null)
        {
           toolTip.Rect.pivot = info.preferedPivotPosition;
           toolTip.Show(info.Message, info.transform.position + (Vector3)info.offset);
        }
    }
}
