﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// Element wyświetlający informacje o kafelku.
/// </summary>
public class TileInfoTooltip : Tooltip
{
    /// <summary>
    /// Kamera. Służy do przeliczania pozycji świata na pozycję ekranu.
    /// </summary>
    public Camera canvasCamera;

    /// <summary>
    /// Pozycja w świecie.
    /// </summary>
    Vector3 positionWorld;

    /// <summary>
    /// Aktualizacja w każdej klatce. Ponowne obliczenie pozycji na ekranie.
    /// </summary>
    void Update()
    {
        transform.position = ClampPosition(canvasCamera.WorldToScreenPoint(positionWorld));
    }

    /// <summary>
    /// Ustawienie wyświetlanej informacji.
    /// </summary>
    /// <param name="content">Tekst ustawiony do wyświetlenia.</param>
    /// <param name="position">Pozycja w świecie.</param>
    public override void Show(string content, Vector3 position)
    {
        positionWorld = canvasCamera.ScreenToWorldPoint(position);
        base.Show(content, positionWorld);
    }
}

