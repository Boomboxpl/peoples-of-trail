﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Licznik odliczający czas do pokazania podpowiedzi. Resetowany po ruchu myszy.
/// </summary>
public class TooltipHoverCounter : MonoBehaviour
{
    /// <summary>
    /// Delegat wywoływany po rozpoczęciu działania licznika.
    /// </summary>
    public Action started;
    /// <summary>
    /// Delegat wywoływany po zakończeniu działania licznika.
    /// </summary>
    public Action ended;

    /// <summary>
    /// Próg czasu w bezruchu, po którym wyświetlane są informacje o kafelku.
    /// </summary>
    public float infoTimeThreshold;
    /// <summary>
    /// Próg dystansu pokonanego, po którym odświeżany jest czas do wyświetlenia informacji o kafelku i ukrywane są aktualne informacje.
    /// </summary>
    public float infoDistanceThreshold;

    /// <summary>
    /// Ostatnia pozycja kursora na ekranie.
    /// </summary>
    Vector3 lastPositionScreen;
    /// <summary>
    /// Czas od zatrzymania kursora nad mapą.
    /// </summary>
    float hoverStartTime;
    /// <summary>
    /// Przebyty dystans od zatrzymania kursora nad mapą.
    /// </summary>
    float hoverDistance;
    /// <summary>
    /// Informacja, czy licznik jest aktywny. Obiekt na scenie jest zawsze aktywny.
    /// </summary>
    bool isActive;

    /// <summary>
    /// Inicjalizacja. Ustawienie licznika jako niekatywnego.
    /// </summary>
    void Awake()
    {
        isActive = false;
    }

    /// <summary>
    /// Aktualizacja pod koniec klatki. Aktualizacja zatrzymania kursora nad mapą.
    /// </summary>
    void LateUpdate()
    {
        UpdateHover();
    }

    /// <summary>
    /// Rozpoczęcie obsługi zatrzymania kursora nad mapą.
    /// </summary>
    public void StartHover()
    {
        hoverStartTime = Time.time;
        isActive = true;
        started?.Invoke();
        hoverDistance = 0f;
    }

    /// <summary>
    /// Aktualizacja kursora nad mapą.
    /// </summary>
    void UpdateHover()
    {
        hoverDistance += (lastPositionScreen - Input.mousePosition).sqrMagnitude;
        lastPositionScreen = Input.mousePosition;

        if (hoverDistance > infoDistanceThreshold * infoDistanceThreshold)
        {
            StartHover();
        }

        if (Time.time - hoverStartTime > infoTimeThreshold && isActive)
        {
            isActive = false;
            ended?.Invoke();
        }
    }
}
