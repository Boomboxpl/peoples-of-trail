﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Podpowiedź nie zmieniająca pozycji na ekranie.
/// </summary>
public class StaticTooltip : Tooltip
{
    /// <summary>
    /// Ustawienie wyświetlanej informacji.
    /// </summary>
    /// <param name="content">Tekst ustawiony do wyświetlenia.</param>
    /// <param name="position">Pozycja.</param>
    public override void Show(string content, Vector3 position)
    {
        base.Show(content, position);
        StartCoroutine(SetPositionAfterFrame(position));
    }

    /// <summary>
    /// Ustawienie pozycji po klatce. Po klatce następuje przebudowanie i ponowne obliczenie rozmiaru.
    /// </summary>
    /// <param name="position">Pozycja na ekranie.</param>
    /// <returns>Korutyna.</returns>
    IEnumerator SetPositionAfterFrame(Vector3 position)
    {
        yield return null;
        transform.position = ClampPosition(position);
    }
}
