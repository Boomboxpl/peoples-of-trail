﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// Podpowiedź.
/// </summary>
public class Tooltip : MonoBehaviour
{
    /// <summary>
    /// Wyświetlany tekst.
    /// </summary>
    public TextMeshProUGUI message;

    /// <summary>
    /// RectTransform obiektu.
    /// </summary>
    public RectTransform Rect => thisTransform;

    /// <summary>
    /// Długość animacji pokazywania lub ukrywania.
    /// </summary>
    public float animationDuration = 0.5f;

    /// <summary>
    /// Informacja, czy obiekt jest ukryty.
    /// </summary>
    public bool Hidden { get; private set; }

    /// <summary>
    /// Obiekt ograniczający możliwe pozycje okna z informacjami. Uzyskiwany z rodzica obiektu.
    /// </summary>
    RectTransform boundaryTransform;
    /// <summary>
    /// Pozycja obiektu.
    /// </summary>
    RectTransform thisTransform;
    /// <summary>
    /// Grupa elementów, dla których będzie ustawiana przeźroczystość podczas animacji.
    /// </summary>
    CanvasGroup canvasGroup;


    /// <summary>
    /// Inicjalizacja. Uzyskanie pozycji obiektu ograniczającego oraz aktualnego obiektu.
    /// </summary>
    protected virtual void Awake()
    {
        TooltipSpace space = GetComponentInParent<TooltipSpace>();
        boundaryTransform = space.GetComponent<RectTransform>();
        thisTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        canvasGroup.alpha = 0f;
        Hidden = false;
    }

    /// <summary>
    /// Ustawienie wyświetlanej informacji.
    /// </summary>
    /// <param name="content">Tekst ustawiony do wyświetlenia.</param>
    /// <param name="position">Pozycja.</param>
    public virtual void Show(string content, Vector3 position)
    {
        if (Hidden)
        {
            Hidden = false;
            message.text = content;
            gameObject.SetActive(true);
            canvasGroup.DOFade(1, animationDuration).SetEase(Ease.OutQuad);
        }
    }

    /// <summary>
    /// Ukrycie informacji.
    /// </summary>
    public virtual void Hide()
    {
        if (!Hidden)
        {
            Hidden = true;
            gameObject.SetActive(true);
            canvasGroup.DOFade(0, animationDuration).SetEase(Ease.InQuad).OnComplete(() => gameObject.SetActive(false));
        }
    }

    /// <summary>
    /// Obliczenie pozycji w obiekcie ograniczającym.
    /// </summary>
    /// <param name="position">Pozycja na ekranie.</param>
    /// <returns>Docelowa pozycja na ekranie.</returns>
    protected Vector3 ClampPosition(Vector3 position)
    {
        Rect boundaryRect = RectTransformToScreenSpace(boundaryTransform);
        Rect thisRect = RectTransformToScreenSpace(thisTransform);

        Vector3 minPosition = boundaryRect.min + thisTransform.pivot * thisRect.size;
        Vector3 maxPosition = boundaryRect.max - (Vector2.one - thisTransform.pivot) * thisRect.size;

        position.x = Mathf.Clamp(position.x, minPosition.x, maxPosition.x);
        position.y = Mathf.Clamp(position.y, minPosition.y, maxPosition.y);

        return position;
    }

    /// <summary>
    /// Przeliczenie prostokąta ustawionego na ekranie przez obiekt ograniczający.
    /// </summary>
    /// <param name="transform">Obiekt ograniczający.</param>
    /// <returns>Prostokąt na ekranie.</returns>
    static Rect RectTransformToScreenSpace(RectTransform transform)
    {
        Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
        return new Rect((Vector2)transform.position - (size * transform.pivot), size);
    }

}
