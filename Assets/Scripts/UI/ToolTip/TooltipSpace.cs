﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Przestrzeń zarządzająca wyświetlaniem podpowiedzi.
/// </summary>
public abstract class TooltipSpace : MonoBehaviour
{
    /// <summary>
    /// Licznik.
    /// </summary>
    public TooltipHoverCounter hoverCounter;

    /// <summary>
    /// Obiekt wyświetlający podpowiedź.
    /// </summary>
    protected Tooltip toolTip;

    /// <summary>
    /// Inicjalizacja. Ustawienie reakcji na zmianę stanu licznika. Ustawienie licznika jeśli nie został ustawiony.
    /// </summary>
    protected virtual void Awake()
    {
        toolTip = GetComponentInChildren<Tooltip>();

        if (hoverCounter == null)
        {
            hoverCounter = GetComponentInParent<TooltipHoverCounter>();
        }
        hoverCounter.started += Hide;
        hoverCounter.ended += TryShow;
    }

    /// <summary>
    /// Pokazanie podpowiedzi.
    /// </summary>
    protected abstract void TryShow();

    /// <summary>
    /// Ukrycie podpowiedzi.
    /// </summary>
    protected virtual void Hide()
    {
        toolTip.Hide();
    }
}
