﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Przestrzeń zarządzająca wyświetlaniem informacji o kafelkach.
/// </summary>
public class TileInfoSpace : TooltipSpace
{
    /// <summary>
    /// Przestrzeń robocza w której następuje przesuwanie kamery/mapy.
    /// </summary>
    public DragWorkspace dragWorkspace;
    /// <summary>
    /// Zarządca mapy.
    /// </summary>
    public MapManager mapManager;


    /// <summary>
    /// Inicjalizacja. Przypisanie reakcji na ruch kamery.
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
        dragWorkspace.cameraMoveManager.cameraMoved += CameraMoved;
    }

    /// <summary>
    /// Usunięcie obiektu. Usunięcie przypisania reakcji na ruch kamery.
    /// </summary>
    void OnDestroy()
    {
        if (dragWorkspace != null && dragWorkspace.cameraMoveManager != null)
        {
            dragWorkspace.cameraMoveManager.cameraMoved -= CameraMoved;
        }
    }

    /// <summary>
    /// Reakcja na ruch kamery. Obsługa nowego zatrzymania kursora nad mapą. Ukrycie aktualnych infromacji.
    /// </summary>
    /// <param name="delta">Zmiana pozycji kamery.</param>
    void CameraMoved(CameraDelta delta)
    {
        if (dragWorkspace.IsPointerOver)
        {
            hoverCounter.StartHover();
        }
    }

    /// <summary>
    /// Pokazanie podpowiedzi.
    /// </summary>
    protected override async void TryShow()
    {
        if (dragWorkspace.IsPointerOver)
        {
            Vector3 worldPosition = dragWorkspace.cameraMoveManager.ControlledCamera.ScreenToWorldPoint(Input.mousePosition);
            toolTip.Show(await mapManager.GetTileAt(worldPosition).GetInfo(), Input.mousePosition);
        }
    }
}
