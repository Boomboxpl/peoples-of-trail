﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Zarządca interfejsu prezentującego morale.
/// </summary>
public class MoralesUI : MonoBehaviour
{
    /// <summary>
    /// Suwak przedstawiający procent morali.
    /// </summary>
    public Slider slider;

    /// <summary>
    /// Ustawienie watości morali.
    /// </summary>
    /// <param name="value">Nowa wartość.</param>
    public void SetMorales(float value)
    {
        slider.value = value;
    }
}
