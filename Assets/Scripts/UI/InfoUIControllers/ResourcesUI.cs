﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// Zarządca interfejsu prezentującego zebrane zasoby.
/// </summary>
public class ResourcesUI : MonoBehaviour
{
    /// <summary>
    /// Format tekstu.
    /// </summary>
    const string format = "{0} <color=#{1}>{2}";

    /// <summary>
    /// Wyświetlany tekst.
    /// </summary>
    public TextMeshProUGUI text;
    /// <summary>
    /// Kolor dla braku zmiany zasobów.
    /// </summary>
    public Color colorNeutral;
    /// <summary>
    /// Kolor dla wzrostu ilości zasobów.
    /// </summary>
    public Color colorPositive;
    /// <summary>
    /// Kolor dla spadku ilości zasobów.
    /// </summary>
    public Color colorNegative;

    /// <summary>
    /// Ustawienie zasobów.
    /// </summary>
    /// <param name="resources">Ilość zasobów.</param>
    /// <param name="delta">Zmiana ilości zasobów.</param>
    public void SetResources(int resources, int delta)
    {
        string color;
        if(delta > 0)
        {
            color = ColorUtility.ToHtmlStringRGBA(colorPositive);
        }
        else if(delta < 0)
        {
            color = ColorUtility.ToHtmlStringRGBA(colorNegative);
        }
        else
        {
            color = ColorUtility.ToHtmlStringRGBA(colorNeutral);
        }

        text.text = string.Format(format, resources, color, delta.ToString("+#;-#;+0"));
        Canvas.ForceUpdateCanvases();
    }
}
