﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Pasek grupujący elementy interfejsu podczas rozgrywki.
/// </summary>
public class InfoBar : MonoBehaviour
{
    /// <summary>
    /// Czas trwania animacji wyświetlania i ukrywania w sekundach.
    /// </summary>
    public float animationDuration = 0.5f;

    /// <summary>
    /// Komponent kontrolujący przeźroczystość i interakcje elementów na pasku.
    /// </summary>
    public CanvasGroup canvasGroup { get; private set; }

    /// <summary>
    /// Przesunięcie, o które musi się przesunąć pasek, aby się ukryć poza ekranem.
    /// </summary>
    [SerializeField] Vector2 hideOffset;
    /// <summary>
    /// Komponent zarządcający pozycją paska.
    /// </summary>
    RectTransform rectTransform;
    /// <summary>
    /// Pozycja wyświetlania.
    /// </summary>
    Vector2 showPosition;
    /// <summary>
    /// Pozycja ukrywania.
    /// </summary>
    Vector2 hidePosition => showPosition + hideOffset;

    /// <summary>
    /// Inicjalizacja. Przypisanie aktualnej pozycji jao pozycji wyświetlania.
    /// </summary>
    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        rectTransform = GetComponent<RectTransform>();
        showPosition = rectTransform.anchoredPosition;
    }

    /// <summary>
    /// Wyświetlanie.
    /// </summary>
    public void Show()
    {
        rectTransform.DOAnchorPos(showPosition, animationDuration);
    }

    /// <summary>
    /// Ukrywanie.
    /// </summary>
    public void Hide()
    {
        rectTransform.DOAnchorPos(hidePosition, animationDuration);
    }
}
