﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// Zarządca interfejsu prezentującego liczbę ludności.
/// </summary>
public class PeoplesUI : MonoBehaviour
{
    /// <summary>
    /// Format tekstu.
    /// </summary>
    const string format = "{0}/{1}";

    /// <summary>
    /// Wyświetlany tekst.
    /// </summary>
    public TextMeshProUGUI text;

    /// <summary>
    /// Ustawienie liczby ludności.
    /// </summary>
    /// <param name="peoples">Liczba ludności.</param>
    /// <param name="maxPeoples">Maksymalna liczba ludności.</param>
    public void SetPeoples(int peoples, int maxPeoples)
    {
        text.text = string.Format(format, peoples, maxPeoples);
    }
}
