﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Zarządca informacji o grze wyświetlanych podczas rozgrywki. 
/// </summary>
public class InGameInfo : MonoBehaviour
{
    /// <summary>
    /// Zarządca interfejsu prezentującego morale.
    /// </summary>
    [SerializeField] MoralesUI morales;
    /// <summary>
    /// Zarządca interfejsu prezentującego zebrane zasoby.
    /// </summary>
    [SerializeField] ResourcesUI resources;
    /// <summary>
    /// Zarządca interfejsu prezentującego liczbę ludności.
    /// </summary>
    [SerializeField] PeoplesUI peoples;

    /// <summary>
    /// Odświeżenie interfejsu użytkownika.
    /// </summary>
    /// <param name="player">Aktualne dane.</param>
    public void Refresh(Player player)
    {
        morales.SetMorales(player.data.Morales);
        resources.SetResources(player.data.Resources, player.NextMoveResourcesDelta);
        peoples.SetPeoples(player.data.Peoples, player.data.MaxPeoples);
    }
}
