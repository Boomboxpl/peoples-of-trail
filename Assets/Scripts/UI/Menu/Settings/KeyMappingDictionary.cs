﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

/// <summary>
/// Słownik przekształcający kod klawisza na przyjazną dla użytkownika nazwę lub symbol.
/// </summary>
[CreateAssetMenu(fileName = "KeyMapping", menuName = "KeyMapping")]
public class KeyMappingDictionary : ScriptableObject
{
    /// <summary>
    /// Font dla klawiszy.
    /// </summary>
    public TMP_FontAsset keyFont;

    /// <summary>
    /// Słownik z danymi służącymi do przekształcania.
    /// </summary>
    [SerializeField] List<MappingData> dictionary;

    /// <summary>
    /// Przekształcenie klawisza na przyjazną dla użytkownika nazwę.
    /// </summary>
    /// <param name="key">Kod klawisza.</param>
    /// <returns>Nazwa, w zależności od konfiguracji.</returns>
    public string GetKeyFontString(KeyCode key)
    {
        MappingData data = dictionary.Find(_data => _data.key == key);

        string result = "";
        switch (data.type)
        {
            case MappingData.Type.Font:
                result = $"<font=\"{keyFont.name}\">{data.character}</font>";
                break;
            case MappingData.Type.Raw:
                result = data.character;
                break;
            case MappingData.Type.Localized:
                result = LocalizationManager.Localize(data.character);
                break;
        }
        return result;
    }

    /// <summary>
    /// Przygotowanie słownika. Utworzenie słownika z wszystkimi możliwymi kluczami i pustymi wartościami. 
    /// Nie robi nic, jeżeli słownik jest już przygotowany.
    /// </summary>
    [ContextMenu("Prepare")]
    public void PrepareDictionary()
    {
        if(dictionary != null)
        {
            return;
        }
        dictionary = new List<MappingData>();
        foreach(KeyCode value in Enum.GetValues(typeof(KeyCode)))
        {
            dictionary.Add(new MappingData(value, ""));
        }
    }

    /// <summary>
    /// Dane służące do przekształcania.
    /// </summary>
    [Serializable]
    private struct MappingData
    {
        /// <summary>
        /// Nazwa, do wyświetlenia w edytorze Unity.
        /// </summary>
        public string name;
        /// <summary>
        /// Kod klawisza.
        /// </summary>
        public KeyCode key;
        /// <summary>
        /// Typ konfiguracji.
        /// </summary>
        public Type type;
        /// <summary>
        /// Dane znaku. Interpretacja zależna od typu.
        /// </summary>
        public string character;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="key">Kod klawisza.</param>
        /// <param name="character">Dane znaku.</param>
        public MappingData(KeyCode key, string character)
        {
            name = key.ToString();
            this.key = key;
            type = Type.Font;
            this.character = character;
        }

        /// <summary>
        /// Typ interpretacji danych.
        /// </summary>
        public enum Type
        {
            /// <summary>
            /// Specialny znak w foncie dla klawiszy.
            /// </summary>
            Font,
            /// <summary>
            /// Nieprzetworzony ciąg znaków.
            /// </summary>
            Raw, 
            /// <summary>
            /// Klucz w systemie tłumaczącym.
            /// </summary>
            Localized
        }
    }

}
