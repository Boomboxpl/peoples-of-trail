﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// Zarządca ustawienia konfiguracji pojedynczej akcji sterowania.
/// </summary>
public class ControllSetting : MonoBehaviour
{
    /// <summary>
    /// Ustawiana akcja.
    /// </summary>
    public InputAction Action
    {
        get => action;
        set
        {
            action = value;
            FetchInput();
            RefreshComponents();
        }
    }

    /// <summary>
    /// Ustawiony klawisz podstawowy.
    /// </summary>
    public KeyCode SettedKey { get; private set; }
    /// <summary>
    /// Ustawiony klawisz alternatywny.
    /// </summary>
    public KeyCode SettedAlternativeKey { get; private set; }

    /// <summary>
    /// Etykieta akcji.
    /// </summary>
    public LocalizedText label;
    /// <summary>
    /// Przycisk ustawiania klawisza podstawowego.
    /// </summary>
    public Button primaryButton;
    /// <summary>
    /// Przycisk ustawiania klawisza alternatywnego.
    /// </summary>
    public Button alternativeButton;
    /// <summary>
    /// Słownik przekształcający kod klawisza na przyjazną dla użytkownika nazwę.
    /// </summary>
    public KeyMappingDictionary dictionary;
    /// <summary>
    /// Prefab okna dialogowego, ustawiającego klawisz sterowania.
    /// </summary>
    public InputSettingDialog dialogPrefab;

    /// <summary>
    /// Tekst klawisza podstawowego.
    /// </summary>
    TextMeshProUGUI primaryButtonText;
    /// <summary>
    /// Tekst klawisza alternatywnego.
    /// </summary>
    TextMeshProUGUI alternativeButtonText;
    /// <summary>
    /// Płótno, na którym zostanie wyświetlone okno dialogowe.
    /// </summary>
    Transform canvasTransform;

    /// <summary>
    /// Ustawiana akcja.
    /// </summary>
    InputAction action;

    /// <summary>
    /// Inicjalizacja. Ustawienie operacji dla przycisków.
    /// </summary>
    void Awake()
    {
        primaryButtonText = primaryButton.GetComponentInChildren<TextMeshProUGUI>(true);
        alternativeButtonText = alternativeButton.GetComponentInChildren<TextMeshProUGUI>(true);

        primaryButton.onClick.AddListener(
            () => PrepareSettingDialog().onComplete += (dialog, success) => SettingComplete(dialog, success, false)
            );

        alternativeButton.onClick.AddListener(
            () => PrepareSettingDialog().onComplete += (dialog, success) => SettingComplete(dialog, success, true)
            );

        canvasTransform = GetComponentInParent<Canvas>().transform;
    }

    /// <summary>
    /// Obsługa aktywacji obiektu. Odświeżenie informacji o klawiszach.
    /// </summary>
    void OnEnable()
    {
        FetchInput();
        RefreshComponents();
    }

    /// <summary>
    /// Odświeżenie informacji o klawiszach.
    /// </summary>
    public void RefreshComponents()
    {
        label.LocalizationKey = $"InputAction.{Action}";
        primaryButtonText.text = dictionary.GetKeyFontString(SettedKey);
        alternativeButtonText.text = dictionary.GetKeyFontString(SettedAlternativeKey);
    }

    /// <summary>
    /// Sprawdzenie czy klawisz został zmieniony.
    /// </summary>
    /// <returns>True, gdy użytkownik zmienił jeden z przypisanych klawiszy.</returns>
    public bool IsChanged()
    {
        KeyCode key;
        KeyCode alternativeKey;
        if (InputManager.Instance.GetInputKeys(Action, out key, out alternativeKey))
        {
            return SettedKey != key || SettedAlternativeKey != alternativeKey;
        }
        return false;
    }

    /// <summary>
    /// Zapis ustawienie do zarządcy sterowania.
    /// </summary>
    public void Save()
    {
        InputManager.Instance.SetInputKey(Action, SettedKey, SettedAlternativeKey);
    }

    /// <summary>
    /// Pobranie ustawienia z zarządcy sterowania.
    /// </summary>
    public void FetchInput()
    {
        KeyCode key;
        KeyCode alternativeKey;
        if (InputManager.Instance.GetInputKeys(Action, out key, out alternativeKey))
        {
            SettedKey = key;
            SettedAlternativeKey = alternativeKey;
        }
    }

    /// <summary>
    /// Zamiana klawiszy.
    /// </summary>
    public void SwapKeys()
    {
        KeyCode temporary = SettedKey;
        SettedKey = SettedAlternativeKey;
        SettedAlternativeKey = temporary;
        RefreshComponents();
    }

    /// <summary>
    /// Wyczyszczenie klawisza podstawowego.
    /// </summary>
    public void ClearPrimaryKey()
    {
        SettedKey = KeyCode.None;
        RefreshComponents();
    }

    /// <summary>
    /// Wyczyszczenie klawisza alternatywnego.
    /// </summary>
    public void ClearAlternativeKey()
    {
        SettedAlternativeKey = KeyCode.None;
        RefreshComponents();
    }

    /// <summary>
    /// Przygotowanie okna dialogowego.
    /// </summary>
    /// <returns>Okno dialogowe.</returns>
    InputSettingDialog PrepareSettingDialog()
    {
        InputSettingDialog dialog = Instantiate(dialogPrefab, canvasTransform);
        dialog.ActionName = label.Text;
        InputManager.Instance.SetMute(Action, true);
        return dialog;
    }

    /// <summary>
    /// Operacja wywoływana po zakończeniu działania okna dialogowego.
    /// </summary>
    /// <param name="dialog">Okno dialogowe.</param>
    /// <param name="success">Informacja, czy działanie okna się powiodło.</param>
    /// <param name="alternative">True, gdy ustawiany był klawisz alternatywny.</param>
    void SettingComplete(Dialog dialog, bool success, bool alternative)
    {
        InputSettingDialog inputSettingDialog = dialog as InputSettingDialog;
        if (success)
        {
            SetKey(inputSettingDialog, alternative);
        }
        CloseDialog(inputSettingDialog);
    }

    /// <summary>
    /// Zamknięcie okna dialogowego.
    /// </summary>
    /// <param name="inputSettingDialog">Okno dialogowe.</param>
    void CloseDialog(InputSettingDialog inputSettingDialog)
    {
        inputSettingDialog.Hide();
        InputManager.Instance.SetMute(Action, false);
    }

    /// <summary>
    /// Ustawienie klawisza.
    /// </summary>
    /// <param name="inputSettingDialog">Okno dialogowe.</param>
    /// <param name="alternative">True, gdy ustawiany był klawisz alternatywny.</param>
    void SetKey(InputSettingDialog inputSettingDialog, bool alternative)
    {
        if (inputSettingDialog != null)
        {
            if (!alternative)
            {
                SettedKey = inputSettingDialog.ChoosedKey;
            }
            else
            {
                SettedAlternativeKey = inputSettingDialog.ChoosedKey;
            }
            RefreshComponents();
        }
    }
}
