﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

/// <summary>
/// Zarządca ustawiania konfiguracji dla akcji.
/// </summary>
public class ControllsSettingsManager : MonoBehaviour, IPanelChangeHandler
{
    /// <summary>
    /// Klucz w systemie tłumaczącym dla komunikatu o podwójnym używaniu klawisza w konfiguracji.
    /// </summary>
    const string conflictingActionLocalizationKey = "MainMenu.Settings.Controlls.Dialog.Conflict";
    /// <summary>
    /// Klucz w systemie tłumaczącym dla komunikatu o skutkach ustawienia domyślnej konfiguracji.
    /// </summary>
    const string setDefaultLocalizationKey = "MainMenu.Settings.Controlls.Dialog.SetDefaultWarning";
    /// <summary>
    /// Klucz w systemie tłumaczącym dla komunikatu o niezapisanych zmianach w konfiguracji.
    /// </summary>
    const string changedLocalizationKey = "MainMenu.Settings.Controlls.Dialog.Changed";
    /// <summary>
    /// Klucz w systemie tłumaczącym dla komunikatu o akcji bez przypisanego klawisza.
    /// </summary>
    const string emptyLocalizationKey = "MainMenu.Settings.Controlls.Dialog.Empty";

    /// <summary>
    /// Zawartość, pod którą będą dodawane kolejne opcje konfiguracji sterowania.
    /// </summary>
    public Transform content;
    /// <summary>
    /// Prefabrykat dla opcji konfiguracji sterowania.
    /// </summary>
    public ControllSetting settingPrefab;

    /// <summary>
    /// Prefab okna dialogowego z przyciskiem OK.
    /// </summary>
    public OkDialog okDialogPrefab;
    /// <summary>
    /// Prefab okna dialogowego z przyciskami TAK/NIE.
    /// </summary>
    public YesNoDialog yesNoDialogPrefab;

    /// <summary>
    /// Opcje konfiguracji sterowania.
    /// </summary>
    List<ControllSetting> settings;
    /// <summary>
    /// Płótno, na którym zostanie wyświetlone okno dialogowe.
    /// </summary>
    Transform canvasTransform;

    /// <summary>
    /// Inicjalizacja pól.
    /// </summary>
    void Awake()
    {
        canvasTransform = GetComponentInParent<Canvas>().transform;
        settings = new List<ControllSetting>();
    }

    /// <summary>
    /// Inicjalizacja. Ustawienie opcji konfiguracji sterowania dla każdej akcji. 
    /// </summary>
    void Start()
    {
        foreach (InputAction value in Enum.GetValues(typeof(InputAction)))
        {
            ControllSetting instatiatedSetting = Instantiate(settingPrefab, content);
            instatiatedSetting.Action = value;
            settings.Add(instatiatedSetting);
        }
    }

    /// <summary>
    /// Próba zapisania ustawień.
    /// </summary>
    public void TrySaveSettings()
    {
        if (!ValidateSettings())
        {
            return;
        }
        foreach(ControllSetting setting in settings)
        {
            setting.Save();
        }
        InputManager.Instance.SaveSettingsToFile();
    }

    /// <summary>
    /// Próba powrotu konfiguracji do domyślnej.
    /// </summary>
    public void TryResetToDefault()
    {
        YesNoDialog dialog = Instantiate(yesNoDialogPrefab, canvasTransform);
        dialog.Message = LocalizationManager.Localize(setDefaultLocalizationKey);
        dialog.onComplete += ResetToDefault;
    }

    /// <summary>
    /// Sprawdzenie, czy panel może zostać zakmnięty. Sprawdzenie niezapisanych zmian.
    /// </summary>
    /// <param name="manager">Zarządca paneli.</param>
    /// <returns>True, gdy panel może być zamkniety.</returns>
    public bool CanChange(MenuPanelManager manager)
    {
        if (settings.Any(setting => setting.IsChanged()))
        {
            YesNoDialog instatiatedDialog = Instantiate(yesNoDialogPrefab, canvasTransform);
            instatiatedDialog.Message = LocalizationManager.Localize(changedLocalizationKey);
            instatiatedDialog.onComplete += (dialog, success) =>
            {
                dialog.Hide();
                if (success)
                {
                    foreach (ControllSetting setting in settings)
                    {
                        setting.FetchInput();
                        setting.RefreshComponents();
                    }
                    manager.Return();
                }
            };

            return false;
        }
        else
        {
            return true;
        }
    }

    /// <summary>
    /// Przywrócenie powrót konfiguracji do domyślnej.
    /// </summary>
    /// <param name="dialog">Okno dialogowe.</param>
    /// <param name="success">Informacja, czy działanie okna się powiodło.</param>
    void ResetToDefault(Dialog dialog, bool success)
    {
        if (success)
        {
            InputManager.Instance.SetDefault();
            foreach (ControllSetting setting in settings)
            {
                setting.FetchInput();
                setting.RefreshComponents();
            }
        }
        dialog.Hide();
    }

    /// <summary>
    /// Sprawdzenie czy konfiguracja jest poprawna. Wywołuje okna dialogowe po napotkaniu błędu.
    /// </summary>
    /// <returns>True, jeżeli konfiguracja jest poprawna.</returns>
    bool ValidateSettings()
    {
        InputAction? conflictingAction = GetConflictingAction();
        if(conflictingAction.HasValue)
        {
            ShowOkDialog(string.Format(
                    LocalizationManager.Localize(conflictingActionLocalizationKey),
                    LocalizationManager.Localize($"InputAction.{conflictingAction}")));
            return false;
        }

        foreach(ControllSetting setting in settings)
        {
            if(setting.SettedKey == KeyCode.None)
            {
                if(setting.SettedAlternativeKey == KeyCode.None)
                {
                    ShowOkDialog(LocalizationManager.Localize(emptyLocalizationKey));
                }
                else
                {
                    setting.SwapKeys();
                }
            }
        }

        return true;
    }

    /// <summary>
    /// Znalezienie akcji, która używa używanego już klawisza.
    /// </summary>
    /// <returns>Akcja, która powoduje konfikt. Null, jeżeli nie ma konfliktów.</returns>
    InputAction? GetConflictingAction()
    {
        HashSet<KeyCode> set = new HashSet<KeyCode>();
        foreach(ControllSetting setting in settings)
        {
            if(set.Contains(setting.SettedKey) || set.Contains(setting.SettedAlternativeKey))
            {
                return setting.Action;
            }

            if(setting.SettedKey != KeyCode.None)
            {
                set.Add(setting.SettedKey);
            }
            if(setting.SettedAlternativeKey != KeyCode.None)
            {
                set.Add(setting.SettedAlternativeKey);
            }
        }
        return null;
    }

    /// <summary>
    /// Wyświetlenie okna dialogowego z napisem OK.
    /// </summary>
    /// <param name="message">Komunikat.</param>
    void ShowOkDialog(string message)
    {
        OkDialog dialog = Instantiate(okDialogPrefab, canvasTransform);
        dialog.autoHide = true;
        dialog.Message = message;
    }
}
