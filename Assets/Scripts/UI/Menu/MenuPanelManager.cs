﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/// <summary>
/// Zarządca paneli menu.
/// </summary>
[RequireComponent(typeof(CanvasGroup))]
public class MenuPanelManager : MonoBehaviour
{
    /// <summary>
    /// Czas trawania animacji przejścia pomiędzy panelami.
    /// </summary>
    public float animationDuration = 1f;

    /// <summary>
    /// Sprawdzenie czy aktywny panel jest pierwszym wyświetlonym panelem.
    /// </summary>
    public bool isOnFirstPanel => panelHistory.Count <= 1;

    /// <summary>
    /// Historia otwartych paneli.
    /// </summary>
    LinkedList<MenuPanel> panelHistory;
    /// <summary>
    /// Grupa, pozwalająca na szybkie zablokowanie paneli podczas animacji.
    /// </summary>
    CanvasGroup canvasGroup;

    /// <summary>
    /// Inicjalizacja. Przygotowanie listy paneli.
    /// </summary>
    void Start()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        MenuPanel[] panels = GetComponentsInChildren<MenuPanel>(true);

        if(panels.Length == 0)
        {
            Debug.LogError("No panels.");
            return;
        }
        foreach(MenuPanel panel in panels)
        {
            panel.gameObject.SetActive(false);
        }

        panels[0].OnSelect();
        panelHistory = new LinkedList<MenuPanel>();
        panelHistory.AddLast(panels[0]);
    }

    /// <summary>
    /// Ustawienie panelu.
    /// </summary>
    /// <param name="nextPanel">Nastepny panel.</param>
    public void SetPanel(MenuPanel nextPanel)
    {
        if(nextPanel != panelHistory.Last.Value)
        {
            ShowHide(nextPanel, panelHistory.Last.Value, nextPanel.direction, false);
            panelHistory.AddLast(nextPanel);
        }
    }

    /// <summary>
    /// Powrót w historii paneli.
    /// </summary>
    public void Return()
    {
        if(panelHistory.Count > 1)
        {
            MenuPanel panelToShow = panelHistory.Last.Previous.Value;
            MenuPanel panelToHide = panelHistory.Last.Value;
            if (panelToHide.IsHandlersReady(this))
            {
                ShowHide(panelToShow, panelToHide, panelToHide.direction, true);
                panelHistory.RemoveLast();
            }
        }
    }

    /// <summary>
    /// Pokazanie i ukrycie paneli.
    /// </summary>
    /// <param name="panelToShow">Panel do pokazania.</param>
    /// <param name="panelToHide">Panel do ukrycia.</param>
    /// <param name="direction">Kierunek animacji.</param>
    /// <param name="reverseDirection">Informacja, czy animacja powinna być wykonana w odwrotnym kierunku.</param>
    void ShowHide(MenuPanel panelToShow, MenuPanel panelToHide, AnimationDirection direction, bool reverseDirection)
    {
        RectTransform nextRectTransform = panelToShow.GetComponent<RectTransform>();
        RectTransform hideRectTransform = panelToHide.GetComponent<RectTransform>();
        nextRectTransform.DOKill();
        hideRectTransform.DOKill();

        panelToShow.OnSelect();
        canvasGroup.blocksRaycasts = false;

        Vector2 hiddenPosition = GetHiddenPosition(direction);
        if (reverseDirection)
        {
            hiddenPosition = -hiddenPosition;
        }
        nextRectTransform.anchoredPosition = hiddenPosition;

       hideRectTransform
            .DOAnchorPos(-hiddenPosition, animationDuration)
            .OnComplete(() => panelToHide.OnHide());
        nextRectTransform
            .DOAnchorPos(Vector2.zero, animationDuration)
            .OnComplete(() => canvasGroup.blocksRaycasts = true);
    }

    /// <summary>
    /// Obliczenie pozycji ukrycia na podstawie kierunku animacji i rozmiaru płótna.
    /// </summary>
    /// <param name="direction">Kierunek animacji.</param>
    /// <returns>Pozycja, na której panel jest ukryty.</returns>
    Vector2 GetHiddenPosition(AnimationDirection direction)
    {
        Vector2 position = Vector2.zero;
        Vector2 canvasSize = this.GetCanvasSize();
        switch (direction)
        {
            case AnimationDirection.Up:
                position.y += canvasSize.y;
                break;
            case AnimationDirection.Down:
                position.y -= canvasSize.y;
                break;
            case AnimationDirection.Left:
                position.x -= canvasSize.x;
                break;
            case AnimationDirection.Right:
                position.x += canvasSize.x;
                break;
        }
        return position;
    }

    /// <summary>
    /// Możliwe kierunki animacji.
    /// </summary>
    public enum AnimationDirection
    {
        /// <summary>
        /// Góra
        /// </summary>
        Up,
        /// <summary>
        /// Dół
        /// </summary>
        Down,
        /// <summary>
        /// Lewo
        /// </summary>
        Left,
        /// <summary>
        /// Prawo
        /// </summary>
        Right
    }
}
