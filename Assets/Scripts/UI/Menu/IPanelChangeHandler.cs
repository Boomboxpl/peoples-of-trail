﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interfejs komponentów obsługujących warunki zamknięcia panelu.
/// </summary>
public interface IPanelChangeHandler
{
    /// <summary>
    /// Sprawdzenie, czy panel może zostać zakmnięty.
    /// </summary>
    /// <param name="manager">Zarządca paneli.</param>
    /// <returns>True, gdy panel może być zamkniety.</returns>
    bool CanChange(MenuPanelManager manager);
}
