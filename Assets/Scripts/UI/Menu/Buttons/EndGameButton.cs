﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Przycisk zakończenia gry. Usuwa aktywny zapis.
/// </summary>
public class EndGameButton : ToMainMenuButton
{
    /// <summary>
    /// Powrót do menu głównego z usunieciem zapisu.
    /// </summary>
    /// <param name="dialog">Okno dialogowe.</param>
    /// <param name="success">Informacja, czy użytkownik potwierdził zakończenie gry.</param>
    protected override async void ReturnToMainMenu(Dialog dialog, bool success)
    {
        if (success)
        {
            await GameManager.Instance.DeleteCurrentGame();
        }
        base.ReturnToMainMenu(dialog, success);
    }
}
