﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Przycisk wyjścia z aplikacji.
/// </summary>
public class ExitButton : MonoBehaviour
{
    /// <summary>
    /// Obsługa naciśnięcia. Zakończenie działania aplikacji.
    /// </summary>
    public void OnClick()
    {
        Application.Quit();
    }
}
