﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Przycisk powrotu z panelu.
/// </summary>
[RequireComponent(typeof(Button))]
public class ReturnButton : MonoBehaviour
{
    /// <summary>
    /// Przycisk.
    /// </summary>
    Button button;
    /// <summary>
    /// Zarządca paneli.
    /// </summary>
    MenuPanelManager panelManager;

    /// <summary>
    /// Inicjalizacja. Przypisanie odwołań do przycisku i zarządcy paneli.
    /// </summary>
    void Awake()
    {
        button = GetComponent<Button>();
        panelManager = gameObject.GetComponentInParent<MenuPanelManager>();
    }

    /// <summary>
    /// Obsługa zmiany stanu na aktywny. Ustawienie nasłuchiwania na naciśnięcie przycisku.
    /// </summary>
    void OnEnable()
    {
        button.onClick.AddListener(panelManager.Return);
    }

    /// <summary>
    /// Obsługa zmiany stanu na nieaktywny. Usunięcie nasłuchiwania na naciśnięcie przycisku.
    /// </summary>
    void OnDisable()
    {
        button.onClick.RemoveListener(panelManager.Return);
    }
}
