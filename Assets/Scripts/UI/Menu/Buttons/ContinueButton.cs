﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

/// <summary>
/// Przycisk kontynuacji ostatniego zapisu gry.
/// </summary>
[RequireComponent(typeof(Button))]
public class ContinueButton : MonoBehaviour
{
    /// <summary>
    /// Komponent przycisku.
    /// </summary>
    Button button;

    /// <summary>
    /// Inicjalizacja. Ustawienie komponentu przycisku i reakcji na jego naciśnięcie.
    /// </summary>
    void Start()
    {
        button = GetComponent<Button>();
        button.interactable = false;
        button.onClick.AddListener(Continue);
    }

    /// <summary>
    /// Aktualizacja interaktywności przycisku. Przycisk jest aktywny, jeżeli istnieje zapis do wczytania.
    /// </summary>
    void Update()
    {
        button.interactable = IsAnySave;
    }

    /// <summary>
    /// Rozpoczęcie wczytywania ostatniego zapisu gry.
    /// </summary>
    public void Continue()
    {
        if(IsAnySave)
        {
            SaveData lastSave = PlayerSaver.Saves[0];
            GameLoadManager.LoadGame(lastSave);
        }
    }

    /// <summary>
    /// Informacja, czy istnieje jakikolwiek zapis.
    /// </summary>
    bool IsAnySave => PlayerSaver.Saves?.Count > 0;
}
