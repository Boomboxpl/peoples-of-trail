﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Przycisk powrotu do menu głównego.
/// </summary>
public class ToMainMenuButton : MonoBehaviour
{
    /// <summary>
    /// Klucz systemu tłumaczącego dla potwierdzenia powrotu do menu głównego.
    /// </summary>
    public string returnMessageKey = "InGameMenu.GoToMainMenu.Dialog.AcceptAction";

    /// <summary>
    /// Prefab okna dialogowego z przyciskami TAK/NIE.
    /// </summary>
    public YesNoDialog yesNoDialogPrefab;

    /// <summary>
    /// Płótno, na którym zostanie wyświetlone okno dialogowe.
    /// </summary>
    Transform canvasTransform;

    /// <summary>
    /// Inicjalizacja. Przypisanie płótna do okna dialogowego.
    /// </summary>
    void Awake()
    {
        canvasTransform = GetComponentInParent<Canvas>().transform;
    }

    /// <summary>
    /// Próba powrotu do menu głównego. Utworzenie okna dialogowego z potwiedzeniem powrotu.
    /// </summary>
    public void TryReturnToMainMenu()
    {
        YesNoDialog dialog = Instantiate(yesNoDialogPrefab, canvasTransform);
        dialog.Message = LocalizationManager.Localize(returnMessageKey);
        dialog.onComplete += ReturnToMainMenu;
    }

    /// <summary>
    /// Powrót do menu głównego.
    /// </summary>
    /// <param name="dialog">Okno dialogowe.</param>
    /// <param name="success">Informacja, czy użytkownik potwierdził powrót.</param>
    protected virtual void ReturnToMainMenu(Dialog dialog, bool success)
    {
        if (success)
        {
            GetComponentInParent<InGameMenuManager>()?.Hide();
            GameLoadManager.LoadMainMenu();
        }
        dialog.Hide();
    }
}
