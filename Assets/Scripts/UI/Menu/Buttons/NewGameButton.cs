﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cysharp.Threading.Tasks;

/// <summary>
/// Przycisk rozpoczęcia nowej gry.
/// </summary>
public class NewGameButton : MonoBehaviour
{
    /// <summary>
    /// Klucz systemu tłumaczącego dla prośby o wpisanie imienia.
    /// </summary>
    const string localizationKey = "NewGame.SelectName";

    /// <summary>
    /// Zarządca aktywnych konfiguracji umiejętności.
    /// </summary>
    public SkillProviderManager skillProviderManager;

    /// <summary>
    /// Prefab okna dialogowego pozwalajace na wprowadzenie imienia.
    /// </summary>
    public TextInputDialog inputDialogPrefab;

    /// <summary>
    /// Płótno, na którym zostanie wyświetlone okno dialogowe.
    /// </summary>
    Transform canvasTransform;

    /// <summary>
    /// Inicjalizacja. Przypisanie płótna do okna dialogowego.
    /// </summary>
    void Awake()
    {
        canvasTransform = GetComponentInParent<Canvas>().transform;
    }

    /// <summary>
    /// Obsługa naciśniecia przycisku. Wywołanie okna dialogowego.
    /// </summary>
    public void OnClick()
    {
        TextInputDialog dialog = Instantiate(inputDialogPrefab, canvasTransform);
        dialog.Message = LocalizationManager.Localize(localizationKey);
        dialog.onComplete += InputCompleted;
    }

    /// <summary>
    /// Zakończone działanie okna dialogowego.
    /// </summary>
    /// <param name="dialog">Okno dialogowe.</param>
    /// <param name="succes">Informacja, czy użytkownik potwierdził wprowadzanie tekstu.</param>
    void InputCompleted(Dialog dialog, bool succes)
    {
        if (succes)
        {
            TextInputDialog inputDialog = dialog as TextInputDialog;
            if (inputDialog != null)
            {
                GameLoadManager.StartNewGame(inputDialog.text, skillProviderManager);
                dialog.Hide();
            }
        }
        dialog.Hide();
    }
}
