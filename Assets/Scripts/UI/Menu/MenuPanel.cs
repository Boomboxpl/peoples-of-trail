﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// Panel menu.
/// </summary>
public class MenuPanel : MonoBehaviour
{
    /// <summary>
    /// Kierunek, z którego zostanie wykonana animacja wejścia panelu.
    /// </summary>
    public MenuPanelManager.AnimationDirection direction;

    /// <summary>
    /// Obsługa wybrania panelu.
    /// </summary>
    public virtual void OnSelect()
    {
        gameObject.SetActive(true);
        GetComponentInChildren<DefaultSelectable>()?.Select();
    }

    /// <summary>
    /// Obsługa ukrycia panelu.
    /// </summary>
    public virtual void OnHide()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Sprawdzenie, czy panel można zamknąć.
    /// </summary>
    /// <param name="manager">Zarządca paneli.</param>
    /// <returns>True, gdy można zamknąć panel.</returns>
    public bool IsHandlersReady(MenuPanelManager manager)
    {
        var handlers = GetComponentsInChildren<IPanelChangeHandler>();
        return handlers.All(handler => handler.CanChange(manager));
    }
}
