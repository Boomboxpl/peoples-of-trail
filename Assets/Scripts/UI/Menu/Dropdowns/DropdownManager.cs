﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// Zarządca menu rozwijanego.
/// </summary>
[RequireComponent(typeof(TMP_Dropdown))]
public abstract class DropdownManager : MonoBehaviour
{
    /// <summary>
    /// Komponent menu rozwijanego.
    /// </summary>
    protected TMP_Dropdown dropdown;

    /// <summary>
    /// Inicjalizacja. Uzyskanie odwołania do menu.
    /// </summary>
    protected virtual void Awake()
    {
        dropdown = GetComponent<TMP_Dropdown>();
    }

    /// <summary>
    /// Obsługa zmiany stanu na aktywny. Ustawienie nasłuchiwania na zmianę wybranego elementu.
    /// </summary>
    protected virtual void OnEnable()
    {
        dropdown.onValueChanged.AddListener(SelectedIndexChanged);
        LocalizationManager.LocalizationChanged += Refresh;
        Refresh();
    }

    /// <summary>
    /// Obsługa zmiany stanu na nieaktywny. Usunięcie nasłuchiwania na zmianę wybranego elementu.
    /// </summary>
    protected virtual void OnDisable()
    {
        dropdown.onValueChanged.RemoveListener(SelectedIndexChanged);
        LocalizationManager.LocalizationChanged -= Refresh;
    }

    /// <summary>
    /// Odświeżenie zawartości menu rozwijanego.
    /// </summary>
    protected virtual void Refresh()
    {
    }

    /// <summary>
    /// Obsługa wybrania elementu menu.
    /// </summary>
    /// <param name="index">Wybrany indeks.</param>
    protected abstract void SelectedIndexChanged(int index);
}
