﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Cysharp.Threading.Tasks;

/// <summary>
/// Zarządca menu rozwijanego z wyborem języka.
/// </summary>
[RequireComponent(typeof(TMP_Dropdown))]
public class LanguageChangeDropdown : DropdownManager
{
    /// <summary>
    /// Inicjalizacja. Ustawienie opcji dla dostępnych języków. Ustawienie reakcji na zmianę języka.
    /// </summary>
    protected override async void Awake()
    {
        base.Awake();
        dropdown.ClearOptions();
        dropdown.AddOptions(await LocalizationManager.GetLanguages());
        LocalizationManager.LocalizationChanged += Localize;
        dropdown.value = LocalizationManager.CurrentLanguage;
    }

    /// <summary>
    /// Obsługa usunięcia. Usunięcie reakcji na zmianę języka.
    /// </summary>
    void OnDestroy()
    {
        LocalizationManager.LocalizationChanged -= Localize;
    }

    /// <summary>
    /// Ustawienie aktywnego języka jako wybranej opcji.
    /// </summary>
    void Localize()
    {
        dropdown.SetValueWithoutNotify(LocalizationManager.CurrentLanguage);
    }

    /// <summary>
    /// Obsługa wybrania elementu menu. Zmiana języka.
    /// </summary>
    /// <param name="index">Wybrany indeks.</param>
    protected override void SelectedIndexChanged(int index)
    {
        LocalizationManager.SetLanguage(index).Forget();
    }
}
