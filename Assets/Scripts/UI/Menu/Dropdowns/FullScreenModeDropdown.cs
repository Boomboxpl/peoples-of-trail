﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Zarządca menu rozwijanego z trybami wyświetlania pełnego ekranu.
/// </summary>
public class FullScreenModeDropdown : DropdownManager
{
    /// <summary>
    /// Obsługa zmiany stanu na aktywny. Ustawienie opcji.
    /// </summary>
    protected override void OnEnable()
    {
        base.OnEnable();
        Refresh();
    }

    /// <summary>
    /// Obsługa wybrania elementu menu. Zmiana trybu wyświetlania pełnego ekranu.
    /// </summary>
    /// <param name="index">Wybrany indeks.</param>
    protected override void SelectedIndexChanged(int index)
    {
        Screen.fullScreenMode = (FullScreenMode)index;
    }

    /// <summary>
    /// Przygotowanie opcji.
    /// </summary>
    /// <returns>Lista opcji w wybranym języku.</returns>
    List<string> PrepareOptions()
    {
        List<string> result = new List<string>();
        foreach (string value in Enum.GetNames(typeof(FullScreenMode)))
        {
            result.Add(LocalizationManager.Localize($"FullScreenMode.{value}"));
        }
        return result;
    }

    /// <summary>
    /// Odświeżenie zawartości menu rozwijanego.
    /// </summary>
    protected override void Refresh()
    {
        base.Refresh();
        dropdown.options.Clear();
        dropdown.AddOptions(PrepareOptions());
        dropdown.SetValueWithoutNotify((int)Screen.fullScreenMode);
    }
}
