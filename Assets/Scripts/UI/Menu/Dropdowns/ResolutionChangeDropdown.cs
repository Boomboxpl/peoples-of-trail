﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

/// <summary>
/// Zarządca menu rozwijanego z wyborem rozdzielczości.
/// </summary>
public class ResolutionChangeDropdown : DropdownManager
{
    /// <summary>
    /// Format wyświetlania rozdzielczości.
    /// </summary>
    public string resolutionFormat = "{0}x{1}";

    /// <summary>
    /// Obsługiwane rozdzielczości.
    /// </summary>
    List<Resolution> resolutions;

    /// <summary>
    /// Obsługa zmiany stanu na aktywny. Ustawienie opcji dla dostępnych rozdzielczości.
    /// </summary>
    protected override void OnEnable()
    {
        base.OnEnable();
        dropdown.ClearOptions();
        resolutions = Screen.resolutions.Reverse().ToList();
        List<string> options = resolutions.Select(FormatResolution).ToList();

        int currentIndex = resolutions.FindIndex(IsCurrentResolution);
        if (currentIndex == -1)
        {
            dropdown.AddOptions(new List<string>{ FormatResolution(Screen.currentResolution) });
            currentIndex = 0;
        }
        dropdown.AddOptions(options);
        dropdown.SetValueWithoutNotify(currentIndex);
    }

    /// <summary>
    /// Obsługa wybrania elementu menu. Zmiana aktywnej rozdzielczości.
    /// </summary>
    /// <param name="index">Wybrany indeks.</param>
    protected override void SelectedIndexChanged(int index)
    {
        Resolution resolution = resolutions[index];
        if (!IsCurrentResolution(resolution))
        {
            Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreenMode);
        }
    }

    /// <summary>
    /// Formatowanie rozdzielczności na przyjazny dla użytkownika tekst.
    /// </summary>
    /// <param name="resolution">Rozdzielczość.</param>
    /// <returns>Zformatowany tekst.</returns>
    string FormatResolution(Resolution resolution) => string.Format(resolutionFormat, resolution.width, resolution.height);

    /// <summary>
    /// Sprawdzenie czy podana rozdzielczość jest aktualną rozdzielczością.
    /// </summary>
    /// <param name="resolution">Rozdzielczość.</param>
    /// <returns>True, jeżeli rozmiar aktywnej rozdzielczości jest równy rozmiarowi rozdzielczości z parametru.</returns>
    bool IsCurrentResolution(Resolution resolution) =>
        resolution.width == Screen.currentResolution.width && resolution.height == Screen.currentResolution.height;
}
