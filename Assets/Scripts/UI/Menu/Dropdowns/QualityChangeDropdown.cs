﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Zarządca menu rozwijanego z wyborem jakości tekstur.
/// </summary>
public class QualityChangeDropdown : DropdownManager
{
    /// <summary>
    /// Klucz do ustawiania preferowanej jakości.
    /// </summary>
    const string playerPrefsKey = "Quality";

    /// <summary>
    /// Inicjalizacja. Ustawienie opcji dla ustawień graficznych.
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
        Refresh();
        int prefQuality = PlayerPrefs.GetInt(playerPrefsKey, -1);
        if(prefQuality != -1)
        {
            int index = Mathf.Clamp(prefQuality, 0, QualitySettings.names.Length - 1);
            dropdown.value = index;
            SelectedIndexChanged(index);
        }
    }

    /// <summary>
    /// Obsługa wybrania elementu menu. Zmiana ustawień graficznych.
    /// </summary>
    /// <param name="index">Wybrany indeks.</param>
    protected override void SelectedIndexChanged(int index)
    {
        QualitySettings.SetQualityLevel(index);
        PlayerPrefs.SetInt(playerPrefsKey, index);
    }

    /// <summary>
    /// Przygotowanie opcji.
    /// </summary>
    /// <returns>Lista opcji w wybranym języku.</returns>
    List<string> PrepareOptions()
    {
        List<string> result = new List<string>();
        foreach (string value in QualitySettings.names)
        {
            result.Add(LocalizationManager.Localize($"MainMenu.Settings.Quality.{value}"));
        }
        return result;
    }

    /// <summary>
    /// Odświeżenie zawartości menu rozwijanego.
    /// </summary>
    protected override void Refresh()
    {
        base.Refresh();
        dropdown.ClearOptions();
        dropdown.AddOptions(PrepareOptions());
        dropdown.SetValueWithoutNotify(QualitySettings.GetQualityLevel());
    }
}
