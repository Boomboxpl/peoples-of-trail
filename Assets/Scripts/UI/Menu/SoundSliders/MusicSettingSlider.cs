﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Suwak ustawiający głośność muzyki.
/// </summary>
public class MusicSettingSlider : SoundSettingSlider
{
    /// <summary>
    /// Obsługa zmiany ustawionej głośności.
    /// </summary>
    /// <param name="value">Ustawiona głośność w przedziale (0,1).</param>
    protected override void SetVolume(float value)
    {
        SoundManager.Instance.MusicVolume = value;
    }

    /// <summary>
    /// Aktualizacja pozycji suwaka do ustawionego poziomu.
    /// </summary>
    protected override void UpdatePosition()
    {
        slider.SetValueWithoutNotify(SoundManager.Instance.MusicVolume);
    }
}
