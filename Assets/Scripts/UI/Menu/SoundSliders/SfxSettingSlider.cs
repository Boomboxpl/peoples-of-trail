﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Suwak ustawiający głośność efektów dźwiękowych.
/// </summary>
public class SfxSettingSlider : SoundSettingSlider
{
    /// <summary>
    /// Obsługa zmiany ustawionej głośności.
    /// </summary>
    /// <param name="value">Ustawiona głośność w przedziale (0,1).</param>
    protected override void SetVolume(float value)
    {
        SoundManager.Instance.SfxVolume = value;
    }

    /// <summary>
    /// Aktualizacja pozycji suwaka do ustawionego poziomu.
    /// </summary>
    protected override void UpdatePosition()
    {
        slider.SetValueWithoutNotify(SoundManager.Instance.SfxVolume);
    }
}
