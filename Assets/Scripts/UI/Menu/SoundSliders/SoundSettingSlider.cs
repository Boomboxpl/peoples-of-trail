﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Suwak ustawiający głośność dźwięku.
/// </summary>
[RequireComponent(typeof(Slider))]
public abstract class SoundSettingSlider : MonoBehaviour
{
    /// <summary>
    /// Komponent suwaka.
    /// </summary>
    protected Slider slider;

    /// <summary>
    /// Inicjalizacja. Przypisane suwaka oraz reakcji na zmianę ustawionej wartości.
    /// </summary>
    void Awake()
    {
        slider = GetComponent<Slider>();
        slider.onValueChanged.AddListener(SetVolume);
    }

    /// <summary>
    /// Obsługa aktywacji. Aktualizacja pozycji suwaka do ustawionego poziomu.
    /// </summary>
    void OnEnable()
    {
        UpdatePosition();
    }

    /// <summary>
    /// Obsługa zmiany ustawionej głośności.
    /// </summary>
    /// <param name="value">Ustawiona głośność w przedziale (0,1).</param>
    protected abstract void SetVolume(float value);

    /// <summary>
    /// Aktualizacja pozycji suwaka do ustawionego poziomu.
    /// </summary>
    protected abstract void UpdatePosition();
}
