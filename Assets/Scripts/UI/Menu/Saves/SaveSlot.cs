﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

/// <summary>
/// Zarządca wyświetlania pojedynczego zapisu.
/// </summary>
[RequireComponent(typeof(Toggle))]
public class SaveSlot : MonoBehaviour
{
    /// <summary>
    /// Tytuł.
    /// </summary>
    public TextMeshProUGUI title;
    /// <summary>
    /// Tytuł.
    /// </summary>
    public TextMeshProUGUI date;
    /// <summary>
    /// Przełącznik.
    /// </summary>
    public Toggle RadialToggle
    {
        get
        {
            if(radialToggle == null)
            {
                radialToggle = GetComponent<Toggle>();
                radialToggle.group = GetComponentsInParent<ToggleGroup>(true).FirstOrDefault();
            }
            return radialToggle;
        }
    }

    /// <summary>
    /// Przełącznik.
    /// </summary>
    Toggle radialToggle;

    /// <summary>
    /// Dane gracza.
    /// </summary>
    public SaveData Data
    {
        get => data;
        set
        {
            data = value;
            RefreshComponents();
        }
    }
    /// <summary>
    /// Dane gracza.
    /// </summary>
    SaveData data;

    /// <summary>
    /// Odświeżenie komponentów z informacjami.
    /// </summary>
    void RefreshComponents()
    {
        title.text = $"{data.name} ({data.playerData.Respect.RespectLevel})";
        date.text = data.lastSaveDate.ToString();
    }
}
