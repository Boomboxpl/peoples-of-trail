﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Cysharp.Threading.Tasks;

/// <summary>
/// Zarządca wyświetlania zapisów.
/// </summary>
public class SavesManager : MonoBehaviour
{
    /// <summary>
    /// Klucz w systemie tłumaczącym dla potwierdzenia usunięcia zapisu.
    /// </summary>
    const string deleteLocalizationKey = "MainMenu.DeleteSave.AcceptAction";

    /// <summary>
    /// Prefab zarządcy pojedynczego zapisu.
    /// </summary>
    public SaveSlot saveSlotPrefab;
    /// <summary>
    /// Obiekt zawartości, pod którą zostaną dodane zapisy.
    /// </summary>
    public Transform content;

    /// <summary>
    /// Prefab okna dialogowego z przyciskami TAK/NIE.
    /// </summary>
    public YesNoDialog yesNoDialogPrefab;
    /// <summary>
    /// Płótno, na którym zostanie wyświetlone okno dialogowe.
    /// </summary>
    Transform canvasTransform;

    /// <summary>
    /// Stworzeni zarządcy zapisów. 
    /// </summary>
    List<SaveSlot> instatiatedSlots;

    /// <summary>
    /// Inicjalizacja. Ustawienie zapisów.
    /// </summary>
    async void Awake()
    {
        await PlayerSaver.ReloadFile();
        instatiatedSlots = new List<SaveSlot>();
        canvasTransform = GetComponentInParent<Canvas>().transform;
        foreach(SaveData save in PlayerSaver.Saves)
        {
            SaveSlot instatiatedSlot = Instantiate(saveSlotPrefab, content);
            instatiatedSlot.Data = save;
            instatiatedSlot.RadialToggle.isOn = false;
            instatiatedSlots.Add(instatiatedSlot);
        }
        if(instatiatedSlots.Count > 0)
        {
            instatiatedSlots[0].RadialToggle.isOn = true;
        }
    }

    /// <summary>
    /// Wczytanie wybranego zapisu.
    /// </summary>
    public void LoadSelectedSave()
    {
        SaveSlot selectedSave = GetSelectedSave();
        if(selectedSave != null)
        {
            GameLoadManager.LoadGame(selectedSave.Data);
        }
    }

    /// <summary>
    /// Próba usunięcia zapisu. Utworzenie okna dialogowego z potwiedzeniem usunięcia.
    /// </summary>
    public void TryDeleteSelectedSave()
    {
        if(GetSelectedSave() == null)
        {
            return;
        }
        YesNoDialog dialog = Instantiate(yesNoDialogPrefab, canvasTransform);
        dialog.Message = LocalizationManager.Localize(deleteLocalizationKey);
        dialog.onComplete += DeleteSelectedSave;
    }

    /// <summary>
    /// Usunięcie zapisu, jeżeli użytkownik potwierdził usunięcie.
    /// </summary>
    /// <param name="dialog">Okno dialogowe.</param>
    /// <param name="success">Informacja, czy użytkownik potwierdził usunięcie zapisu.</param>
    void DeleteSelectedSave(Dialog dialog, bool success)
    {
        if (success)
        {
            SaveSlot save = GetSelectedSave();
            PlayerSaver.RemovePlayerData(save.Data.id).Forget();
            Destroy(save.gameObject);
            instatiatedSlots.Remove(save);
        }
        dialog.Hide();
    }

    /// <summary>
    /// Pobranie wybranego zapisu.
    /// </summary>
    /// <returns>Wybrany zapis.</returns>
    SaveSlot GetSelectedSave()
    {
        foreach (SaveSlot save in instatiatedSlots)
        {
            if (save.RadialToggle.isOn)
            {
                return save;
            }
        }
        return null;
    }
}
