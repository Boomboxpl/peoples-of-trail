﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Przycisk zamykający okno dialogowe.
/// </summary>
public class DialogCloseButton : MonoBehaviour
{
    /// <summary>
    /// Obsługa naciśnięcia. Zamknięcie okna dialogowego z niepowodzeniem.
    /// </summary>
    public void OnClick()
    {
        GetComponentInParent<Dialog>().Cancel();
    }
}
