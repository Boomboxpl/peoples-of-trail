﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// Okno dialogowe pozwalające na wprowadzenie tekstu przez użytkownika. 
/// </summary>
public class TextInputDialog : Dialog
{
    /// <summary>
    /// Maksymalna długość wprowadzanego ciągu znaków.
    /// </summary>
    public int maxLength;
    /// <summary>
    /// Tekst wprowadzony w oknie.
    /// </summary>
    public string text => inputField.text;
    /// <summary>
    /// Komponent obsługujące pole wprowadzania tekstu.
    /// </summary>
    [SerializeField] TMP_InputField inputField;
    /// <summary>
    /// Przycisk OK, służący do akceptacji wprowadzonych danych.
    /// </summary>
    [SerializeField] Button okButton;

    /// <summary>
    /// Inicjalizacja. Przypisanie reakcji na zmiany w polu wprowadzania tekstu.
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
        inputField.text = "";
        okButton.interactable = false;
        inputField.onValueChanged.AddListener(OnValueChanged);
        inputField.onSubmit.AddListener(OnSubmit);
    }

    /// <summary>
    /// Inicjalizacja. Ustawienie pola wprowadzania tekstu jako aktywnego.
    /// W wyniku działania uzytkownik nie musi sam wybierać pola za pomocą kusora.
    /// </summary>
    void Start()
    {
        inputField.Select();
        inputField.ActivateInputField();
    }

    /// <summary>
    /// Obsługa zmiany wartości pola wprowadzania tekstu.
    /// </summary>
    /// <param name="value">Nowa wartość w polu wprowadzania tekstu.</param>
    void OnValueChanged(string value)
    {
        if(maxLength > 0)
        {
            inputField.SetTextWithoutNotify(value.Truncate(maxLength));
        }
        okButton.interactable = !string.IsNullOrWhiteSpace(value);
    }

    /// <summary>
    /// Obsługa akceptacji wprowadzonych danych za pomocą klawiatury. 
    /// </summary>
    /// <param name="value">Wartość w polu wprowadzania tekstu.</param>
    void OnSubmit(string value)
    {
        if (!string.IsNullOrWhiteSpace(value))
        {
            OkClick();
        }
    }

    /// <summary>
    /// Obsługa naciśnięcia przycisku OK.
    /// </summary>
    public void OkClick()
    {
        Complete(true);
    }
}
