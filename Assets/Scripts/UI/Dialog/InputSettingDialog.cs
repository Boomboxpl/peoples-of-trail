﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Okno pozwalające użytkownikowi wybrać klaiwsz do ustawienia.
/// </summary>
public class InputSettingDialog : Dialog
{
    /// <summary>
    /// Klucz w systemie tłumaczącym dla komunikatu.
    /// </summary>
    const string localizationKey = "Dialog.Settings.Input";

    /// <summary>
    /// Słownik przekształcający kod klawisza na przyjazną dla użytkownika nazwę.
    /// </summary>
    [SerializeField] KeyMappingDictionary dictionary;

    /// <summary>
    /// Nazwa akcji.
    /// </summary>
    public string ActionName
    {
        get => actionName;
        set
        {
            actionName = value;
            RefreshMessage();
        }
    }
    /// <summary>
    /// Wybrany klawisz. Odczytywany po zakończeniu działania okna.
    /// </summary>
    public KeyCode ChoosedKey 
    {
        get => choosedKey; 
        private set
        {
            choosedKey = value;
            RefreshMessage();
        }
    }
    /// <summary>
    /// Nazwa akcji.
    /// </summary>
    string actionName;
    /// <summary>
    /// Wybrany klawisz.
    /// </summary>
    KeyCode choosedKey;
    /// <summary>
    /// Format komunikatu.
    /// </summary>
    string messageFormat;

    /// <summary>
    /// Inicjalizacja. Przygotowanie formatu komunikatu.
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
        choosedKey = KeyCode.None;
        messageFormat = LocalizationManager.Localize(localizationKey);
    }

    /// <summary>
    /// Aktualizacja pod koniec klatki. Obsługa wybrania klawisza.
    /// </summary>
    void LateUpdate()
    {
        KeyCode downKey = InputManager.GetDownButton();
        if(Validate(downKey))
        {
            StartCoroutine(WaitForKeyUp(downKey));
        }
    }

    /// <summary>
    /// Odświeżenie komunikatu.
    /// </summary>
    void RefreshMessage()
    {
        InputManager.Instance.GetInputKeys(InputAction.Back, out KeyCode backKey, out _);
        Message = string.Format(messageFormat, ActionName, dictionary.GetKeyFontString(backKey), "<sprite=33>");
    }

    /// <summary>
    /// Sprawdzenie poprawności wybranego klawisza. 
    /// Klawisz nie może być pusty oraz nie może być przyciśnięciem myszy na przycisk na ekranie.
    /// </summary>
    /// <param name="downKey">Wybrany klawisz.</param>
    /// <returns>True, gdy klawisz jest poprawny.</returns>
    bool Validate(KeyCode downKey)
    {
        ///Przycisk nie jest pusty.
        if(downKey == KeyCode.None)
        {
            return false;
        }

        ///Wciśnięty przycisk na ekranie.
        if(InputManager.Instance.GetMouseButtonDown(0)
            && EventSystem.current.currentSelectedGameObject != null
            && EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
        {
            return false;
        }

        return true;
    }

    /// <summary>
    /// Odczekanie na puszczenie klawisza.
    /// </summary>
    /// <param name="key">Wybrany klawisz.</param>
    /// <returns>Korutyna.</returns>
    IEnumerator WaitForKeyUp(KeyCode key)
    {
        yield return new WaitWhile(() => Input.GetKey(key));
        if (!completed)
        {
            ChoosedKey = key;
            Complete(true);
        }
    }
}
