﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using DG.Tweening;

/// <summary>
/// Okno dialogowe.
/// </summary>
public class Dialog : MonoBehaviour
{
    /// <summary>
    /// Ramka, w której będą wyświetlane elementy okna dialogowego.
    /// </summary>
    [SerializeField] RectTransform box;
    /// <summary>
    /// Komunikat okna dialogowego.
    /// </summary>
    public string Message
    {
        get => messageComponent.text;
        set
        {
            messageComponent.text = value;
        }
    }
    /// <summary>
    /// Komponent wyświetlający komunikat.
    /// </summary>
    public TextMeshProUGUI messageComponent;
    /// <summary>
    /// Automatyczne ukrywanie okna po zakończeniu.
    /// </summary>
    public bool autoHide = false;
    /// <summary>
    /// Operacja wykonywana po zakończeniu.
    /// Jako parametry przekazane zostanie okno dialogowe oraz informacja o powodzeniu działania.
    /// </summary>
    public Action<Dialog, bool> onComplete;
    /// <summary>
    /// Informacja, czy działanie okna zostało zakończone.
    /// </summary>
    protected bool completed;

    /// <summary>
    /// Informacja, czy okno jest w trakcie ukrywania.
    /// </summary>
    bool isHiding;

    /// <summary>
    /// Inicjalizacja. Pokazanie okna. Ustawienie reakcji na naciśnięcie klawisza zamknięcia.
    /// </summary>
    protected virtual void Awake()
    {
        completed = false;
        isHiding = false;
        InputManager.Instance.SetPressCallback(InputAction.Back, Cancel);
        Show();
    }

    /// <summary>
    /// Obsługa usunięcia obiektu. Usunięcie reakcji na naciśnięcie klawisza zamknięcia.
    /// </summary>
    void OnDestroy()
    {
        InputManager.Instance?.ClearPressCallback(InputAction.Back, Cancel);
    }

    /// <summary>
    /// Pokazanie okna.
    /// </summary>
    public void Show()
    {
        box.localScale = Vector3.zero;
        box.DOScale(Vector3.one, 0.2f);
    }

    /// <summary>
    /// Ukrycie okna. Po zakończeniu animacji niszczy obiekt.
    /// </summary>
    public void Hide()
    {
        if (isHiding)
        {
            return;
        }
        isHiding = true;
        box.DOScale(Vector3.zero, 0.2f).OnComplete(() => Destroy(gameObject));
    }

    /// <summary>
    /// Anulowanie okna.
    /// </summary>
    public void Cancel()
    {
        Complete(false);
    }

    /// <summary>
    /// Zakończenie działania okna. 
    /// Wywołanie przypisanych operacji po zakończeniu działania. 
    /// W przypadku ustawienia automatycznego ukrywania, rozpoczyna ukrywanie okna.
    /// </summary>
    /// <param name="success">Informacja, czy działanie okna się powiodło.</param>
    public void Complete(bool success)
    {
        if (!completed)
        {
            completed = true;
            StopAllCoroutines();
            onComplete?.Invoke(this, success);
            if (autoHide)
            {
                Hide();
            }
        }
    }
}
