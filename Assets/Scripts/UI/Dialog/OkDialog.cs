﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Informacyjne okno dialogowe z przyciskiem OK.
/// </summary>
public class OkDialog : Dialog
{
    /// <summary>
    /// Obsługa naciśnięcia przycisku. Zakończenie działania okna z powodzeniem.
    /// </summary>
    public void OkClick()
    {
        Complete(true);
    }
}
