﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Okno dialogowe z przyciskami TAK/NIE.
/// </summary>
public class YesNoDialog : Dialog
{
    /// <summary>
    /// Obsługa naciśnięcia przycisku TAK. Zakończenie działania okna z powodzeniem.
    /// </summary>
    public void YesClick()
    {
        Complete(true);
    }

    /// <summary>
    /// Obsługa naciśnięcia przycisku NIE. Zakończenie działania okna z niepowodzeniem. 
    /// </summary>
    public void NoClick()
    {
        Cancel();
    }
}
