﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;
using System.Text;
using Cysharp.Threading.Tasks;

/// <summary>
/// Przycisk opcji wyboru w oknie rozwiązywania zdarzeń losowych.
/// </summary>
[RequireComponent(typeof(Button))]
public class EventOptionButton : MonoBehaviour
{
    /// <summary>
    /// Operacja wywoływana po wyborze.
    /// Jako argumenty zostanie przekazana konfiguracja następnego zdarzenia losowego.
    /// </summary>
    public Action<EventProvider> onChoose;

    /// <summary>
    /// Obraz dla aktywnej opcji wyboru.
    /// </summary>
    [SerializeField] Sprite normalSprite;
    /// <summary>
    /// Obraz dla nieaktywnej opcji wyboru.
    /// </summary>
    [SerializeField] Sprite disabledSprite;
    /// <summary>
    /// Komponent wyświetlający opis opcji wyboru.
    /// </summary>
    [SerializeField] TextMeshProUGUI description;

    /// <summary>
    /// Konfiguracja opcji rozwiązania zdarzenia losowego. Przypisanie odświeża sposób wyświetlania.
    /// </summary>
    public EventOptionBase Option
    {
        get => option;
        set
        {
            option = value;
            RefreshComponents().Forget();
        }
    }
    /// <summary>
    /// Konfiguracja opcji rozwiązania zdarzenia losowego.
    /// </summary>
    EventOptionBase option;

    /// <summary>
    /// Komponent obsługujący działanie przycisku.
    /// </summary>
    Button button;
    /// <summary>
    /// Komponent wyświetlający obraz przycisku.
    /// </summary>
    Image buttonImage;

    /// <summary>
    /// Inicjalizacja. Przygotowanie przycisku i ustawienie reakcji na naciśnięcie.
    /// </summary>
    void Awake()
    {
        button = GetComponent<Button>();
        buttonImage = button.targetGraphic as Image;
        button.onClick.AddListener(OnClick);
    }

    /// <summary>
    /// Obsługa naciśnięcia przycisku. Wywołanie następnego zdarzenia.
    /// </summary>
    void OnClick()
    {
        onChoose?.Invoke(option.GetNextEventProvider());
    }

    /// <summary>
    /// Przypisanie odświeża sposóbu wyświetlania.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    async UniTask RefreshComponents()
    {
        await option.PrepareData();
        string actionDescription = LocalizationManager.Localize(option.actionDescriptionKey);
        string requirementDescription = option.requirement?.GetRequirementDescription();
        string chanceOfSuccessDescription = option.GetChanceOfSuccessDescription();
        description.text = actionDescription + requirementDescription + chanceOfSuccessDescription;
        bool isActive = true; 
        if(option.requirement != null)
        {
            isActive = await option.requirement.IsRequirementMeet();
        }
        buttonImage.sprite = isActive ? normalSprite : disabledSprite;
        buttonImage.type = Image.Type.Tiled;
        button.interactable = isActive;
    }
}
