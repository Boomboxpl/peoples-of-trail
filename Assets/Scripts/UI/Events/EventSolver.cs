﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;
using System;
using Cysharp.Threading.Tasks;

/// <summary>
/// Okno rozwiązywania zdarzeń losowych.
/// </summary>
public class EventSolver : InGameWindowManager
{
    /// <summary>
    /// Operacja wywoływana po zakończeniu rozwiązywania zdarzeń losowych.
    /// </summary>
    public Action eventEnded;

    /// <summary>
    /// Czas pokazywania i ukrywania ekranu przejścia pomiędzy zdarzeniami losowymi w sekundach.
    /// </summary>
    public float transtitionShowHideAnimationDuration = 0.5f;
    /// <summary>
    /// Opóźnienie w sekundach pomiędzy pokazywaniem i ukrywaniem ekranu przejścia pomiędzy zdarzeniami losowymi.
    /// </summary>
    public float transtitionHideDelay = 1f;
    /// <summary>
    /// Ekran przejścia pomiędzy zdarzeniami losowymi.
    /// </summary>
    public Image panelTransitionImage;

    /// <summary>
    /// Komponent obsługujący okno z paskiem przewijania.
    /// </summary>
    public ScrollRect scrollRect;
    /// <summary>
    /// Wyświetlany tytuł zdarzenia.
    /// </summary>
    public LocalizedText title;
    /// <summary>
    /// Wyświetlany opis zdarzenia.
    /// </summary>
    public TextMeshProUGUI description;
    /// <summary>
    /// Obraz zdarzenia.
    /// </summary>
    public Image image;
    /// <summary>
    /// Obiekt grupujący przyciski z opcjami wyboru rozwiązania zdarzenia losowego.
    /// </summary>
    public RectTransform buttonsContainer;
    /// <summary>
    /// Prefab opcji z statycznym prawdopodobieństwem. 
    /// </summary>
    public EventOptionButton staticOptionButtonPrefab;
    /// <summary>
    /// Prefab opcji skalowanej z umiejętnością.
    /// </summary>
    public EventOptionButton skillOptionButtonPrefab;

    /// <summary>
    /// Wymuszone zdarzenie. Będzie wywołane przed następnym zdarzeniem. 
    /// </summary>
    EventProvider forcedEvent;
    /// <summary>
    /// Odłożone zdarzenie. Przechowuje zdarzenie podczas rozwiązywania wymuszonego zdarzenia.
    /// </summary>
    EventProvider postponedEvent;
    /// <summary>
    /// Komponent kontrolujący rozmiary obrazu zdarzenia.
    /// </summary>
    LayoutElement imageLayoutElement;

    /// <summary>
    /// Inicjalizacja. Przypisanie elementu kontrolującego rozmiar obrazu zdarzenia.
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
        imageLayoutElement = image.GetComponent<LayoutElement>();
    }

    /// <summary>
    /// Wywołanie zdarzenia losowego.
    /// </summary>
    /// <param name="eventProvider">Zdarzenie losowe. Jeżeli wartość jest pusta i nie ma odłożonego zdarzenia, rozwązywanie zdarzeń losowych zostanie zakończone.</param>
    public async void CallEvent(EventProvider eventProvider)
    {
        if(forcedEvent != null && !isShown)
        {
            postponedEvent = eventProvider;
            eventProvider = forcedEvent;
            forcedEvent = null;
        }
        if (eventProvider == null)
        {
            if(postponedEvent != null)
            {
                eventProvider = postponedEvent;
                postponedEvent = null;
            }
            else
            {
                EndEventSolving();
                return;
            }
        }

        await PrepareWindow();
        eventProvider.result?.Apply();
        SetEventOnPanel(eventProvider);
    }

    /// <summary>
    /// Ustawenie zdarzenia fabularnego. Nadpisuje wymuszone zdarzenie.
    /// </summary>
    /// <param name="storyEvent">Zdarzenie fabularne.</param>
    public void SetStoryEvent(EventProvider storyEvent)
    {
        forcedEvent = storyEvent;
    }

    /// <summary>
    /// Ustawienie zdarzenia wymuszonego. Zostanie ustawione tylko wtedy, gdy wcześniej nie ustawiono wymuszenia.
    /// </summary>
    /// <param name="newForcedEvent"></param>
    /// <returns>True, jeżeli zdarzenie zostało ustawione.</returns>
    public bool SetForcedEvent(EventProvider newForcedEvent)
    {
        if(forcedEvent == null)
        {
            forcedEvent = newForcedEvent;
            return true;
        }
        return false;
    }

    /// <summary>
    /// Ustawienie wyświetlania zdarzenia.
    /// </summary>
    /// <param name="eventProvider">Zdarzenie losowe.</param>
    async void SetEventOnPanel(EventProvider eventProvider)
    {
        title.LocalizationKey = eventProvider.titleKey;
        PassiveEffectData passiveEffect = GameManager.Instance.player.data.PassiveEffect;
        float moralesDelta = passiveEffect.ApplyMorales(eventProvider.result.moralesDelta);
        int resourcesDelta = passiveEffect.ApplyResources(eventProvider.result.resourcesDelta);
        description.text = string.Format(
            LocalizationManager.Localize(eventProvider.desctiprionKey),
            $"{eventProvider.result.respectGain} {SpriteTextDefinitions.respectSprite}",
            $"{moralesDelta.ToProcentString()} {SpriteTextDefinitions.moralesSprite}",
            $"{eventProvider.result.peoplesDelta} {SpriteTextDefinitions.peoplesSprite}",
            $"{resourcesDelta} {SpriteTextDefinitions.resourcesGatherSprite}");

        image.sprite = eventProvider.sprite;
        image.preserveAspect = true;
        image.gameObject.SetActive(eventProvider.sprite != null);
        imageLayoutElement.preferredHeight = eventProvider.preferedImageHeight;
        buttonsContainer.KillChildren();
        foreach(StaticChanceEventOption option in eventProvider.staticOptions)
        {
            PrepareOption(option, staticOptionButtonPrefab);
        }

        foreach(SkillScaledEventOption option in eventProvider.skillOptions)
        {
            PrepareOption(option, skillOptionButtonPrefab);
        }
        await HidePanelTransition();
        SoundManager.Instance.PlaySound(eventProvider.eventStartSound);
    }

    /// <summary>
    /// Przygotowanie pojedynczej opcji wyboru.
    /// </summary>
    /// <param name="option">Konfiguracja opcji.</param>
    /// <param name="buttonPrefab">Prefab przycisku.</param>
    void PrepareOption(EventOptionBase option, EventOptionButton buttonPrefab)
    {
        EventOptionButton instatiatedButton = Instantiate(buttonPrefab, buttonsContainer);
        instatiatedButton.Option = option;
        instatiatedButton.onChoose += CallEvent;
    }

    /// <summary>
    /// Przygotowanie okna. 
    /// W przypadku ukrytego okna, wywołuje wyświetlenie okna.
    /// W przypadku wyświetlanego okna, wywołuje wyświetlenie ekranu przejściowego pomiędzy zdarzeniami i odczekuje na jego zakończenie.  
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    async UniTask PrepareWindow()
    {
        if (!isShown)
        {
            Show();
            panelTransitionImage.color = panelTransitionImage.color.SetAlpha(0f);
            panelTransitionImage.raycastTarget = false;
        }
        else
        {
            await ShowPanelTransition();
        }
        scrollRect.normalizedPosition = Vector2.up;
    }

    /// <summary>
    /// Zakończenie rozwiązywania zdarzń losowych. Wywołanie przypisanej operacji oraz ukrycie okna rozwiązywania zdarzeń losowych.
    /// </summary>
    void EndEventSolving()
    {
        Hide();
        eventEnded?.Invoke();
    }

    /// <summary>
    /// Wyświetlenie ekranu przejściowego pomiędzy zdarzeniami losowymi.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    async UniTask ShowPanelTransition()
    {
        panelTransitionImage.raycastTarget = true;
        scrollRect.horizontalScrollbar.interactable = false;
        scrollRect.verticalScrollbar.interactable = false;
        await panelTransitionImage.DOFade(1f, transtitionShowHideAnimationDuration);
    }

    /// <summary>
    /// Ukrycie ekranu przejściowego pomiędzy zdarzeniami losowymi.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    async UniTask HidePanelTransition()
    {
        scrollRect.horizontalScrollbar.interactable = true;
        scrollRect.verticalScrollbar.interactable = true;
        await panelTransitionImage.DOFade(0f, transtitionShowHideAnimationDuration).SetDelay(transtitionHideDelay);
        panelTransitionImage.raycastTarget = false;
    }
}
