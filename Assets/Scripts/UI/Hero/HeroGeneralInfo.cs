﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// Komponent wyświetlający informacje o poziomie rozwiju dowódcy grupy.
/// </summary>
public class HeroGeneralInfo : MonoBehaviour
{
    /// <summary>
    /// Klucz systemu tłumaczącego z formatem opisu.
    /// </summary>
    const string formatKey = "Skill.GeneralInfoFormat";
    /// <summary>
    /// Klucz systemu tłumaczącego z opisem osiągnięcia maksymalnego poziomu.
    /// </summary>
    const string maxKey = "Skill.GeneralInfoFormat.Max";
    /// <summary>
    /// Klucz systemu tłumaczącego z opisem postępu rozwoju.
    /// </summary>
    const string progressKey = "Skill.GeneralInfoFormat.Progress";

    /// <summary>
    /// Komponent wyświetlający tekst.
    /// </summary>
    TextMeshProUGUI text;

    /// <summary>
    /// Inicjalizacja. Przypisanie reakcji na zmianę języka.
    /// </summary>
    void Awake()
    {
        LocalizationManager.LocalizationChanged += RefreshText;
    }

    /// <summary>
    /// Obsługa usunięcia obiektu. Usunięcie reakcji na zmianę jeżyka oraz postępu rozwoju.
    /// </summary>
    void OnDestroy()
    {
        if (playerData != null)
        {
            playerData.Respect.stateChanged -= RefreshText;
        }
        LocalizationManager.LocalizationChanged -= RefreshText;
    }

    /// <summary>
    /// Obsługa aktywacji. Odświeżenie wyświetlanego tekstu.
    /// </summary>
    void OnEnable() => RefreshText();

    /// <summary>
    /// Ustawienie daych gracza. Ustawienie reakcji na postęp rozwoju oraz odświeżenie wyświetlanego tekstu.
    /// </summary>
    public void SetPlayerData()
    {
        playerData.Respect.stateChanged += RefreshText;
        RefreshText();
    }

    /// <summary>
    /// Odświeżenie wyświetlanego tekstu.
    /// </summary>
    void RefreshText()
    {
        if(playerData != null)
        {
            if(text == null)
            {
                text = GetComponentInChildren<TextMeshProUGUI>();
            }
            RespectData respect = playerData.Respect;
            string pointsText;
            string progressText;
            if (respect.isMaxLevel)
            {
                pointsText = LocalizationManager.Localize(maxKey);
                progressText = "";
            }
            else
            {
                pointsText = (respect.PointsForNextLevel - respect.CurrentPoints).ToString();
                progressText = string.Format(LocalizationManager.Localize(progressKey), 
                    ((float)respect.CurrentPoints / respect.PointsForNextLevel).ToProcentString());
            }

            text.text = string.Format(LocalizationManager.Localize(formatKey),
                respect.RespectLevel,
                pointsText,
                progressText,
                respect.FreeSkillPoints
                );
        }
        else
        {
            text.text = "";
        }
    }

    /// <summary>
    /// Dane gracza.
    /// </summary>
    PlayerData playerData => GameManager.Instance.player.data;
}
