﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/// <summary>
/// Wskaźnik powiadomienia o posiadaniu wolnych punktów umiejętności.
/// </summary>
public class FreeSkillPointIndicator : MonoBehaviour
{
    /// <summary>
    /// Maksymalna wartość skali podczas animacji.
    /// </summary>
    public float maxScale = 1.5f;
    /// <summary>
    /// Czas trwania jednej pętli animacji w sekundach.
    /// </summary>
    public float animationDuration = 0.5f;

    /// <summary>
    /// Obsługa aktywacji. Ustawienie skali początkowej oraz rozpoczęcie działania korutyny.
    /// </summary>
    void OnEnable()
    {
        transform.localScale = Vector3.one;
        StartCoroutine(IndicatingRoutine());
    }

    /// <summary>
    /// Obsługa dezaktywacji. Zatrzymanie korutyny animacji.
    /// </summary>
    void OnDisable()
    {
        StopAllCoroutines();
    }

    /// <summary>
    /// Korutyna animacji. Jeżeli istnieją wolne punkty umiejętności zwiększa i zmniejsza skalę obiektu, do którego dołączony jest komponent.
    /// </summary>
    /// <returns>Korutyna.</returns>
    IEnumerator IndicatingRoutine()
    {
        while (true)
        {
            RespectData respect = GameManager.Instance?.player?.data?.Respect;
            if (respect != null && respect.FreeSkillPoints > 0)
            {
                yield return transform.DOScale(maxScale, animationDuration).SetLoops(2, LoopType.Yoyo).WaitForCompletion();
            }
            yield return null;
        }
    }
}
