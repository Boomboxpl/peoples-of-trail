﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// Kropka określająca poziom umiejętności.
/// </summary>
public class SkillLevelSetterDot : MonoBehaviour
{
    /// <summary>
    /// Czas trwania animacji zmiany stanu w sekundach.
    /// </summary>
    public float animationDuration = 0.5f;

    /// <summary>
    /// Obraz dla przypisanego poziomu.
    /// </summary>
    public Image selectedDotImage;
    /// <summary>
    /// Obraz dla przypisanego i zapisanego poziomu.
    /// </summary>
    public Image savedDotImage;

    /// <summary>
    /// Ustawienie stanu nieprzypisanego poziomu.
    /// </summary>
    /// <param name="withAnimation">Informacja, czy zmiana powinna być animowana.</param>
    public void SetNonSelected(bool withAnimation = true)
    {
        selectedDotImage.transform.DOScale(0, AniamtionDuration(withAnimation));
        savedDotImage.transform.DOScale(0, AniamtionDuration(withAnimation));
    }

    /// <summary>
    /// Ustawienie stanu przypisanego poziomu.
    /// </summary>
    /// <param name="withAnimation">Informacja, czy zmiana powinna być animowana.</param>
    public void SetSelected(bool withAnimation = true)
    {
        selectedDotImage.transform.DOScale(1, AniamtionDuration(withAnimation));
        savedDotImage.transform.DOScale(0, AniamtionDuration(withAnimation));
    }

    /// <summary>
    /// Ustawienie stanu przypisanego i zapisanego poziomu.
    /// </summary>
    /// <param name="withAnimation">Informacja, czy zmiana powinna być animowana.</param>
    public void SetSaved(bool withAnimation = true)
    {
        selectedDotImage.transform.DOScale(0, AniamtionDuration(withAnimation));
        savedDotImage.transform.DOScale(1, AniamtionDuration(withAnimation));
    }

    /// <summary>
    /// Wyliczenie czasu animacji.
    /// W przyadku braku animacji zostanie zwrócone 0.
    /// W przypadku animacji zostanie zwrócona wartość z pola obiektu.
    /// </summary>
    /// <param name="isAnimated">Informacja, czy wystąpi animacja.</param>
    /// <returns>Czas animacji w sekundach.</returns>
    float AniamtionDuration(bool isAnimated)
    {
        return isAnimated ? animationDuration : 0f;
    }
}
