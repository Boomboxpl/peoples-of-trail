﻿using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Komponent pozwalający na ustawienie poziomu umiejętności.
/// </summary>
public class SkillLevelSetter : MonoBehaviour
{
    /// <summary>
    /// Przycisk zmniejszający poziom.
    /// </summary>
    public Button leftArrow;
    /// <summary>
    /// Przycisk zwiększający poziom.
    /// </summary>
    public Button rightArrow;
    /// <summary>
    /// Prefab kropki określającej poziom umiejętności.
    /// </summary>
    public SkillLevelSetterDot dotPrefab;

    /// <summary>
    /// Przypisana umiejętność.
    /// </summary>
    HeroSkill heroSkill;
    /// <summary>
    /// Konfiguracja umiejętności.
    /// </summary>
    SkillProvider skillProvider;

    /// <summary>
    /// Stworzone kropki określające poziom umięjętności.
    /// </summary>
    List<SkillLevelSetterDot> instatiatedDots;
    /// <summary>
    /// Przypisany poziom umiejętności.
    /// </summary>
    int settedLevel;

    /// <summary>
    /// Obsługa usunięcia obiektu. Usunięcie reakcji na zmianę poziomu rozwoju.
    /// </summary>
    void OnDestroy()
    {
        playerData.Respect.stateChanged -= RefreshArrowsActive;
    }

    /// <summary>
    /// Ustawienie informacji o umiejętności.
    /// </summary>
    /// <param name="manager">Zarządca okna rozwoju umiejętności.</param>
    /// <param name="skill">Umiejętnośc dowódcy grupy.</param>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async UniTask SetSkillAsync(HeroInfoManager manager, HeroSkill skill)
    {

        heroSkill = skill;
        settedLevel = skill.level;
        skillProvider = await skill.GetSkill();
        instatiatedDots = new List<SkillLevelSetterDot>(skillProvider.MaxLevel);
        for (int i = 0; i < skillProvider.MaxLevel; i++)
        {
            SkillLevelSetterDot newDot = Instantiate(dotPrefab, transform);
            newDot.transform.SetSiblingIndex(newDot.transform.parent.childCount - 2);
            instatiatedDots.Add(newDot);
        }
        RefreshDots(false);
        playerData.Respect.stateChanged += RefreshArrowsActive;
    }

    /// <summary>
    /// Usunięcie wyświetlanych informacji.
    /// </summary>
    public void ClearSkill()
    {
        heroSkill = null;
        foreach (SkillLevelSetterDot dot in instatiatedDots)
        {
            Destroy(dot.gameObject);
        }
        instatiatedDots.Clear();
    }

    /// <summary>
    /// Podniesienie poziomu umiejętności.
    /// Zmiany wykonują się tylko, jeżeli istnieją wolne punkty umiejętności.
    /// </summary>
    public void IncreaseLevel()
    {
        if(CanIncrease)
        {
            playerData.Respect.FreeSkillPoints--;
            SetLevel(settedLevel + 1);
        }
    }

    /// <summary>
    /// Obniżenie poziomu umiejętności.
    /// Zmiany wykonują się tylko, jeżeli przypisano wolne punkty umiejętności, ale ich nie zapisano.
    /// </summary>
    public void DecreaseLevel()
    {
        if (CanDecrease)
        {
            playerData.Respect.FreeSkillPoints++;
            SetLevel(settedLevel - 1);
        }
    }

    /// <summary>
    /// Ustawienie poziomu umiejętności.
    /// </summary>
    /// <param name="level">Nowy poziom umiejętności.</param>
    public void SetLevel(int level)
    {
        settedLevel = level;
        RefreshDots();
    }

    /// <summary>
    /// Zapisanie przypisanego poziomu umiejętności.
    /// </summary>
    public void ApplyLevel()
    {
        heroSkill.level = settedLevel;
        RefreshDots();
    }

    /// <summary>
    /// Odświeżenie kropek określających poziom umiejętności.
    /// </summary>
    /// <param name="withAnimation">Informacja, czy zmiana powinna być animowana.</param>
    void RefreshDots(bool withAnimation = true)
    {
        RefreshArrowsActive();
        for (int i = 0; i < skillProvider.MaxLevel; i++)
        {
            SkillLevelSetterDot dot = instatiatedDots[i];
            if (i < heroSkill.level)
            {
                dot.SetSaved(withAnimation);
            }
            else
            {
                if (i < settedLevel)
                {
                    dot.SetSelected(withAnimation);
                }
                else
                {
                    dot.SetNonSelected(withAnimation);
                }
            }
        }
    }

    /// <summary>
    /// Odświeżenie widzialności przycisków do zmiany przypisanego poziomu.
    /// </summary>
    void RefreshArrowsActive()
    {
        leftArrow.interactable = CanDecrease;
        rightArrow.interactable = CanIncrease;
    }

    /// <summary>
    /// Dane gracza.
    /// </summary>
    PlayerData playerData => GameManager.Instance.player.data;

    /// <summary>
    /// Informacja, czy można zmniejszyć poziom. True tylko, jeżeli przypisano wolne punkty umiejętności, ale ich nie zapisano.
    /// </summary>
    bool CanDecrease => (settedLevel > heroSkill.level);

    /// <summary>
    /// Informacja, czy można podnieść poziom. True tylko, jeżeli istnieją wolne punkty umiejętności.
    /// </summary>
    bool CanIncrease => (settedLevel < skillProvider.MaxLevel && playerData.Respect.FreeSkillPoints > 0);
}
