﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Cysharp.Threading.Tasks;
using System.Linq;

/// <summary>
/// Zarządca okna rozwoju umiejętności.
/// </summary>
public class HeroInfoManager : InGameWindowManager
{
    /// <summary>
    /// Prefab komponentu wyświetlającego informacje o rozwoju dowódcy.
    /// </summary>
    public HeroGeneralInfo generalInfoPrefab;
    /// <summary>
    /// Prefab komponentu pozwalającego na zarządzanie umiejętnością.
    /// </summary>
    public HeroSkillSetter skillSetterPrefab;
    /// <summary>
    /// Obiekt, pod którym będzie wyświetlana zawartość okna.
    /// </summary>
    public Transform content;

    /// <summary>
    /// Lista utworzonych komponentów zarządzania umiejętnościami.
    /// </summary>
    List<HeroSkillSetter> skillSetters;

    /// <summary>
    /// Wygenerowanie animacji wyświetlania okna.
    /// </summary>
    /// <returns>Animacja.</returns>
    protected override Tween GetShowTween()
    {
        InputManager.Instance.SetPressCallback(InputAction.Back, Hide);
        InputManager.Instance.SetPressCallback(InputAction.Hero, Hide);
        return base.GetShowTween();
    }

    /// <summary>
    /// Wygenerowanie animacji ukrywania okna.
    /// </summary>
    /// <returns>Animacja.</returns>
    protected override Tween GetHideTween()
    {
        InputManager.Instance.ClearPressCallback(InputAction.Back, Hide);
        InputManager.Instance.ClearPressCallback(InputAction.Hero, Hide);
        return base.GetHideTween();
    }

    /// <summary>
    /// Ustawienie danych gracza do wyświetlania.
    /// </summary>
    public void SetPlayerData()
    {
        ClearContent();
        Instantiate(generalInfoPrefab, content).SetPlayerData();
        skillSetters = new List<HeroSkillSetter>(playerData.HeroSkills.Count);
        foreach(var skill in playerData.HeroSkills)
        {
            HeroSkillSetter newSetter = Instantiate(skillSetterPrefab, content);
            newSetter.SetSkillData(this, skill).Forget();
            skillSetters.Add(newSetter);
        }
    }

    /// <summary>
    /// Zapisanie przydzielonych poziomów umiejętności.
    /// </summary>
    public void ApplyAllLevels()
    {
        foreach(SkillLevelSetter levelSetter in skillSetters.Select(skillSetter => skillSetter.skillLevel))
        {
            levelSetter.ApplyLevel();
        }
    }

    /// <summary>
    /// Wyczyszczenie zawartości okna.
    /// </summary>
    void ClearContent()
    {
        content.KillChildren();
    }

    /// <summary>
    /// Dane gracza.
    /// </summary>
    PlayerData playerData => GameManager.Instance.player.data;
}
