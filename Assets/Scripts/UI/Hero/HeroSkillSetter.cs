﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Cysharp.Threading.Tasks;

/// <summary>
/// Komponent pozwalający na zarządzanie umiejętnością.
/// </summary>
public class HeroSkillSetter : MonoBehaviour
{
    /// <summary>
    /// Przyrostek do klucza systemu tłumaczącego z opisem umiejętności.
    /// Pełny klucz zostaje utworzony przez dodanie przyrostka do klucza nazwy umiejętności.
    /// </summary>
    const string descriptionKeySuffix = ".Description";

    /// <summary>
    /// Ikona.
    /// </summary>
    public Image icon;
    /// <summary>
    /// Komponent wyświetlający nazwę umiejętności.
    /// </summary>
    public LocalizedText skillName;
    /// <summary>
    /// Komponent pozwalający ustawić poziom umiejętności.
    /// </summary>
    public SkillLevelSetter skillLevel;
    /// <summary>
    /// Zarządca wyświetlania informacji o elemencie interfejsu.
    /// </summary>
    public UIInfoProvider infoProvider;

    /// <summary>
    /// Ustawienie danych umiejętności.
    /// </summary>
    /// <param name="manager">Zarządca okna rozwoju umiejętności.</param>
    /// <param name="skill">Umiejętnośc dowódcy grupy.</param>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async UniTask SetSkillData(HeroInfoManager manager, HeroSkill skill)
    {
        SkillProvider skillProvider = await skill.GetSkill();
        icon.sprite = skillProvider.icon;
        skillName.LocalizationKey = skillProvider.LocalizationKey;
        infoProvider.messageKey = skillProvider.LocalizationKey + descriptionKeySuffix;
        await skillLevel.SetSkillAsync(manager, skill);
    }
}
