﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// Domyślny element interfejsu, który jest ustawiany jako wybrany po każdej aktywacji obiektu.
/// </summary>
public class DefaultSelectable : MonoBehaviour
{
    /// <summary>
    /// Komponent z możliwością wybrania.
    /// </summary>
    Selectable selectable;

    /// <summary>
    /// Inicjalizacja. Przypisanie odwołania do wybieralnego komponentu obiektu.
    /// </summary>
    void Awake()
    {
        selectable = GetComponent<Selectable>();
    }

    /// <summary>
    /// Obsługa aktywacji. Wybranie komponentu.
    /// </summary>
    void OnEnable() => Select();

    /// <summary>
    /// Aktualizacja w każdej klatce. Element zostanie wybrany, jeżeli nie będzie istniał inny aktywny wybrany element.
    /// </summary>
    void Update()
    {
        if(EventSystem.current.currentSelectedGameObject == null)
        {
            Select();
        }
    }

    /// <summary>
    /// Wybranie komponentu.
    /// </summary>
    public void Select()
    {
        EventSystem.current.SetSelectedGameObject(null);
        selectable.Select();
    }
}
