﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using System;

/// <summary>
/// Element blokujący interakcje. Blokuje wszystkie elementy interfejsu wizualnie znajdjące się za nim.
/// </summary>
[RequireComponent(typeof(Image))]
public class RaycastBlocker : MonoBehaviour, IPointerClickHandler
{
    /// <summary>
    /// Operacja wykonywana po naciśnięciu.
    /// </summary>
    public Action onClick;
    /// <summary>
    /// Informacja, czy przypisane operacje po naciśnięciu zostaną wywołane.
    /// </summary>
    public bool isCallingClicks = true;
    /// <summary>
    /// Czas wyświetlania i ukrywania w sekundach.
    /// </summary>
    public float animationDuration = 0.5f;

    /// <summary>
    /// Obraz blokujący elementy.
    /// </summary>
    Image image;
    /// <summary>
    /// Domyślna wartość kanału alfaobrazu podczas wyświetlania.
    /// </summary>
    float defaultAlpha;

    /// <summary>
    /// Inicjalizacja. Ustawienie obrazu jako przeźroczystego.
    /// </summary>
    void Awake()
    {
        image = GetComponent<Image>();
        defaultAlpha = image.color.a;
        image.color = image.color.SetAlpha(0f);
    }

    /// <summary>
    /// Wyświetlanie.
    /// </summary>
    public void Show()
    {
        image.raycastTarget = true;
        image.DOFade(defaultAlpha, animationDuration);
    }

    /// <summary>
    /// Ukrywanie.
    /// </summary>
    public void Hide()
    {
        image.raycastTarget = false;
        image.DOFade(0f, animationDuration);
    }

    /// <summary>
    /// Obsługa naciśnięcia.
    /// </summary>
    /// <param name="eventData">Dane o zdarzeniu.</param>
    public void OnPointerClick(PointerEventData eventData)
    {
        if (isCallingClicks)
        {
            onClick?.Invoke();
        }
    }
}
