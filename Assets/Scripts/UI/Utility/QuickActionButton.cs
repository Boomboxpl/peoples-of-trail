﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Przycisk z skrótem klawiszowym.
/// </summary>
[RequireComponent(typeof(Button))]
public class QuickActionButton : MonoBehaviour
{
    /// <summary>
    /// Akcja przypisana do skrótu klawiszowego.
    /// </summary>
    public InputAction listenedAction;

    /// <summary>
    /// Komponent przycisku.
    /// </summary>
    Button button;

    /// <summary>
    /// Inicjalizacja. Przypisanie przycisku.
    /// </summary>
    void Awake()
    {
        button = GetComponent<Button>();
    }

    /// <summary>
    /// Obsługa aktywacji. Rozpoczęcie korutyny obsługącej nasłuchiwanie akcji.
    /// </summary>
    void OnEnable()
    {
        StartCoroutine(StateHandlingRoutine());
    }

    /// <summary>
    /// Obsługa dezaktywacji. Zakończenie korutyn.
    /// </summary>
    void OnDisable()
    {
        StopAllCoroutines();
    }

    /// <summary>
    /// Próba naciśnięcia przycisku za pomocą skrótu klawiszowego.
    /// Zmiany zostaną wykonane tylko, jeżeli przycisk ma włączone interakcje. 
    /// </summary>
    void TryPress()
    {
        if(button.interactable == true)
        {
            button.onClick.Invoke();
        }
    }

    /// <summary>
    /// Korutyna obsługąca nasłuchiwanie akcji.
    /// Ustawia nasłuchiwanie, jeżeli nie jest aktywne żadne okno.
    /// Usuwa nasłuchiwanie, jeżeli pojawi się nowe okno.
    /// </summary>
    /// <returns>Korutyna.</returns>
    IEnumerator StateHandlingRoutine()
    {
        while (true)
        {
            UIManager uIManager = GameManager.Instance?.uiManager;
            if(uIManager != null)
            {
                bool IsClearInLoop = uIManager.IsClear;
                if (IsClearInLoop)
                {
                    InputManager.Instance.SetPressCallback(listenedAction, TryPress);
                }
                else
                {
                    InputManager.Instance.ClearPressCallback(listenedAction, TryPress);
                }
                yield return new WaitWhile(() => IsClearInLoop == uIManager.IsClear);
            }
            else
            {
                yield return null;
            }
        }
    }
}
