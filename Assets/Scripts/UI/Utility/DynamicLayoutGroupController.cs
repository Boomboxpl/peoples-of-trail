﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Element układający elemnty interfejsu w układzie horyzontalnym i wertylkalnym w zależności od proporcji ekranu.
/// </summary>
public class DynamicLayoutGroupController : MonoBehaviour, IScreenChangeHandler
{
    /// <summary>
    /// Odsunięcie elementów od krawędzi obiektu.
    /// </summary>
    public RectOffset padding;
    /// <summary>
    /// Progowa wartość proporcji ekranu. Po jej przekroczeniu następuje zmiana układu.
    /// </summary>
    public float thresholdAspectRatio;

    /// <summary>
    /// Informacja, czy aktualny układ jest horyzontalny.
    /// </summary>
    bool isHorizontal;
    /// <summary>
    /// Aktualny układ.
    /// </summary>
    HorizontalOrVerticalLayoutGroup currentLayoutGroup;

    /// <summary>
    /// Inicjalizacja. Przypisanie aktualnego układu.
    /// </summary>
    void Awake()
    {
        currentLayoutGroup = GetComponent<HorizontalOrVerticalLayoutGroup>();
        if(currentLayoutGroup == null)
        {
            SetHorizontal();
        }
        PresetCurrentLayoutGroup();
    }

    /// <summary>
    /// Obsługa zmiany rozmiaru ekranu. Ustawienie najbardziej dopasowanego układu.
    /// </summary>
    /// <param name="aspectRatio">Nowe proporcje ekranu.</param>
    public void OnScreenChanged(float aspectRatio)
    {
        if(aspectRatio < thresholdAspectRatio && isHorizontal)
        {
            SetVertical();
        }
        else if(aspectRatio > thresholdAspectRatio && !isHorizontal)
        {
            SetHorizontal();
        }
    }

    /// <summary>
    /// Ustawinie układu horyzontalnego.
    /// </summary>
    void SetHorizontal()
    {
        DeleteCurrentLayoutGroup();
        isHorizontal = true;
        currentLayoutGroup = gameObject.AddComponent<HorizontalLayoutGroup>();
        PresetCurrentLayoutGroup();
    }

    /// <summary>
    /// Ustawienie układu wertykalnego.
    /// </summary>
    void SetVertical()
    {
        DeleteCurrentLayoutGroup();
        isHorizontal = false;
        currentLayoutGroup = gameObject.AddComponent<VerticalLayoutGroup>();
        PresetCurrentLayoutGroup();
    }

    /// <summary>
    /// Usunięcie aktualnego układu.
    /// </summary>
    void DeleteCurrentLayoutGroup()
    {
        LayoutGroup currentLayoutGroup = GetComponent<LayoutGroup>();
        if(currentLayoutGroup != null)
        {
            DestroyImmediate(currentLayoutGroup);
        }
    }

    /// <summary>
    /// Ustawienie parametrów aktualnego układu. Zapewnia, że różne układy będą miały te same parametry.
    /// </summary>
    void PresetCurrentLayoutGroup()
    {
        currentLayoutGroup.padding = padding;
        currentLayoutGroup.childForceExpandWidth = false;
        currentLayoutGroup.childForceExpandHeight = false;
        currentLayoutGroup.childControlWidth = false;
        currentLayoutGroup.childControlHeight = false;
        currentLayoutGroup.childAlignment = TextAnchor.MiddleCenter;
    }
}
