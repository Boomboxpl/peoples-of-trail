﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;
using Cysharp.Threading.Tasks;
using System.Threading;

/// <summary>
/// Zarządca zapisywania postępów gracza.
/// </summary>
public static class PlayerSaver
{
    /// <summary>
    /// Nazwa pliku zapisu.
    /// </summary>
    const string savesFileName = "saves.bin";

    /// <summary>
    /// Zapisy gry.
    /// </summary>
    public static List<SaveData> Saves { get; private set; }
    /// <summary>
    /// Informacja, czy wszystkie zapisy zostały załadowane.
    /// </summary>
    static bool isLoaded;

    /// <summary>
    /// Informacja, czy aplikacja jest w trakcie operacji na pliku zapisu.
    /// </summary>
    static bool isSaving;
    /// <summary>
    /// Token anulowania zapisu.
    /// </summary>
    static CancellationTokenSource cancelationTokenSource;

    /// <summary>
    /// Wymuszenie pononego załadowania pliku. 
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public static async UniTask ReloadFile()
    {
        isLoaded = false;
        await TryLoadFile();
    }

    /// <summary>
    /// Konstruktor statyczny.
    /// </summary>
    static PlayerSaver()
    {
        Saves = new List<SaveData>();
        isLoaded = false;
    }

    /// <summary>
    /// Uzyskanie nieużywanego identyfikatora.
    /// </summary>
    /// <returns>Zadanie asynchroniczne zwracające identyfikator.</returns>
    public static async UniTask<int> GetUnusedId()
    {
        if (!await TryLoadFile())
        {
            return 0;
        }
        IEnumerable<int> savesIndices = Saves.Select(data => data.id);
        for (uint i = 1; i < uint.MaxValue; i++)
        {
            if (!savesIndices.Contains((int)i))
            {
                return (int)i;
            }
        }
        return 0;
    }

    /// <summary>
    /// Odczytanie zapisu gry z pliku.
    /// </summary>
    /// <param name="id">Identyfikator zapisu gry.</param>
    /// <returns>Zadanie asynchroniczne zwracające zapis gry.</returns>
    public static async UniTask<SaveData> GetPlayerData(int id)
    {
        if (!await TryLoadFile())
        {
            return null;
        }
        int dataIndex = Saves.FindIndex(data => data.id == id);
        if (dataIndex != -1)
        {
            return Saves[dataIndex];
        }

        return null;
    }

    /// <summary>
    /// Usunięcie zapisu z pliku.
    /// </summary>
    /// <param name="id">Identyfikator zapisu.</param>
    public static async UniTask RemovePlayerData(int id)
    {
        if (!await TryLoadFile())
        {
            return;
        }
        int dataIndex = Saves.FindIndex(data => data.id == id);
        if (dataIndex != -1)
        {
            Saves.RemoveAt(dataIndex);
        }
        isSaving = true;
        await BinnarySerializer.Serialize(savesFileName, Saves);
        isSaving = false;
    }

    /// <summary>
    /// Zapisanie zapisu gyr do pliku.
    /// </summary>
    /// <param name="dataToSave">Dane zapisu.</param>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async static UniTask Save(SaveData dataToSave)
    {
        await TryLoadFile();
        int id = dataToSave.id;
        dataToSave.lastSaveDate = DateTime.Now;
        int dataIndex = Saves.FindIndex(data => data.id == id);
        if (dataIndex != -1)
        {
            Saves[dataIndex] = dataToSave;
        }
        else
        {
            Saves.Add(dataToSave);
        }
        cancelationTokenSource?.Cancel();
        await UniTask.WaitUntil(() => !isSaving);
        cancelationTokenSource = new CancellationTokenSource();
        isSaving = true;
        await BinnarySerializer.Serialize(savesFileName, Saves, cancelationTokenSource.Token);
        cancelationTokenSource.Dispose();
        cancelationTokenSource = null;
        isSaving = false;
    }

    /// <summary>
    /// Próba załadowania pliku zzapisami gry.
    /// </summary>
    /// <returns>Zadanie asynchroniczne zwracające true, jeżeli plik został załadowany.</returns>
    static async UniTask<bool> TryLoadFile()
    {
        if (File.Exists(BinnarySerializer.SavePath(savesFileName)))
        {
            if (!isLoaded)
            {
                await UniTask.WaitUntil(() => !isSaving);
                isSaving = true;
                Saves = await BinnarySerializer.Deserialize<List<SaveData>>(savesFileName);
                isSaving = false;
                if (Saves != null)
                {
                    Saves = Saves.OrderByDescending(save => save.lastSaveDate).ToList();
                }
                isLoaded = true;
            }
            return true;
        }
        return false;
    }
}
