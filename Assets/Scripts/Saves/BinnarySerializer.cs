﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using System.Threading;

/// <summary>
/// Zarządca zapisywania obiektów do plików binarnych.
/// </summary>
public static class BinnarySerializer
{
    /// <summary>
    /// Serializacja obiektu.
    /// </summary>
    /// <param name="fileName">Nazwa pliku.</param>
    /// <param name="objectToSave">Obiekt, który zostanie zapisany.</param>
    /// <param name="cancellationToken">Token anulowania zapisu.</param>
    /// <typeparam name="T">Typ obiektu.</typeparam>
    /// <returns>Zadanie asynchroniczne.</returns>
    public static async UniTask Serialize<T>(string fileName, T objectToSave, CancellationToken cancellationToken = default)
    {
        string savePath = SavePath(fileName);
        try
        {
            await UniTask.RunOnThreadPool(() =>
            {
                using (FileStream fs = File.OpenWrite(savePath))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(fs, objectToSave);
                    fs.Flush();
                    fs.Close();
                }
            },
            true,
            cancellationToken);
        }
        catch (Exception e)
        {
            Debug.LogWarning($"Failed serialize {fileName}: {e.Message}");
        }
    }

    /// <summary>
    /// Deserializacja obiektu z pliku.
    /// </summary>
    /// <param name="fileName">Nazwa pliku.</param>
    /// <param name="cancellationToken">Token anulowania odczytu.</param>
    /// <typeparam name="T">Typ obiektu.</typeparam>
    /// <returns>Zadanie asynchroniczne zwracające odczytany obiekt.</returns>
    public static async UniTask<T> Deserialize<T>(string fileName, CancellationToken cancellationToken = default)
    {
        string savePath = SavePath(fileName);
        T result = default;
        if (File.Exists(savePath))
        {
            try
            {
                await UniTask.RunOnThreadPool(() =>
                {
                    BinaryFormatter formatter = new BinaryFormatter();

                    using (FileStream fs = File.OpenRead(savePath))
                    {
                        result = (T)formatter.Deserialize(fs);
                        fs.Flush();
                        fs.Close();
                    }
                },
                true,
                cancellationToken);
            }
            catch (Exception e)
            {
                Debug.LogWarning($"Failed deserialize {fileName}: {e.Message}");
            }
        }
        return result;
    }

    /// <summary>
    /// Wygenerowanie ścieżki zapisu pliku w katalogu na dane nieulotne.
    /// </summary>
    /// <param name="fileName">Nazwa pliku.</param>
    /// <returns>Ścieżka do pliku.</returns>
    public static string SavePath(string fileName) => $"{Application.persistentDataPath}/{fileName}";
}
