﻿Shader "Custom/ColorDisplacementShader"
{
    Properties
    {
        [MainColor]_Tint ("Tint", Color) = (1,1,1,1)
        [HideInInspector]_MainTex ("Albedo", 2D) = "white" {}
        _FirstColor("First", Color) = (1,1,1,1)
        _LastColor("Last", Color) = (1,1,1,1)
        _DisplacementTime("DisplacementTime", Float) = 1
        [PerRendererData]_DisplacementTex("Displacement", 2D) = "black" {}
        _StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 1
    }
    SubShader
    {
        Tags
        {
            "Queue" = "Transparent+1"
            "IgnoreProjector" = "True"
            "RenderType" = "Transparent"
            "PreviewType" = "Plane"
            "CanUseSpriteAtlas" = "True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        Stencil{
		    Ref [_Stencil]
		    Comp [_StencilComp]
		}

        CGPROGRAM

        #pragma surface surf NoLighting alpha

        //Definicja zmiennych.
        sampler2D _MainTex;
        sampler2D _DisplacementTex;
        fixed4 _Tint;
        fixed4 _FirstColor;
        fixed4 _LastColor;
        float _DisplacementTime;

        struct Input
        {
            float2 uv_MainTex;
        };

        //Shader wierzchołków i fragmentów.
        void surf (Input IN, inout SurfaceOutput o)
        {
            //Główna tekstura z podbiciem.
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Tint;
            //Obliczenie przesunięcia jako części dziesiętnej sumy przesunięcia z tekstury i z czasu.
            float currentDisplacement = frac(tex2D(_DisplacementTex, IN.uv_MainTex).r + _DisplacementTime);
            ///Przeskalowanie przesunięcia tak by dla 0.5 maiał wartość 0, a dla 0 i 1 wartość 1.
            currentDisplacement = abs(currentDisplacement - 0.5f) * 2;
            //Dodanie interpolowanego koloru.
            c *= lerp(_LastColor, _FirstColor, currentDisplacement);
            //Zwrócenie wartości.
            o.Albedo = c.rgb;
            o.Alpha = c.a;
        }

        //Funkja przeliczająca ostateczną wartość koloru po zignorowaniu oświetlenia.
        fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten) {
            return fixed4(s.Albedo, s.Alpha);
        }

        ENDCG

    }
    FallBack "Diffuse"
}
