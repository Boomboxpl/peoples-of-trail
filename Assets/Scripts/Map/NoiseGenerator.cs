﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

/// <summary>
/// Generator szumów.
/// </summary>
[Serializable]
public class NoiseGenerator
{
    /// <summary>
    /// Stałe przesunięcie. Funkcja generacji szumów daje lepsze efekty z przesunięciem po przecinku.
    /// </summary>
    const float noiseShift = 0.01f;

    /// <summary>
    /// Skala generowanego szumu. Zwiększanie powoduje powstawanie większych obszarów o podobnej wartości.
    /// </summary>
    public float scale = 1f;
    /// <summary>
    /// Różnorodność generowanych szumów. Zwiększanie może generować bardziej różniące się szumy.
    /// </summary>
    public float diversity = 100;

    /// <summary>
    /// Przesunięcie względem początku szumu. Pozwala na wygenerowanie szumu o dowolnym ziarnie i odczytanie jego wartości.
    /// </summary>
    protected Vector2 offset = Vector2.zero;

    /// <summary>
    /// Wygenerowanie tekstury do testów. Plik z obrazem zapisywany w folderze Test.
    /// </summary>
    /// <param name="size">Rozmiar wygenerowanej tekstury.</param>
    /// <param name="name">Nazwa wygenerowanego pliku.</param>
    public void GenereteTestTexture(Vector2Int size, string name)
    {
        size *= 10;
        Texture2D texture = new Texture2D(size.x, size.y);
        for (float y = 0.1f; y < size.y + 0.1f; y++)
        {
            for (float x = 0.1f; x < size.x + 0.1f; x++)
            {
                Vector2 position = new Vector2(x / 10f, y / 10f);
                float perlinValue = Get(position);
                texture.SetPixel((int)x, (int)y, new Color(perlinValue, perlinValue, perlinValue, 1));
            }
        }
        byte[] bytes = texture.EncodeToPNG();
        var dirPath = "Test/";
        Directory.CreateDirectory(dirPath);
        File.WriteAllBytes(dirPath + name + ".png", bytes);
    }

    /// <summary>
    /// Przygotowanie ziarna szumu.
    /// </summary>
    /// <param name="randomGenerator">Generator liczb pseudolosowych.</param>
    public virtual void PrepareNoise(System.Random randomGenerator)
    {
        offset = new Vector2(randomGenerator.NextFloat(0, diversity), randomGenerator.NextFloat(0, diversity));
    }

    /// <summary>
    /// Uzyskanie wartości szumu na określonej pozycji.
    /// </summary>
    /// <param name="position">Pozycja w obrazie szumu.</param>
    /// <returns>Wartość szumu.</returns>
    public virtual float Get(Vector2 position)
    {
        Vector2 effectivePosition = position + offset + new Vector2(noiseShift, noiseShift);
        if (scale > 0)
        {
            effectivePosition /= scale;
        }
        return Mathf.Clamp01(Mathf.PerlinNoise(effectivePosition.x, effectivePosition.y));
    }

    /// <summary>
    /// Uzyskanie wartości szumu na określonej pozycji.
    /// </summary>
    /// <param name="positionX">Pozycja w osi X.</param>
    /// <param name="positionY">Pozycja w osi Y.</param>
    /// <returns>Wartość szumu.</returns>
    public float Get(float positionX, float positionY)
    {
        return Get(new Vector2(positionX, positionY));
    }

}
