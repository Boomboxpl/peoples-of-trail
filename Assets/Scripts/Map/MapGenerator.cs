﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using Cysharp.Threading.Tasks;
using UnityEngine.AddressableAssets;
using System.Collections.ObjectModel;

/// <summary>
/// Generator map.
/// </summary>
public class MapGenerator : MonoBehaviour
{
    /// <summary>
    /// Lista możliwych poziomów zasobów.
    /// </summary>
    public List<ThresholdedTileSet> resourcesLevels;
    /// <summary>
    /// Generator szumu dla zasobów.
    /// </summary>
    public NoiseGenerator resourcesNoise;
    /// <summary>
    /// Generator szumu dla wysokości terenu.
    /// </summary>
    public NoiseGenerator heightNoise;

    /// <summary>
    /// Obraz dla muru. Mur odgranicza poziomy.
    /// </summary>
    public AssetReferenceSprite wallSprite;
    /// <summary>
    /// Obraz dla bramy. Brama pozwala przejść przez mur i jest punktem kontrolnym.
    /// </summary>
    public AssetReferenceSprite gateSprite;

    /// <summary>
    /// Zdarzenia losowe użyte podczas generacji terenu.
    /// </summary>
    HashSet<EventProviderReference> usedEvents;

    /// <summary>
    /// Inicjalizacja obiektu. Walidacja skonfigurowania co najmniej jednego zestawu zasobności. Sortowanie listy zasobności.
    /// </summary>
    void Awake()
    {
        if (resourcesLevels.Count < 0)
        {
            Debug.LogError("Empty resources list");
        }
        resourcesLevels.Sort();
        if (resourcesLevels[0].threshold > 0)
        {
            Debug.LogWarning("No default resources level, setting lowest as default");
            resourcesLevels[0].threshold = 0;
        }
        usedEvents = new HashSet<EventProviderReference>();
    }

    /// <summary>
    /// Funkcja zapisująca wygenerowane szumy w przedziale (0,20) w plikach w folderze Test.
    /// Pozwala na testowanie generacji szumu.
    /// </summary>
    [ContextMenu("Generate test noise")]
    void GenerateTestTextures()
    {
        Vector2Int size = new Vector2Int(20, 20);
        resourcesNoise.GenereteTestTexture(size, "resorces");
        heightNoise.GenereteTestTexture(size, "height");
    }

    /// <summary>
    /// Załadowanie poziomu. W przypadku ukończonego poziomu zostanie wygenerowany identyczny na podstawie ziaren generacji.
    /// </summary>
    /// <param name="level">Poziom.</param>
    /// <returns>Zadanie asynchroniczne zwracające przygotowany poziom.</returns>
    public async UniTask<MapLevel> LoadLevel(MapLevel level)
    {
        if (level.tiles == null || level.isFinished)
        {
            int gatePosition = level.gatePosition;
            level = await GenerateMap(level.Dimensions.x, level.Dimensions.y, level.resourcesSeed, level.heightSeed);
            if (gatePosition > 0)
            {
                level.gatePosition = gatePosition;
            }
            ReleaseTilesets();
        }
        return level;
    }

    /// <summary>
    /// Procedura generowania poziomu.
    /// </summary>
    /// <param name="mapWidth">Szerokość poziomu.</param>
    /// <param name="mapHeight">Wysokość poziomu.</param>
    /// <param name="resourcesSeed">Ziarno generatora liczb pseudolosowych. Jeżeli null, użyte zostanie domyślne ziarno.</param>
    /// <param name="heightSeed">Ziarno generatora liczb pseudolosowych. Jeżeli null, użyte zostanie domyślne ziarno.</param>
    /// <returns>Zadanie asynchroniczne zwracające wygenerowany poziom</returns>
    public async UniTask<MapLevel> GenerateMap(int mapWidth, int mapHeight, int? resourcesSeed = null, int? heightSeed = null)
    {
        resourcesSeed = resourcesSeed ?? (int)DateTime.Now.Ticks;
        System.Random resourcesRandomGenerator = new System.Random(resourcesSeed.Value);
        resourcesNoise.PrepareNoise(resourcesRandomGenerator);

        heightSeed = heightSeed ?? (int)DateTime.Now.Ticks;
        System.Random heightRandomGenerator = new System.Random(resourcesSeed.Value);
        heightNoise.PrepareNoise(heightRandomGenerator);

        System.Random eventRandomGenerator = new System.Random((int)DateTime.Now.Ticks);

        List<MapTile> resultTiles = new List<MapTile>();

        for (int i = 0; i < mapWidth * mapHeight; i++)
        {
            Vector2 position = new Vector2(i % mapWidth, i / mapWidth);
            float resources = resourcesNoise.Get(position);

            TileSet tileSet = await GetTileSet(resources);
            if (tileSet == null)
            {
                Debug.LogError("No tileset found.");
                continue;
            }

            float heightTerrarian = heightNoise.Get(position);
            HeightSet heightSet = tileSet.GetHeightSet(heightTerrarian);
            if (heightSet == null)
            {
                Debug.LogError("No sprite found.");
                continue;
            }

            resultTiles.Add(
                new MapTile(heightSet.sprites.Random(heightRandomGenerator),
                tileSet.resourcesToGather,
                heightSet.resourcesToLost,
                await PrepareEvent(tileSet, heightSet, eventRandomGenerator))
            );
        }
        ReadOnlyCollection<EventProviderReference> castleEvents = 
            (await GameManager.Instance.mapManager.GetCommonEvents()).CastleEvents;
        MapEvent castleEvent = new MapEvent(castleEvents.Random(eventRandomGenerator), 1f);

        UnloadUsedEvents();

        return new MapLevel(
            resultTiles,
            new Vector2Int(mapWidth, mapHeight),
            resourcesSeed.Value,
            heightSeed.Value,
            resourcesRandomGenerator.Next(1, mapWidth - 1),
            new GateTile(gateSprite, castleEvent),
            new WallTile(wallSprite)
            );
    }

    /// <summary>
    /// Pobranie zestawu kafelków dla terenu o podanej zasobności.
    /// </summary>
    /// <param name="resources">Poziom zasobów.</param>
    /// <returns>Zadanie asynchroniczne zwracające zestaw kafelków o danej zasobności.</returns>
    public async UniTask<TileSet> GetTileSet(float resources)
    {
        for (int i = resourcesLevels.Count - 1; i >= 0; i--)
        {
            TileSetReference tilesetReference = resourcesLevels[i].GetThresholded(resources);
            if(tilesetReference != null)
            {
                TileSet result = tilesetReference.Asset as TileSet;
                if (result == null)
                {
                    result = await tilesetReference.LoadAssetAsync<TileSet>();
                }
                return result;
            }
        }
        return null;
    }

    /// <summary>
    /// Zwolnienie zestawów kafelków.
    /// </summary>
    public void ReleaseTilesets()
    {
        foreach(var reference in resourcesLevels)
        {
            if(reference.property.Asset != null)
            {
                reference.property.ReleaseAsset();
            }
        }
    }

    /// <summary>
    /// Przygotowanie zdarzenia losowego.
    /// Zdarzenie losowe wybierane z pul zdarzeń ogólnych i typowych dla terenu.
    /// Pule przed wyborem losowego elementu są filtrowane za pomocą wylosowanego progu.
    /// </summary>
    /// <param name="tileSet">Zestaw kafelków o danej zasobności.</param>
    /// <param name="heightSet">Zestaw kafelków o danej zasobności i wysokości.</param>
    /// <param name="random">Generator liczb pseudolosowych</param>
    /// <returns>Zadanie asynchroniczne zwracające zdarzenie losowe mapy.</returns>
    async UniTask<MapEvent> PrepareEvent(TileSet tileSet, HeightSet heightSet, System.Random random)
    {
        float randomValue = (float)random.NextDouble();
        IEnumerable<ThresholdedEvent> possibleEvents = (await GameManager.Instance.mapManager.GetCommonEvents()).GeneralEvents
            .Union(heightSet.events)
            .Union(tileSet.events)
            .Where(mapEvent => mapEvent.threshold <= randomValue);


        EventProviderReference eventProviderReference = possibleEvents.Random(random)?.property;
        if (eventProviderReference == null)
        {
            return null;
        }
        if(eventProviderReference.Asset == null)
        {
            await eventProviderReference.LoadAssetAsync();
            usedEvents.Add(eventProviderReference);
        }

        EventProvider eventProvider = (EventProvider)eventProviderReference.Asset;
        float activationChance = Mathf.Lerp(
            eventProvider.minActivationChance,
            eventProvider.maxActivationChance,
            (float)random.NextDouble());

        return new MapEvent(eventProviderReference, activationChance);
    }

    /// <summary>
    /// Wyładowanie użytych odwołań do zdarzeń losowych.
    /// </summary>
    void UnloadUsedEvents()
    {
        foreach(EventProviderReference eventReference in usedEvents)
        {
            eventReference.ReleaseAsset();
        }
        usedEvents.Clear();
    }
}