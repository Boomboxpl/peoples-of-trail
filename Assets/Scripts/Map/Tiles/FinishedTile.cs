﻿using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Ukończony kafelek.
/// </summary>
public class FinishedTile : MapTile
{
    /// <summary>
    /// Klucz systemu tłumaczącego z opisem kafelka.
    /// </summary>
    const string localizationKey = "TileInfo.Finished";

    /// <summary>
    /// Konstruktor.
    /// </summary>
    public FinishedTile() : base(null, 0, 0, null)
    {
        IsMovable = false;
        IsFoggable = false;
        IsEffectTarget = false;
    }

    /// <summary>
    /// Pobranie informacji o kafelku.
    /// </summary>
    /// <returns>Zadanie asynchroniczne zwracające opis słowny ukończonego kafelka.</returns>
    public override UniTask<string> GetInfo()
    {
        return new UniTask<string>(LocalizationManager.Localize(localizationKey));
    }
}
