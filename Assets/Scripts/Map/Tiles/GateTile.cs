﻿using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

/// <summary>
/// Kafelek zamku z bramą.
/// </summary>
[System.Serializable]
public class GateTile : WallTile
{
    /// <summary>
    /// Klucz systemu tłumaczącego z opisem kafelka.
    /// </summary>
    const string localizationKey = "TileInfo.Gate";

    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="terrarianSprite">Obraz dla kafelka.</param>
    /// <param name="castleEvent">Zdarzenie zamkowe. Wywoływane zawsze po wejściu. Jeżeli wystąpi zdarzenie fabularne, zdarzenie zamkowe zostanie wywołane po jego zakończeniu.</param>
    public GateTile(AssetReferenceSprite terrarianSprite, MapEvent castleEvent): base(terrarianSprite)
    {
        IsMovable = true;
        mapEvent = castleEvent;
    }

    /// <summary>
    /// Pobranie informacji o kafelku.
    /// </summary>
    /// <returns>Zadanie asynchroniczne zwracające opis słowny zamku.</returns>
    public override UniTask<string> GetInfo()
    {
        return new UniTask<string>(LocalizationManager.Localize(localizationKey));
    }

    /// <summary>
    /// Obsługa wejścia na kafelek. Wywołanie zdarzenia fabularnego i zamkowego.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public override async UniTask OnStep()
    {
        GameManager gameManager = GameManager.Instance;
        MapManager mapManager = gameManager.mapManager;
        CommonEvents commonEvents = await mapManager.GetCommonEvents();
        EventProviderReference eventReference = commonEvents.GetStoryEvent(mapManager.GetLevelCount() - 1);
        SerializableAsset<EventProvider> eventAsset = new SerializableAsset<EventProvider>();
        eventAsset.Set(eventReference);
        gameManager.uiManager.eventSolver.SetForcedEvent(await eventAsset.GetAsync());
        gameManager.reachedCheckpoint = true;
        await base.OnStep();
    }
}
