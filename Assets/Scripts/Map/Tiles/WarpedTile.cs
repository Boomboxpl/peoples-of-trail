﻿using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Kafelek wypaczony, znajdujący się poza planszą.
/// </summary>
public class WarpedTile : MapTile
{
    /// <summary>
    /// Klucz systemu tłumaczącego z opisem kafelka.
    /// </summary>
    const string localizationKey = "TileInfo.Warped";

    /// <summary>
    /// Konstruktor.
    /// </summary>
    public WarpedTile(): base(null,0,0,null)
    {
        IsMovable = false;
        IsFoggable = false;
        IsEffectTarget = false;
    }

    /// <summary>
    /// Pobranie informacji o kafelku.
    /// </summary>
    /// <returns>Zadanie asynchroniczne zwracające opis kafelka poza planszą.</returns>
    public override UniTask<string> GetInfo()
    {
        return new UniTask<string>(LocalizationManager.Localize(localizationKey));
    }
}
