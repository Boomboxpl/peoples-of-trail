﻿using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

/// <summary>
/// Kafelek z murem.
/// </summary>
[System.Serializable]
public class WallTile : MapTile
{
    /// <summary>
    /// Klucz systemu tłumaczącego z opisem kafelka.
    /// </summary>
    const string localizationKey = "TileInfo.Wall";

    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="terrarianSprite">Obraz dla kafelka.</param>
    public WallTile(AssetReferenceSprite terrarianSprite) : base(terrarianSprite, 0, 0, null)
    {
        IsMovable = false;
        IsFoggable = false;
        IsEffectTarget = false;
    }

    /// <summary>
    /// Pobranie informacji o kafelku.
    /// </summary>
    /// <returns>Zadanie asynchroniczne zwracające opis słowny kafelka muru.</returns>
    public override UniTask<string> GetInfo()
    {
        return new UniTask<string>(LocalizationManager.Localize(localizationKey));
    }
}
