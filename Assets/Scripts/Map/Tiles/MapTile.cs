﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.AddressableAssets;
using Cysharp.Threading.Tasks;

/// <summary>
/// Kafelek znajdujący się na mapie.
/// </summary>
[Serializable]
public class MapTile
{
    /// <summary>
    /// Format tekstowej informacji o kafelku. Określa sposób prezentacji szczegółowych informacji po przytrzymaniu kursora nad kafelkiem. 
    /// Wyświetla odpowiednie ikony. Argumenty:\n
    /// 0 - zyskiwane zasoby.\n
    /// 1 - tracone zasoby.\n
    /// 2 - prawdopodobieństwo wystąpienia zdarzenia losowego.\n
    /// 3 - poziom niebezpieczeństwa.
    /// </summary>
    const string stringFormat = SpriteTextDefinitions.resourcesGatherSprite + SpriteTextDefinitions.arrowUp + " {0}\n" +
        SpriteTextDefinitions.resourcesLoseSprite + SpriteTextDefinitions.arrowDown + " {1}\n" +
        SpriteTextDefinitions.diceSprite + " {2}\n" +
        SpriteTextDefinitions.skullSprite + " {3}";

    /// <summary>
    /// Odwołanie do obrazu kafelka.
    /// </summary>
    SerializableAsset<Sprite> sprite;

    /// <summary>
    /// Liczba zasobów dodawanych do zasobów gracza po przejściu przez kafelek.
    /// </summary>
    public int resourcesToGather;
    /// <summary>
    /// Liczba zasobów usuwanych z zasobów gracza po przejściu przez kafelek.
    /// </summary>
    public int resourcesToLose;

    /// <summary>
    /// Określa sposób wyświetlania mgły nad kafelkiem.
    /// </summary>
    public FogOfWarState fogState;

    /// <summary>
    /// Pozycja na mapie kafelków.
    /// </summary>
    [NonSerialized] public Vector3Int tilemapPosition;
    /// <summary>
    /// Informacja, czy można poruszyć się na kafelek. Jeżeli false, gracz nie może poruszyć się na kafelek.
    /// </summary>
    public bool IsMovable { get; protected set; }
    /// <summary>
    /// Informacja, czy mgła wojny może przykryć kafelek. Jeżeli false, mgła wojny zawsze odkrywa kafelek.
    /// </summary>
    public bool IsFoggable { get; protected set; }
    /// <summary>
    /// Informacja, czy efekty mapy działają na kafelek.
    /// </summary>
    public bool IsEffectTarget { get; protected set; }
    /// <summary>
    /// Zdarzenie przypisane do kafelka.
    /// </summary>
    public MapEvent mapEvent { get; protected set; }

    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="terrarianSprite">Obraz dla kafelka.</param>
    /// <param name="resourcesToGather">Zasoby dodawane po przejściu.</param>
    /// <param name="resourcesToLose">Zasoby usuwane po przejściu.</param>
    /// <param name="mapEvent">Zdarzenie losowe przypisane do kafelka.</param>
    public MapTile(AssetReferenceSprite terrarianSprite, int resourcesToGather, int resourcesToLose, MapEvent mapEvent)
    {
        sprite = new SerializableAsset<Sprite>();
        SetSprite(terrarianSprite);
        this.resourcesToGather = resourcesToGather;
        this.resourcesToLose = resourcesToLose;
        this.mapEvent = mapEvent;
        fogState = FogOfWarState.Full;
        IsMovable = true;
        IsFoggable = true;
        IsEffectTarget = true;
    }

    /// <summary>
    /// Pobranie informacji o kafelku.
    /// </summary>
    /// <returns>Zadanie asynchroniczne zwracające opis parametrów kafelka.</returns>
    public virtual async UniTask<string> GetInfo()
    {
        float dangerLevel = 0f;
        if(mapEvent?.eventProvider != null)
        {
            dangerLevel = (await mapEvent.eventProvider.GetAsync()).dangerLevel;
        }
        return string.Format(stringFormat, 
            resourcesToGather, 
            resourcesToLose, 
            (mapEvent?.activationChance ?? 0f).ToProcentString(),
            dangerLevel.ToProcentString()
            );
    }

    /// <summary>
    /// Obsługa wejścia na kafelek. Wywołanie zdarzenia losowego, jeśli to możliwe.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public virtual async UniTask OnStep()
    {
        bool isCalled = false;
        if (mapEvent != null)
        {
            isCalled = await mapEvent.Call();
        }

        if (isCalled)
        {
            ClearEvent();
        }
        else
        {
            GameManager.Instance.uiManager.eventSolver.CallEvent(null);
        }
    }

    /// <summary>
    /// Pobranie obrazu dla kafelka.
    /// </summary>
    /// <returns>Zadanie asynchroniczne zwracające obraz.</returns>
    public async UniTask<Sprite> GetSprite()
    {
        Sprite result = await sprite.GetAsync();
        return result;
    }

    /// <summary>
    /// Pobranie odwołania do obrazu dla kafelka.
    /// </summary>
    /// <returns>Odwołanie do obrazu.</returns>
    public AssetReferenceSprite GetSpriteReference() => sprite.GetReference() as AssetReferenceSprite;

    /// <summary>
    /// Ustawienie obrazu dla kafelka.
    /// </summary>
    /// <param name="spriteReference">Odwołanie do obrazu dla kafelka.</param>
    public void SetSprite(AssetReferenceSprite spriteReference) => this.sprite.Set(spriteReference);

    /// <summary>
    /// Usunięcie zdarzenia losowego.
    /// </summary>
    public void ClearEvent()
    {
        mapEvent = null;
    }
}
