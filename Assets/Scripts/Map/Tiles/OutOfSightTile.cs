﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Kafelek poza zasięgiem wzroku.
/// </summary>
[Serializable]
public class OutOfSightTile : MapTile
{
    /// <summary>
    /// Klucz systemu tłumaczącego z opisem kafelka.
    /// </summary>
    const string localizationKey = "TileInfo.OutOfSight";

    /// <summary>
    /// Konstruktor.
    /// </summary>
    public OutOfSightTile() : base(null, 0, 0, null)
    {
        IsMovable = false;
        IsFoggable = false;
        IsEffectTarget = false;
    }

    /// <summary>
    /// Pobranie informacji o kafelku.
    /// </summary>
    /// <returns>Zadanie asynchroniczne zwracające opis słowny kafelka poza zasięgiem wzroku..</returns>
    public override UniTask<string> GetInfo()
    {
        return new UniTask<string>(LocalizationManager.Localize(localizationKey));
    }
}
