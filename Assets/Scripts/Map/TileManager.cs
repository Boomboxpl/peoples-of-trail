﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

/// <summary>
/// Zarządca dynamicznego tworzenia obiektów kafelków na podstawie obiektów Sprite. 
/// Zapobiega zbędnej alokacji obiektów przy tworzeniu dużej ilości tych samych kafelków.
/// </summary>
public class TileManager : MonoBehaviour
{
    /// <summary>
    /// Schemat kafelka kopiowany podczas tworzenia nowego wystąpienia Sprite.
    /// </summary>
    public Tile tilePrefab;

    /// <summary>
    /// Używane kafelki.
    /// </summary>
    Dictionary<Sprite, Tile> tiles = new Dictionary<Sprite, Tile>();

    /// <summary>
    /// Wyszukiwanie kafelka dla Sprite. W przypadku braku gotowego kafelka tworzy nowy i go zwraca.
    /// </summary>
    /// <param name="sprite">Obraz wyświetlany przez kafelek.</param>
    /// <returns>Kafelek z podanym obrazem.</returns>
    public Tile GetTile(Sprite sprite)
    {
        if (!tiles.ContainsKey(sprite))
        {
            Tile tile = Instantiate(tilePrefab);
            tile.sprite = sprite;
            tiles.Add(sprite, tile);
        }
        return tiles[sprite];
    }
}
