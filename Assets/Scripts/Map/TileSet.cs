﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Zestaw konfiguracji kafelków o określonej zasobności.
/// </summary>
[CreateAssetMenu(fileName = "TileSet", menuName = "TileSet")]
public class TileSet : ScriptableObject
{
    /// <summary>
    /// Liczba zasobów do zebrania na danym terenie.
    /// </summary>
    public int resourcesToGather;
    /// <summary>
    /// Zdarzenia losowe przypisane do zasobności terenu.
    /// </summary>
    public List<ThresholdedEvent> events;
    /// <summary>
    /// Lista poziomów wysokości terenu w zestawie.
    /// </summary>
    public List<ThresholdedHeights> heightLevels;

    /// <summary>
    /// Inicjalizacja obiektu. Walidacja skonfigurowania co najmniej jednego zestawu wysokości. Sortowanie listy wysokości. 
    /// </summary>
    void Awake()
    {
        if (!Application.isPlaying)
        {
            return;
        }
        if (heightLevels.Count < 0)
        {
            Debug.LogError("Empty levels list");
        }
        heightLevels.Sort();
        if (heightLevels[0].threshold > 0)
        {
            Debug.LogWarning("No default level, setting lowest as default");
            heightLevels[0].threshold = 0;
        }
    }

    /// <summary>
    /// Pobranie losowego zestawu dla terenu o podanej wysokości.
    /// </summary>
    /// <param name="height">Wysokość.</param>
    /// <returns>Losowy zestaw z pośród najbardziej pasujących do wysokości terenu.</returns>
    public HeightSet GetHeightSet(float height)
    {
        for (int i = heightLevels.Count - 1; i >= 0; i--)
        {
            HeightSet result = heightLevels[i].GetThresholded(height);
            if (result != null)
            {
                return result;
            }
        }
        return null;
    }
}