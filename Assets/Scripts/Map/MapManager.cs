﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.Linq;
using Cysharp.Threading.Tasks;
using System.Threading.Tasks;
using UnityEngine.AddressableAssets;
#if UNITY_EDITOR
using UnityEditor;
#endif 

/// <summary>
/// Zarządca mapy. Inicuje generowanie mapy i ustawia ją na scenie.
/// </summary>
public class MapManager : MonoBehaviour
{
    /// <summary>
    /// Mapa kafelków rysowanych na scenie.
    /// </summary>
    public Tilemap terrarianTileMap;
    /// <summary>
    /// Podstawowy rozmiar poziomu.
    /// Takiego rozmiaru będzie pierwszy poziom.
    /// </summary>
    public Vector2Int baseLevelSize;
    /// <summary>
    /// Zmiana rozmiaru poziomu z każdym kolejnym poziomem.
    /// </summary>
    public Vector2Int levelSizeDelta;

    /// <summary>
    /// Lista wygenerowanych poziomów.
    /// </summary>
    public List<MapLevel> levels { get; private set; }

    /// <summary>
    /// Generator mapy.
    /// </summary>
    public MapGenerator mapGenerator { get; private set; }

    /// <summary>
    /// Odwołanie do grupy zdarzeń losowych niezależnych od typu terenu.
    /// </summary>
    [SerializeField] CommonEventsReference commonEventsReference;

    /// <summary>
    /// Zarządca kafelków.
    /// </summary>
    TileManager tileManager;
    /// <summary>
    /// Zarządca mgły wojny.
    /// </summary>
    FogOfWarManager fogOfWar;
    /// <summary>
    /// Grupa zdarzeń losowych niezależnych od typu terenu.
    /// </summary>
    CommonEvents commonEvents;

    /// <summary>
    /// Inicjalizacja obiektu. Pobranie referencji do generatora map i zarządcy kafelków.
    /// </summary>
    void Awake()
    {
        levels = new List<MapLevel>();
        mapGenerator = GetComponent<MapGenerator>();
        tileManager = GetComponent<TileManager>();
        fogOfWar = GetComponentInChildren<FogOfWarManager>();
        fogOfWar.mapManager = this;
    }

#if UNITY_EDITOR
    /// <summary>
    /// Funkcja przypisująca pozycje na mapie do kafelka w edytorze. Używana tylko do testów.
    /// </summary>
    void OnDrawGizmos()
    {
        if (levels == null)
        {
            return;
        }
        GUIStyle style = new GUIStyle();
        style.normal.textColor = Color.white;
        Gizmos.color = Color.white;
        Handles.color = Color.white;
        foreach (var level in levels)
        {
            Vector2Int position = new Vector2Int();
            for (position.y = 0; position.y < level.Dimensions.y; position.y++)
            {
                for (position.x = level.StartX; position.x < level.Dimensions.x; position.x++)
                {
                    MapTile tile = GetTileAt(position);
                    Vector3 worldPosition = terrarianTileMap.CellToWorld((Vector3Int)position);
                    Handles.Label(worldPosition, position.ToString(), style);
                }
            }
        }
    }
#endif

    /// <summary>
    /// Pobranie liczby poziomów.
    /// </summary>
    /// <returns>Liczba poziomów. Jeżeli poziomy nie zostały zainicjowane, zwraca 0.</returns>
    public int GetLevelCount() => levels?.Count ?? 0;

    /// <summary>
    /// Pobranie grupy zdarzeń losowych niezależnych od typu terenu.
    /// </summary>
    /// <returns>Zadanie asynchroniczne zwracające grupę zdarzeń losowych niezależnych od typu terenu.</returns>
    public async UniTask<CommonEvents> GetCommonEvents()
    {
        if(commonEvents == null)
        {
            commonEvents = await commonEventsReference.LoadAssetAsync<CommonEvents>();
            commonEventsReference.ReleaseAsset();
        }
        return commonEvents;
    }

    /// <summary>
    /// Procedura tworzenia nowego poziomu.
    /// Rozmiar poziomu zostanie wyliczony na podstawie poprzednich poziomów.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public UniTask CreateNewLevel()
    {
        Vector2Int levelDimension = baseLevelSize + levelSizeDelta * GetLevelCount();
        return CreateNewLevel(levelDimension);
    }

     /// <summary>
    /// Procedura tworzenia nowego poziomu.
    /// </summary>
    /// <param name="levelDimension">Rozmiar poziomu wyrżony w liczbie kafelków.</param>
    /// <returns>Zadanie asynchroniczne</returns>
    public async UniTask CreateNewLevel(Vector2Int levelDimension)
    {
        MapLevel lastLevel = levels.LastOrDefault();
        if (lastLevel != null)
        {
            fogOfWar.UpdateFinished(lastLevel);
            lastLevel.SetFinished();
        }
        MapLevel newLevel = await mapGenerator.GenerateMap(levelDimension.x, levelDimension.y);
        mapGenerator.ReleaseTilesets();
        levels.Add(newLevel);
        DrawLevel(newLevel, levels.LastOrDefault()).Forget();
    }

    /// <summary>
    /// Przygotowanie poziomów z listy. W przypadku pustej listy zostanie utworzony nowy poziom.
    /// </summary>
    /// <param name="mapLevels">Lista poziomów.</param>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async UniTask SetLevels(List<MapLevel> mapLevels)
    {
        levels = mapLevels;
        terrarianTileMap.ClearAllTiles();
        if (GetLevelCount() == 0)
        {
            await CreateNewLevel();
        }
        else
        {
            for (int i = GetLevelCount() - 1; i >= 0; i--)
            {
                await SetLevel(i);
            }
        }
    }

    /// <summary>
    /// Ponowne ustawienie sposobu rysowania mapy dla ostatniego poziomu.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async UniTask RedrawLastLevel()
    {
        MapLevel lastLevel = levels.LastOrDefault();
        MapLevel previousLevel = null;
        if(levels.Count > 1)
        {
            previousLevel = levels[levels.Count - 1];
        }
        await DrawLevel(lastLevel, previousLevel);
    }

    /// <summary>
    /// Aktualizacja mgły wojny.
    /// </summary>
    public void UpdateFog() => fogOfWar.UpdateFog().Forget();

    /// <summary>
    /// Usunięcie zdarzenia z wybranego kafelka
    /// </summary>
    /// <param name="position">Pozycja kafelka na siatce.</param>
    public void ClearEvent(Vector2Int position)
    {
        MapTile tile = GetTileAt(position);
        if (tile != null)
        {
            tile.ClearEvent();
        }
    }

    /// <summary>
    /// Pobranie kafelków znajdujących się wokół kafelka centralnego w odległości mniejszej od podanego dystansu.
    /// </summary>
    /// <param name="centerPosition">Pozycja centralnego kafelka.</param>
    /// <param name="distance">Dystans od kafelka centalnego.</param>
    /// <returns>Lista kafelków wokół kafelka centralnego.</returns>
    public List<MapTile> GetTilesInDistance(Vector2Int centerPosition, int distance)
    {
        List<MapTile> result = new List<MapTile>();
        for (int y = centerPosition.y - distance - 1; y < centerPosition.y + distance + 1; y++)
        {
            for (int x = centerPosition.x - distance - 1; x < centerPosition.x + distance + 1; x++)
            {
                Vector2Int tilePosition = new Vector2Int(x, y);
                int tileDistance = CubeDistance(ToCubeCoordinates(centerPosition), ToCubeCoordinates(tilePosition));
                if (tileDistance <= distance)
                {
                    result.Add(GetTileAt(tilePosition, fogMask: false));
                }
            }
        }
        return result;
    }

    /// <summary>
    /// Sprawdzenie czy pozycje są sąsiadami.
    /// </summary>
    /// <param name="homePosition">Pozycja pierwszego kafelka.</param>
    /// <param name="neighbourPosition">Pozycja drugiego kafelka.</param>
    /// <returns>True, gdy pozycje są sąsiadami.</returns>
    public bool IsNeighbour(Vector2Int homePosition, Vector2Int neighbourPosition)
    {
        Vector2Int delta = homePosition - neighbourPosition;
        if (Mathf.Abs(delta.y) > 1)
        {
            return false;
        }

        if (delta.y == 0)
        {
            if (Mathf.Abs(delta.x) > 1)
            {
                return false;
            }
        }
        else
        {
            int parity = neighbourPosition.y % 2 - 1;
            if (delta.x < parity || delta.x > parity + 1)
            {
                return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Pobranie kafelka na pozycji w przestrzeni świata.
    /// </summary>
    /// <param name="position">Pozycja kafelka w świecie.</param>
    /// <param name="fogMask">Informacja, czy podczas pobierania kafelka przykrytego mgłą zwrócić mgłę.</param>
    /// <param name="forceCurrentLevel">Informacja, czy wymusić by kafelka znajdowała sie na aktualnym poziomie gry.</param>
    /// <returns>Kafelek na wskazanej pozycji. Wartość pusta, jeżeli pozycja była poza zakresem mapy.</returns>
    public MapTile GetTileAt(Vector2 position, bool fogMask = true, bool forceCurrentLevel = false)
    {
        return GetTileAt((Vector2Int)terrarianTileMap.WorldToCell(position), fogMask, forceCurrentLevel);
    }

    /// <summary>
    /// Pobranie kafelka na pozycji w przestrzeni siatki kafelków.
    /// </summary>
    /// <param name="position">Pozycja kafelka na siatce.</param>
    /// <param name="fogMask">Informacja, czy podczas pobierania kafelka przykrytego mgłą zwrócić mgłę.</param>
    /// <param name="forceCurrentLevel">Informacja, czy wymusić by kafelka znajdowała sie na aktualnym poziomie gry.</param>
    /// <returns>Kafelek na wskazanej pozycji. Wartość pusta, jeżeli pozycja była poza zakresem mapy.</returns>
    public MapTile GetTileAt(Vector2Int position, bool fogMask = true, bool forceCurrentLevel = false)
    {
        MapTile tile = null;
        if (GetLevelCount() != 0)
        {
            MapLevel hoveredLevel = GetLevelOnHeight(position.y);
            if (hoveredLevel != null)
            {
                if (!forceCurrentLevel || hoveredLevel == levels.LastOrDefault())
                {
                    int startX = hoveredLevel.StartX;
                    Vector2Int levelPosition = new Vector2Int(
                        position.x - startX,
                        position.y - MapHeight(hoveredLevel) + hoveredLevel.Dimensions.y + 1);
                    tile = hoveredLevel.GetTile(levelPosition);
                }
            }
        }

        if (tile == null)
        {
            tile = new WarpedTile();
        }
        else if (fogMask && tile.IsFoggable && tile.fogState != FogOfWarState.None)
        {
            tile = new OutOfSightTile();
        }

        return tile;
    }

    /// <summary>
    /// Przygotowanie pojedynczego poziomu.
    /// </summary>
    /// <param name="levelIndex">Indeks poziomu w liście poziomów.</param>
    /// <returns>Zadanie asynchroniczne.</returns>
    async UniTask SetLevel(int levelIndex)
    {
        bool isFinished = levels[levelIndex].isFinished;
        levels[levelIndex] = await mapGenerator.LoadLevel(levels[levelIndex]);
        MapLevel previousLevel = (levelIndex >= 1 && levelIndex < GetLevelCount()) ? levels[levelIndex - 1] : null;
        await DrawLevel(levels[levelIndex], previousLevel);
        if (isFinished)
        {
            fogOfWar.UpdateFinished(levels[levelIndex]);
            levels[levelIndex].SetFinished();
        }
    }

    /// <summary>
    /// Ustawienie poziomu do rysowania na scenie.
    /// </summary>
    /// <param name="level">Poziom.</param>
    /// <param name="previousLevel">Poprzedni poziom.</param>
    /// <returns>Zadanie asynchroniczne.</returns>
    async UniTask DrawLevel(MapLevel level, MapLevel previousLevel)
    {
        int startY = MapHeight(level) - level.Dimensions.y - 1;
        //Przerysowanie poprzedniego muru, gdy wystąpi różnica w szerokości poziomów.
        if (previousLevel != null && previousLevel.Dimensions.x != level.Dimensions.x)
        {
            previousLevel.WallWidth = level.Dimensions.x;
            await DrawWall(startY - 1, previousLevel);
        }

        List<UniTask> tileTasks = new List<UniTask>(level.tiles.Count);
        for (int i = 0; i < level.tiles.Count; i++)
        {
            tileTasks.Add(DrawTile(level, i, startY));
        }

        level.WallWidth = level.Dimensions.x;
        UpdateFog();
        await DrawWall(level.Dimensions.y + startY, level);
        await UniTask.WhenAll(tileTasks);
    }

    /// <summary>
    /// Ustawienie rysowania kafelka na scenie.
    /// </summary>
    /// <param name="level">Poziom gry.</param>
    /// <param name="index">Indeks kafelka w tablicy kafelków poziomu.</param>
    /// <param name="startY">Wartość w osi Y, od której rozpoczyna się rysowanie poziomów.</param>
    /// <returns>Zadanie asynchroniczne.</returns>
    async UniTask DrawTile(MapLevel level, int index, int startY)
    {
        Vector3Int position = new Vector3Int(index % level.Dimensions.x + level.StartX, index / level.Dimensions.x + startY, 0);
        MapTile mapTile = level.tiles[index];
        mapTile.tilemapPosition = position;
        Tile tile = tileManager.GetTile(await mapTile.GetSprite());
        terrarianTileMap.SetTile(position, tile);
        fogOfWar.Redraw(mapTile);
    }

    /// <summary>
    /// Ustawienie muru do rysowania na scenie.
    /// </summary>
    /// <param name="positionY">Pozycja muru w osi Y.</param>
    /// <param name="level">Poziom do którego należy mur.</param>
    /// <returns>Zadanie asynchroniczne.</returns>
    async UniTask DrawWall(int positionY, MapLevel level)
    {
        Tile wallTile = tileManager.GetTile(await level.Wall.GetSprite());
        for (int i = 0; i < level.WallWidth; i++)
        {
            terrarianTileMap.SetTile(new Vector3Int(i + level.WallStartX, positionY, 0), wallTile);
        }
        Tile gateTile = tileManager.GetTile(await level.Gate.GetSprite());
        terrarianTileMap.SetTile(new Vector3Int(level.gatePosition + level.StartX, positionY, 0), gateTile);
    }

    /// <summary>
    /// Wyliczenie pozycji sześciennej kafelka.
    /// </summary>
    /// <param name="position">Pozycja kafelka na siatce.</param>
    /// <returns>Pozycja sześcienna kafelka.</returns>
    static Vector3Int ToCubeCoordinates(Vector2Int position)
    {
        int x = position.x - (position.y - (position.y & 1)) / 2;
        int z = position.y;
        int y = -x - z;
        return new Vector3Int(x, y, z);

    }

    /// <summary>
    /// Obliczenie dystansu między kafelkami.
    /// </summary>
    /// <param name="a">Pozycja sześcienna pierwszego kafelka.</param>
    /// <param name="b">Pozycja sześcienna drugiego kafelka.</param>
    /// <returns>Odległość między kafelkami.</returns>
    static int CubeDistance(Vector3Int a, Vector3Int b)
    {
        return (Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y) + Mathf.Abs(a.z - b.z)) / 2;
    }

    /// <summary>
    /// Odnalezienie poziomu znajdującego się na odpowiedniej wysokości.
    /// </summary>
    /// <param name="yPosition">Pozycja w osi Y.</param>
    /// <returns>Poziom. Jeżeli na podanej wysokości nie znajduje się żaden pozmiom zwrócona zostanie wartość null.</returns>
    MapLevel GetLevelOnHeight(int yPosition)
    {
        int currentHeight = 0;
        if (yPosition >= 0)
        {
            foreach (MapLevel level in levels)
            {
                currentHeight += level.Dimensions.y + 1;
                if (currentHeight > yPosition)
                {
                    return level;
                }
            }
        }
        return null;
    }

    /// <summary>
    /// Obliczenie wysokości mapy, włącznie z wyszukiwanym poziomem.
    /// </summary>
    /// <param name="searchedLevel">Wyszukiwany poziom. Jeżeli null, zostanie ustawiony na ostatni poziom.</param>
    /// <returns>Wysokość mapy wyrażona w ilości kafelków.</returns>
    int MapHeight(MapLevel searchedLevel = null)
    {
        int result = 0;
        foreach (MapLevel level in levels)
        {
            result += level.Dimensions.y + 1;
            if (level == searchedLevel)
            {
                break;
            }
        }
        return result;
    }
}
