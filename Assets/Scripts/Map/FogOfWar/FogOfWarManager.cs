﻿using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.AddressableAssets;

/// <summary>
/// Zarządca mgły wojny.
/// </summary>
[RequireComponent(typeof(Tilemap))]
public class FogOfWarManager : MonoBehaviour
{
    /// <summary>
    /// Zarządca mapy.
    /// </summary>
    [HideInInspector] public MapManager mapManager;
    /// <summary>
    /// Odwołanie do umiejętności zwiększającej zasięg wzroku.
    /// </summary>
    public SkillProviderReference visionSkillProviderReference;
    /// <summary>
    /// Kolor podbicia zamglonego kafelka.
    /// </summary>
    public Color foggedColor = Color.grey;
    /// <summary>
    /// Kafelek mgły.
    /// </summary>
    public Tile fogTile;
    /// <summary>
    /// Kafelek połowicznej mgły.
    /// </summary>
    public Tile halfFogTile;
    /// <summary>
    /// Mapa kafelków dla mgły wojny.
    /// </summary>
    public Tilemap fogTilemap;
    /// <summary>
    /// Mapa kafelków dla połowicznej mgły wojny.
    /// </summary>
    public Tilemap halfFogTilemap;
    /// <summary>
    /// Umiejętność zwiększająca zasięg wzroku.
    /// </summary>
    SkillProvider visionSkillProvider;

    /// <summary>
    /// Odświeżenie mgły wojny dla kafelka.
    /// </summary>
    /// <param name="mapTile">Kafelek mapy.</param>
    public void Redraw(MapTile mapTile)
    {
        if (!mapTile.IsFoggable)
        {
            return;
        }

        Tile newFullFog = null;
        Tile newHalfFog = null;
        switch (mapTile.fogState)
        {
            case FogOfWarState.Full:
                newFullFog = fogTile;
                newHalfFog = null;
                break;
            case FogOfWarState.Half:
                newFullFog = null;
                newHalfFog = halfFogTile;
                break;
            case FogOfWarState.None:
                newFullFog = null;
                newHalfFog = null;
                break;
        }
        fogTilemap.SetTile(mapTile.tilemapPosition, newFullFog);
        halfFogTilemap.SetTile(mapTile.tilemapPosition, newHalfFog);
    }

    /// <summary>
    /// Zaaktualizowanie mgły wojny na ukończonym poziomie.
    /// </summary>
    /// <param name="level">Ukończony poziom.</param>
    public void UpdateFinished(MapLevel level)
    {
        foreach (MapTile tile in level.tiles)
        {
            tile.fogState = FogOfWarState.Half;
            if (tile.IsFoggable)
            {
                Redraw(tile);
            }
        }
    }

    /// <summary>
    /// Zaaktualizowanie mgły na ostatnim poziomie.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async UniTask UpdateFog()
    {
        MapLevel lastLevel = mapManager.levels.LastOrDefault();
        if(lastLevel == null || GameManager.Instance?.player?.data == null)
        {
            return;
        }

        if(visionSkillProvider == null)
        {
            visionSkillProvider = await visionSkillProviderReference.LoadAssetAsync<SkillProvider>();
            visionSkillProviderReference.ReleaseAsset();
        }

        int skillLevel = await GameManager.Instance.player.data.GetHeroSkillLevel(visionSkillProvider);
        int visionRadius = skillLevel + 1;
        List<MapTile> tiles = mapManager.GetTilesInDistance(GameManager.Instance.player.data.MapPosition, visionRadius);

        foreach (MapTile tile in tiles)
        {
            if (tile.IsFoggable && lastLevel.tiles.Contains(tile))
            {
                tile.fogState = FogOfWarState.None;
            }
        }

        foreach(MapTile tile in lastLevel.tiles)
        {
            if(tile.fogState == FogOfWarState.None && !tiles.Contains(tile))
            {
                tile.fogState = FogOfWarState.Half;
            }
            Redraw(tile);
        }
    }

}
