﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Stan mgły wojny.
/// </summary>
public enum FogOfWarState
{
    Full, 
    Half, 
    None
}
