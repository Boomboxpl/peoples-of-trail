﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Cysharp.Threading.Tasks;
using System.Threading.Tasks;

/// <summary>
/// Zdarzenie wywoływane po wejściu na kafelek.
/// </summary>
[Serializable]
public class MapEvent
{
    /// <summary>
    /// Szansa aktywacji rozwiązywania zdarzenia losowego. Dla wartości 1 zdarzenie jest pewne.
    /// </summary>
    public float activationChance { get; private set; }
    /// <summary>
    /// Konfiguracja zdarzenia losowego.
    /// </summary>
    public SerializableAsset<EventProvider> eventProvider { get; private set; }

    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="eventReference">Odwołanie do konfiguracji zdarzenia losowego.</param>
    /// <param name="activationChance">Szansa aktywacji rozwiązywania zdarzenia losowego.</param>
    public MapEvent(EventProviderReference eventReference, float activationChance)
    {
        eventProvider = new SerializableAsset<EventProvider>();
        eventProvider.Set(eventReference);
        this.activationChance = activationChance;
    }

    /// <summary>
    /// Wywołanie zdarzenia losowego, jeżeli nie zostało wcześniej wywołane.
    /// </summary>
    /// <returns>Zadanie asynchroniczne zwracające informacje czy zdarzenie zostało wywołane.</returns>
    public async UniTask<bool> Call()
    {
        float diceResult = GameManager.Instance.player.data.EventDiceRandom.NextFloat(0f, 1f);
        if (diceResult < activationChance)
        {
            GameManager.Instance.uiManager.eventSolver.CallEvent(await eventProvider.GetAsync());
            return true;
        }
        return false;
    }
}
