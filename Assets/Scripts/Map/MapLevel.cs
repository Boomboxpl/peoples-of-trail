﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Poziom gry.
/// </summary>
[Serializable]
public class MapLevel
{
    /// <summary>
    /// Informacja, czy poziom został ukończony.
    /// </summary>
    public bool isFinished;
    /// <summary>
    /// Tablica informacji o kafelkach.
    /// </summary>
    public List<MapTile> tiles;
    /// <summary>
    /// Ziarno generatora liczb pseudolosowych użyte do wygenerowania wartości zasobów na mapie.
    /// </summary>
    public int resourcesSeed;
    /// <summary>
    /// Ziarno generatora liczb pseudolosowych użyte do wygenerowania wartości wysokości terenu.
    /// </summary>
    public int heightSeed;
    /// <summary>
    /// Pozycja bramy od lewej krawędzi poziomu.
    /// </summary>
    public int gatePosition;

    /// <summary>
    /// Rozmiar poziomu w osi X.
    /// </summary>
    int dimensionsX;
    /// <summary>
    /// Rozmiar poziomu w osi Y.
    /// </summary>
    int dimensionsY;

    /// <summary>
    /// Rozmiar poziomu.
    /// </summary>
    public Vector2Int Dimensions
    {
        get => new Vector2Int(dimensionsX, dimensionsY);
        set
        {
            dimensionsX = value.x;
            dimensionsY = value.y;
        }
    }
    /// <summary>
    /// Długość muru.
    /// </summary>
    public int WallWidth;
    /// <summary>
    /// Kafelek z bramą.
    /// </summary>
    public GateTile Gate;
    /// <summary>
    /// Kafelek z murem.
    /// </summary>
    public WallTile Wall;

    /// <summary>
    /// Początek rysowania poziomu.
    /// </summary>
    public int StartX => -Dimensions.x / 2;

    /// <summary>
    /// Początek rysowania muru.
    /// </summary>
    public int WallStartX => -WallWidth / 2;

    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="tiles">Tablica informacji o kafelkach.</param>
    /// <param name="dimensions">Rozmiar poziomu.</param>
    /// <param name="resourcesSeed">Ziarno generatora liczb pseudolosowych użyte do wygenerowania wartości zasobów na mapie.</param>
    /// <param name="heightSeed">Ziarno generatora liczb pseudolosowych użyte do wygenerowania wartości wysokości terenu.</param>
    /// <param name="gatePosition">Pozycja bramy od lewej krawędzi poziomu.</param>
    /// <param name="gateTile"></param>
    /// <param name="wallTile"></param>
    public MapLevel(List<MapTile> tiles, Vector2Int dimensions, int resourcesSeed, int heightSeed, int gatePosition, GateTile gateTile, WallTile wallTile)
    {
        this.isFinished = false;
        this.tiles = tiles;
        this.Dimensions = dimensions;
        this.resourcesSeed = resourcesSeed;
        this.heightSeed = heightSeed;
        this.gatePosition = gatePosition;
        this.Gate = gateTile;
        this.Wall = wallTile;
    }

    /// <summary>
    /// Konstruktor pustego poziomu. Pusty poziom określa rozmiar poziomu, ale nie określa konkretnych kafelek.
    /// </summary>
    /// <param name="dimensions"></param>
    public MapLevel(Vector2Int dimensions)
    {
        this.isFinished = false;
        this.tiles = null;
        this.Dimensions = dimensions;
        this.resourcesSeed = (int)DateTime.Now.Ticks;
        this.heightSeed = (int)DateTime.Now.Ticks + 1;
        this.gatePosition = -1;
    }

    /// <summary>
    /// Pobranie kafelka z pozycji względem początku poziomu.
    /// </summary>
    /// <param name="positionLevel">Pozycja w przestrzeni poziomu.</param>
    /// <returns>Kafelek z poziomu na określonej pozycji. Jeżeli pozycja poza poziomem, zwróci wartość null.</returns>
    public MapTile GetTile(Vector2Int positionLevel)
    {
        MapTile tile = null;
        if(WallWidth == 0)
        {
            Debug.LogWarning("No Wall in level");
        }

        if(positionLevel.y == Dimensions.y && positionLevel.x >= WallStartX - StartX && positionLevel.x < WallWidth + WallStartX - StartX)
        {
            if (positionLevel.x == gatePosition)
            {
                tile = Gate;
            }
            else
            {
                tile = Wall;
            }
        }
        else if (positionLevel.x >= 0 && positionLevel.x < Dimensions.x && positionLevel.y >= 0 && positionLevel.y < Dimensions.y)
        {
            if (tiles == null)
            {
                tile = new FinishedTile();
            }
            else
            {
                tile = tiles[positionLevel.x + Dimensions.x * positionLevel.y];
            }
        }
        return tile;
    }

    /// <summary>
    /// Ustawienie poziomu jako ukończonego.
    /// Optymalizuje zapis poziomu do pliku poprzez usunięcie informacji o kafelkach.
    /// </summary>
    public void SetFinished()
    {
        isFinished = true;
        tiles.Clear();
        foreach(MapTile tile in tiles)
        {
            tile.mapEvent?.eventProvider?.UnloadReference();
        }
        tiles = null;
    }
}
