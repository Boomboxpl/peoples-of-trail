﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

/// <summary>
/// Właściwości wraz z wartością progową po której są aktywne.
/// </summary>
/// <typeparam name="T">Typ właściwości.</typeparam>
[Serializable]
public class ThresholdedProperty<T> : IComparable
{
    /// <summary>
    /// Wartość progowa.
    /// </summary>
    [Range(0, 1)]
    public float threshold;
    /// <summary>
    /// Właściwość.
    /// </summary>
    public T property;

    /// <summary>
    /// Porównywanie poprzez porównanie wartości progowych.
    /// </summary>
    /// <param name="obj">Drugi obiekt w porównaniu.</param>
    /// <returns>Wartości większe od 0, dla obiektów z wartością progową większą. 0 dla obiektów z równą wartością progową lub bez wartości progowej.</returns>
    public int CompareTo(object obj)
    {
        ThresholdedProperty<T> thresholdedObj = obj as ThresholdedProperty<T>;
        if (thresholdedObj != null)
        {
            return threshold.CompareTo(thresholdedObj.threshold);
        }
        return 0;
    }

    /// <summary>
    /// Otrzymanie właściwości przypisanej do obiektu.
    /// </summary>
    /// <param name="value">Wartość progowana.</param>
    /// <returns>Obiekt, jeżeli wartość progowa została przekroczona. Domyślną wartość (null), jeżeli nie została przekroczona.</returns>
    public T GetThresholded(float value)
    {
        if (value >= threshold)
        {
            return property;
        }
        return default(T);
    }
}

/// <summary>
/// Zestaw konfiguracji kafelków o określonej zasobności z wartością progową, po której jest aktywny.
/// </summary>
[Serializable]
public class ThresholdedTileSet : ThresholdedProperty<TileSetReference>
{
}

/// <summary>
/// Zestaw zdarzeń z wartością progową określającą prawdopodobieństwo wystąpienia.
/// </summary>
[Serializable]
public class ThresholdedEvent : ThresholdedProperty<EventProviderReference>
{
}

/// <summary>
/// Zestaw konfiguracji kafelków o określonej wysokości i zasobności z wartością progową, po której są aktywne.
/// </summary>
[Serializable]
public class ThresholdedHeights : ThresholdedProperty<HeightSet>
{
}