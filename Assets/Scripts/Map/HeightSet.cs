﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

/// <summary>
/// Zestaw konfiguracji kafelków o określonej wysokości i zasobności.
/// </summary>
[Serializable]
public class HeightSet
{
    /// <summary>
    /// Obrazy dla wysokości.
    /// </summary>
    public List<AssetReferenceSprite> sprites;

    /// <summary>
    /// Zdarzenia losowe przypisane do zasobności i wysokości terenu.
    /// </summary>
    public List<ThresholdedEvent> events;
    /// <summary>
    /// Zasoby tracone po wejściu na teren.
    /// </summary>
    public int resourcesToLost;
}
