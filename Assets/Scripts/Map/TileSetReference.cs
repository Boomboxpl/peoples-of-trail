﻿using System;
using UnityEngine;
using UnityEngine.AddressableAssets;

/// <summary>
/// Odowłanie do zestawu kafelków.
/// </summary>
[Serializable]
public class TileSetReference : AssetReferenceT<TileSet>
{
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="guid">Identyfikator odwołania.</param>
    public TileSetReference(string guid) : base(guid)
    {
    }
}
