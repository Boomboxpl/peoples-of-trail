﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Zarządca systemu tłumaczącego.
/// </summary>
public static class LocalizationManager
{
    /// <summary>
    /// Klucz zapisu preferowanego języka interfejsu.
    /// </summary>
    const string languagePlayerPrefs = "Language";

    /// <summary>
    /// Zdarzenie wywoływane po ustawieniu języka.
    /// </summary>
    public static Action LocalizationChanged;

    /// <summary>
    /// Indeks aktualnego języka.
    /// </summary>
    public static int CurrentLanguage
    {
        get => currentLanguage;
        set
        {
            SetLanguage(value).Forget();
        }
    }

    /// <summary>
    /// Obiekt odczytujący dane do systemu tłumaczącego.
    /// </summary>
    static readonly ILocalizationReader reader = new LocalizationReaderCSV(Application.streamingAssetsPath + "/Localization.csv");
    /// <summary>
    /// Indeks aktualnego języka.
    /// </summary>
    static int currentLanguage;
    /// <summary>
    /// Języki możliwe do ustawienia.
    /// </summary>
    static List<string> languages;
    /// <summary>
    /// Słownik dla aktualnie wybranego języka.
    /// </summary>
    static Dictionary<string, string> currentLanguageDictionary;

    /// <summary>
    /// Ustawienie języka.
    /// </summary>
    /// <param name="index">Indeks języka.</param>
    /// <returns>Zadanie asynchroniczne zwracające true, gdy ustawiono język.</returns>
    public static async UniTask<bool> SetLanguage(int index = 0)
    {
        index = Mathf.Clamp(index, 0, (await GetLanguages()).Count);
        PlayerPrefs.SetInt(languagePlayerPrefs, index);
        currentLanguageDictionary = await reader.ReadLocalization(index + 1);
        currentLanguage = index;
        LocalizationChanged?.Invoke();
        return true;
    }

    /// <summary>
    /// Przetłumaczenie klucza.
    /// </summary>
    /// <param name="key">Klucz w systemie lokalizacji.</param>
    /// <returns>Ciąg odczytany dla klucza w aktualnie ustawionym języku.</returns>
    public static string Localize(string key)
    {
        string result;
        if(currentLanguageDictionary == null)
        {
            SetLanguage(PlayerPrefs.GetInt(languagePlayerPrefs, 0)).Forget();
            if(currentLanguageDictionary == null)
            {
                return "";
            }
        }
        if (!currentLanguageDictionary.TryGetValue(key, out result))
        {
            if (!string.IsNullOrWhiteSpace(key))
            {
                Debug.LogError("No localization for " + key);
            }
            result = key;
        }
        return result;
    }

    /// <summary>
    /// Pobranie listy języków z pliku z tłumaczeniem.
    /// </summary>
    /// <returns>Zadanie asynchroniczne zwracające listę języków możliwych do ustawienia.</returns>
    public static async UniTask<List<string>> GetLanguages()
    {
        if (languages == null)
        {
            languages = await reader.ReadLanguages();
        }
        return languages;
    }
}
