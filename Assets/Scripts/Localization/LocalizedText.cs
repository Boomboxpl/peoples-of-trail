﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// Komponent z tekstem automatycznie pobieranym z systemu tłumaczącego.
/// </summary>
[RequireComponent(typeof(TextMeshProUGUI))]
public class LocalizedText : MonoBehaviour
{
    /// <summary>
    /// Aktualnie ustawiony tekst.
    /// </summary>
    public string Text => textComponent.text;

    /// <summary>
    /// Klucz lokalizacji.
    /// </summary>
    public string LocalizationKey
    {
        get
        {
            return localizationKey;
        }
        set
        {
            localizationKey = value;
            Localize();
        }
    }

    /// <summary>
    /// Klucz lokalizacji.
    /// </summary>
    [SerializeField]
    private string localizationKey;

    /// <summary>
    /// Komponent wyświetlający tekst.
    /// </summary>
    TextMeshProUGUI textComponent;

    /// <summary>
    /// Inicjalizacja. Ustawienie reakcji na zmianę języka. Przetłumaczenie klucza.
    /// </summary>
    void Awake()
    {
        textComponent = GetComponent<TextMeshProUGUI>();
        LocalizationManager.LocalizationChanged += Localize;
        Localize();
    }

    /// <summary>
    /// Usunięcie obiektu. Usunięcie przypisania reakcji zmianę języka.
    /// </summary>
    void OnDestroy()
    {
        LocalizationManager.LocalizationChanged -= Localize;
    }

    /// <summary>
    /// Tłumaczenie klucza.
    /// </summary>
    void Localize()
    {
        if(textComponent != null)
        {
            textComponent.text = LocalizationManager.Localize(LocalizationKey);
        }
    }
}
