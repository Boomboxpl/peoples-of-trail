﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.Networking;
using Cysharp.Threading.Tasks;
using System;

/// <summary>
/// Obiekt odczytujacy dane dla systemu tłumaczącego z pliku CSV.
/// </summary>
public class LocalizationReaderCSV : ILocalizationReader
{
    /// <summary>
    /// Ścieżka do pliku.
    /// </summary>
    public string path;
    /// <summary>
    /// Znak rozdzielający pola.
    /// </summary>
    public char fieldSeparator;
    /// <summary>
    /// Znak cudzysłowu.
    /// </summary>
    public char stringQuote;

    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="path">Ścieżka do pliku.</param>
    /// <param name="fieldSeparator">Znak rozdzielający pola.</param>
    /// <param name="stringQuote">Znak cudzysłowu.</param>
    public LocalizationReaderCSV(string path, char fieldSeparator = ',', char stringQuote = '"')
    {
        this.path = path;
        this.fieldSeparator = fieldSeparator;
        this.stringQuote = stringQuote;
    }

    /// <summary>
    /// Odczytanie języków.
    /// </summary>
    /// <returns>Zadanie asynchroniczna zwracające listę języków.</returns>
    public async UniTask<List<string>> ReadLanguages()
    {
        using (var lines = await GetLines(path))
        {
            string dataRow;
            if (NextCSVRow(lines, out dataRow))
            {
                return CsvRowToStringArray(dataRow).Skip(1).ToList();
            }
            return null;
        }
    }

    /// <summary>
    /// Odczytanie danych dla systemu tłumaczącego.
    /// </summary>
    /// <param name="translationColumnIndex">Indeks kolumny z tłumaczeniem.</param>
    /// <param name="keyIndex">Indeks kolumny z kluczem.</param>
    /// <returns>Zadanie asynchroniczna zwracające słownik dla wybranego języka.</returns>
    public async UniTask<Dictionary<string, string>> ReadLocalization(int translationColumnIndex, int keyIndex = 0)
    {
        Dictionary<string, string> result = new Dictionary<string, string>();

        using (IEnumerator<string> lines = await GetLines(path))
        {
            string dataRow;
            while (NextCSVRow(lines, out dataRow))
            {
                string[] csvRow = CsvRowToStringArray(dataRow);
                if (csvRow.Length > keyIndex && csvRow.Length > translationColumnIndex)
                {
                    result.Add(csvRow[keyIndex], csvRow[translationColumnIndex]);
                }
            }
        }
        return result;
    }

    /// <summary>
    /// Pobranie linii z ścieżki.
    /// </summary>
    /// <param name="path">Ścieżka do pliku. Ścieżka lokalna lub adres sieciowy.</param>
    /// <returns>Zadanie asynchroniczne zwracające obiek wyliczający linie pliku.</returns>
    public async UniTask<IEnumerator<string>> GetLines(string path)
    {
        try
        {
            if (File.Exists(path))
            {
                return File.ReadLines(path).GetEnumerator();
            }
            else
            {
                UnityWebRequest request = await UnityWebRequest.Get(path).SendWebRequest();
                if (request.isDone && !request.isHttpError && !request.isNetworkError)
                {
                    return ((IEnumerable<string>)request.downloadHandler.text.Split(
                        new string[] { Environment.NewLine },
                        StringSplitOptions.None
                    )).GetEnumerator();
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError("Error while loading localization: " + e.Message);
        }
        return null;
    }

    /// <summary>
    /// Odczytanie wiersza CSV, włącznie z znakami nowej linii w cudzysłowach.
    /// </summary>
    /// <param name="lines">Iterator linii w pliku.</param>
    /// <param name="result">Odczytany wiersz.</param>
    /// <returns>True, gdy wiersz został poprawnie odczytany.</returns>
    bool NextCSVRow(IEnumerator<string> lines, out string result)
    {
        StringBuilder stringBuilder = new StringBuilder();

        while (lines.MoveNext())
        {
            stringBuilder.Append(lines.Current);
            if (CountChar(stringBuilder, stringQuote) % 2 == 0)
            {
                result = stringBuilder.ToString();
                stringBuilder.Clear();
                return true;
            }
            else
            {
                stringBuilder.Append('\n');
            }
        }
        result = null;
        return false;
    }

    /// <summary>
    /// Przekształcenie wiersza CSV na tablice napisów.
    /// </summary>
    /// <param name="csvRow">Wiersz CSV.</param>
    /// <returns>Tablica powstała z pól w wierszu.</returns>
    string[] CsvRowToStringArray(string csvRow)
    {
        char tempQuote = (char)162;
        while (csvRow.Contains(tempQuote)) { tempQuote = (char)(tempQuote + 1); }
        char tempSeparator = (char)(tempQuote + 1);
        while (csvRow.Contains(tempSeparator)) { tempSeparator = (char)(tempSeparator + 1); }

        csvRow = csvRow.Replace(stringQuote.ToString() + stringQuote.ToString(), tempQuote.ToString());
        var csvArray = csvRow.Split(fieldSeparator).ToList().Aggregate("",
            (string row, string item) =>
            {
                if (row.Count((character) => character == stringQuote) % 2 == 0)
                {
                    return row + (row.Length > 0 ? tempSeparator.ToString() : "") + item;
                }
                else
                {
                    return row + fieldSeparator + item;
                }
            },
            (string row) => row
                .Split(tempSeparator)
                .Select((string item) => item.Trim(stringQuote)
                    .Replace(tempQuote, stringQuote))
            ).ToArray();
        return csvArray;
    }

    /// <summary>
    /// Zliczenie liczby wystąpień znaku w ciągu.
    /// </summary>
    /// <param name="source">Ciąg wejściowy.</param>
    /// <param name="searched">Wyszukiwaney znak.</param>
    /// <returns>Ilość wystąpień znaku.</returns>
    int CountChar(StringBuilder source, char searched)
    {
        int count = 0;
        for (int i = 0; i < source.Length; i++)
        {
            if (source[i] == searched)
            {
                count++;
            }
        }
        return count;
    }
}
