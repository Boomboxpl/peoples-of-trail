﻿using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interfejs obiektu odczytującego dane dla systemu tłumaczącego.
/// </summary>
interface ILocalizationReader
{
    /// <summary>
    /// Odczytanie języków.
    /// </summary>
    /// <returns>Zadanie asynchroniczna zwracające listę języków.</returns>
    UniTask<List<string>> ReadLanguages();

    /// <summary>
    /// Odczytanie dane dla systemu tłumaczącego.
    /// </summary>
    /// <param name="translationColumnIndex">Indeks kolumny z tłumaczeniem.</param>
    /// <param name="keyIndex">Indeks kolumny z kluczem.</param>
    /// <returns>Zadanie asynchroniczna zwracające słownik dla wybranego języka.</returns>
    UniTask<Dictionary<string, string>> ReadLocalization(int translationColumnIndex, int keyIndex = 0);
}
