﻿using System;
using UnityEngine.AddressableAssets;

/// <summary>
/// Odwołanie do CommonEvents.
/// </summary>
[Serializable]
class CommonEventsReference : AssetReferenceT<CommonEvents>
{
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="guid">Identyfikator zasobu.</param>
    public CommonEventsReference(string guid) : base(guid)
    {
    }
}

