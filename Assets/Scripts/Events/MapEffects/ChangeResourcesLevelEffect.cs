﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

/// <summary>
/// Efekt mapy powodujący zmianę zasobności i wysokości terenu wokół miejsca wywołania.
/// </summary>
[CreateAssetMenu(fileName = "ResourcesEffect", menuName = "MapEffects/ChangeResourcesLevel")]
public class ChangeResourcesLevelEffect : MapEffect
{
    /// <summary>
    /// Zasięg działania efektu wyrażony w ilości kafelek.
    /// </summary>
    public int distance = 1;
    /// <summary>
    /// Docelowy poziom zasobów.
    /// </summary>
    public float resourceLevel;
    /// <summary>
    /// Docelowy poziom wysokości.
    /// </summary>
    public float heightLevel;

    /// <summary>
    /// Wywołanie efektu. Zmiana zasobności i wysokości kafelków wokół gracza.
    /// </summary>
    /// <param name="mapPosition">Pozycja na mapie, na której został wywołany efekt.</param>
    public override async void ApplyEffect(Vector2Int mapPosition)
    {
        MapManager mapManager = GameManager.Instance.mapManager;
        MapGenerator mapGenerator = mapManager.mapGenerator;

        TileSet tileSet = await mapGenerator.GetTileSet(resourceLevel);
        mapManager.mapGenerator.ReleaseTilesets();

        HeightSet heightSet = tileSet.GetHeightSet(heightLevel);
        List<MapTile> tiles = mapManager.GetTilesInDistance(mapPosition, distance);
        System.Random randomGenerator = GameManager.Instance.player.data.EventDiceRandom;
        foreach(MapTile tile in tiles)
        {
            if (tile.IsEffectTarget)
            {
                AssetReferenceSprite newSprite = heightSet.sprites.Random(randomGenerator);
                tile.SetSprite(newSprite);
                tile.resourcesToGather = tileSet.resourcesToGather;
                tile.resourcesToLose = heightSet.resourcesToLost;
            }
        }

        await mapManager.RedrawLastLevel();
    }
}
