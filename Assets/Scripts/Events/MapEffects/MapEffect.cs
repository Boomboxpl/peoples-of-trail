﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa bazowa dla efetów mapy, wywoływanych podczas zdarzeń losowych.
/// </summary>
public abstract class MapEffect : ScriptableObject
{
    /// <summary>
    /// Wywołanie efektu.
    /// </summary>
    /// <param name="mapPosition">ozycja na mapie na której został wywołany efekt.</param>
    public abstract void ApplyEffect(Vector2Int mapPosition);
}
