﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System;
using System.Linq;
using Cysharp.Threading.Tasks;

/// <summary>
/// Klasa definiująca wymagania opcji dostępnej dla zdarzenia.
/// </summary>
[Serializable]
public class EventOptionRequirement
{
    /// <summary>
    /// Klucz systemu tłumaczącego z początkiem opisu wymagań.
    /// </summary>
    const string requireKey = "Events.Require";
    /// <summary>
    /// Format tekstowego opisu wymagań.
    /// Zawiera początek opisu stały dla wszystkich opcji oraz wartości wymagań zależny od parametrów wymagania.
    /// </summary>
    const string requirementFormat = "{0}: {1}, ";

    /// <summary>
    /// Umiejętność, której dotyczy wymaganie.
    /// W przypadku, gdy wymagany poziom nie jest poprawny, parametr jest ignorowany.
    /// </summary>
    public SkillProvider requiredSkill;
    /// <summary>
    /// Wymagany poziom umiejętności.
    /// W przypadku, gdy umiejętność jest pusta, parametr jest ignorowany.
    /// </summary>
    public int requiredSkillLevel = -1;
    /// <summary>
    /// Wymagany poziom morali.
    /// </summary>
    public float morales = -1f;
    /// <summary>
    /// Wymagana liczba ludzi.
    /// </summary>
    public int peoples = -1;
    /// <summary>
    /// Wymagana liczba zasobów.
    /// </summary>
    public int resources = -1;

    /// <summary>
    /// Informacja, czy wymaganie jest poprawne. Niepoprawne wymagania są ignorowane.
    /// </summary>
    public bool IsValid => RequireSkill || RequireMorales || RequirePeoples || RequireResources; 
    /// <summary>
    /// Informacja, czy wymagany jest pewien poziom umiejętności.
    /// </summary>
    public bool RequireSkill => requiredSkill != null && requiredSkillLevel > 0;
    /// <summary>
    /// Informacja, czy wymagany jest pewien poziom morali.
    /// </summary>
    public bool RequireMorales => morales > 0f;
    /// <summary>
    /// Informacja, czy wymagana jest pewna liczba ludności.
    /// </summary>
    public bool RequirePeoples => peoples > 0;
    /// <summary>
    /// Informacja, czy wymagana jest pewna liczba zasobów.
    /// </summary>
    public bool RequireResources => resources > 0;

    /// <summary>
    /// Sprawdzenie czy wszystkie wymagania są spełnione przez gracza.
    /// </summary>
    /// <returns>True, gdy wymagania są spełnione.</returns>
    public async UniTask<bool> IsRequirementMeet()
    {
        PlayerData data = GameManager.Instance.player.data;
        int skillLevel = -1;
        if (requiredSkill != null)
        {
            skillLevel = await data.GetHeroSkillLevel(requiredSkill);
        }
        return IsComponentRequirementMeet(RequireSkill, requiredSkillLevel, skillLevel) &&
            IsComponentRequirementMeet(RequireMorales, morales, data.Morales) &&
            IsComponentRequirementMeet(RequirePeoples, peoples, data.Peoples) &&
            IsComponentRequirementMeet(RequireResources, resources, data.Resources);
    }

    /// <summary>
    /// Wygenerowanie opisu wymagań.
    /// </summary>
    /// <returns>Opis wymagań.</returns>
    public string GetRequirementDescription()
    {
        StringBuilder result = new StringBuilder();
        if (IsValid)
        {
            result.AppendFormat(" [{0}: ", LocalizationManager.Localize(requireKey));

            if (RequireSkill)
            {
                result.AppendFormat(requirementFormat,
                    LocalizationManager.Localize(requiredSkill.LocalizationKey),
                    requiredSkillLevel);
            }
            if (RequireMorales)
            {
                result.AppendFormat(requirementFormat,
                    SpriteTextDefinitions.moralesSprite,
                    morales.ToProcentString());
            }
            if (RequirePeoples)
            {
                result.AppendFormat(requirementFormat,
                    SpriteTextDefinitions.peoplesSprite,
                    peoples);
            }
            if (RequireResources)
            {
                result.AppendFormat(requirementFormat,
                    SpriteTextDefinitions.resourcesGatherSprite,
                    resources);
            }

            result.TrimEnd(character => char.IsWhiteSpace(character) || character == ',');
            result.Append(']');
        }
        return result.ToString();
    }

    /// <summary>
    /// Warunkowe sprawdzenie spełnienia wymagania.
    /// </summary>
    /// <param name="isRequired">Czy parametr jest wymagany.</param>
    /// <param name="requiredValue">Wymagana wartość parametru.</param>
    /// <param name="actualValue">Wartość parametru.</param>
    /// <typeparam name="T">Typ porównywanej wartości. Musi realizować interfejs IComparable.</typeparam>
    /// <returns>True, jeżeli wymagania są spełnione lub nie ma wymagań dotyczących danego atrybutu.</returns>
    bool IsComponentRequirementMeet<T>(bool isRequired, T requiredValue, T actualValue) where T : IComparable => 
        !isRequired || actualValue.CompareTo(requiredValue) >= 0;
}
