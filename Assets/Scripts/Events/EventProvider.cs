﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa wykorzystywana do konfiguracji zdarzenia losowego.
/// </summary>
[CreateAssetMenu(fileName = "Event", menuName = "Event")]
public class EventProvider : ScriptableObject
{
    /// <summary>
    /// Klucz systemu tłumaczącego dla tytułu zdarzenia.
    /// </summary>
    public string titleKey;
    /// <summary>
    /// Klucz systemu tłumaczącego dla opisu zdarzenia.
    /// Przyjmuje następujące argumenty formatowania:
    ///0 – liczba zdobytych punktów szacunku;
    ///1 – zmiana liczby morali;
    ///2 – zmiana liczby ludzi;
    ///3 – zmiana liczby zasobów.
    /// </summary>
    public string desctiprionKey;
    /// <summary>
    /// Obraz na początku zdarzenia.
    /// </summary>
    public Sprite sprite;
    /// <summary>
    /// Rozmiar obrazu na początku zdarzenia. Jeżeli ustawiono -1, wartość zostanie użyta domyślna wysokość Sprite.
    /// </summary>
    public float preferedImageHeight = 300;
    /// <summary>
    /// Dźwięk odtwarzany po pojawieniu się zdarzenia.
    /// </summary>
    public AudioClip eventStartSound;
    /// <summary>
    /// Minimalna szansa aktywacji zdarzenia losowego.
    /// </summary>
    [Range(0f, 1f)]
    public float minActivationChance = 0.5f;
    /// <summary>
    /// Maksymalna szansa aktywacji zdarzenia losowego.
    /// </summary>
    [Range(0f, 1f)]
    public float maxActivationChance = 1f;
    /// <summary>
    /// Poziom niebezpieczeństwa, wykorzystywany tylko jako informacja dla użytkownika.
    /// </summary>
    [Range(0f, 1f)]
    public float dangerLevel;
    /// <summary>
    /// Wynik rozpoczęcia zdarzenia losowego.
    /// </summary>
    public EventStartResult result;
    /// <summary>
    /// Lista opcji z statycznym prawdopodobieństwem.
    /// </summary>
    public List<StaticChanceEventOption> staticOptions;
    /// <summary>
    /// Lista opcji skalowanych z umiejętnością.
    /// </summary>
    public List<SkillScaledEventOption> skillOptions;

    /// <summary>
    /// Walidacja wprowadzanych parametrów szansy aktywacji.
    /// </summary>
    void OnValidate()
    {
        minActivationChance = Mathf.Clamp01(minActivationChance);
        maxActivationChance = Mathf.Clamp(maxActivationChance, minActivationChance, 1f);
    }
}
