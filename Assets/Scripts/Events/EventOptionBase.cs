﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Cysharp.Threading.Tasks;

/// <summary>
/// Klasa bazowa dla opcji wyboru podczas rozwiązywania zdarzenia losowego.
/// </summary>
[Serializable]
public abstract class EventOptionBase
{
    /// <summary>
    /// Klucz systemu tłumaczącego z opisem akcji przypisanej do opcji.
    /// </summary>
    public string actionDescriptionKey;
    /// <summary>
    /// Wymagania dla opcji.
    /// </summary>
    public EventOptionRequirement requirement;
    /// <summary>
    /// Następne zdarzenie.
    /// </summary>
    [SerializeField] EventProvider nextEvent;
    /// <summary>
    /// Następne zdarzenie, w przyapdku porażki w losowaniu.
    /// </summary>
    [SerializeField] EventProvider nextEventOnFail;
    /// <summary>
    /// Szansa na powodzenie akcji.
    /// </summary>
    float effectiveChanceOfSuccess;
    /// <summary>
    /// Wyliczenie szansy na powodzenie akcji.
    /// </summary>
    /// <returns>Szansa na powodzenie akcji.</returns>
    protected virtual UniTask<float> GetChanceOfSuccess() => new UniTask<float>(-1);
    
    /// <summary>
    /// Przygotowanie danych dla opcji. Wyliczenie szansy na powodzenie akcji.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async UniTask PrepareData()
    {
         effectiveChanceOfSuccess = await GetChanceOfSuccess();
    }

    /// <summary>
    /// Wygenerowanie opisu szansy na powodzenie akcji.
    /// </summary>
    /// <returns>Opis szansy na powodzenie akcji.</returns>
    public virtual string GetChanceOfSuccessDescription()
    {
        if (IsRandom)
        {
            return $" ({SpriteTextDefinitions.diceSprite} {effectiveChanceOfSuccess.ToProcentString()})";
        }
        return "";
    }

    /// <summary>
    /// Pobranie następnego zdarzenia.
    /// </summary>
    /// <returns>Następne zdarzenie lub wartość pusta, gdy skończy się rozwiązywanie zdarzeń.</returns>
    public EventProvider GetNextEventProvider()
    {
        if (IsRandom)
        {
            float diceResult = GameManager.Instance.player.data.EventDiceRandom.NextFloat(0f, 1f);
            if(diceResult < effectiveChanceOfSuccess)
            {
                return nextEvent;
            }
            else
            {
                return nextEventOnFail;
            }
        }
        return nextEvent;
    }

    /// <summary>
    /// Informacja, czy opcja jest losowa.
    /// </summary>
    bool IsRandom => effectiveChanceOfSuccess > 0f;
}
