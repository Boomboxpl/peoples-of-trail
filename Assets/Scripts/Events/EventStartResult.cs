﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Klasa wykorzystywana do konfiguracji wyniku rozpoczęcia zdarzenia losowego.
/// </summary>
[Serializable]
public class EventStartResult
{
    /// <summary>
    /// Przyrost punktów szacunku.
    /// </summary>
    public int respectGain;
    /// <summary>
    /// Zmiana poziomu morali.
    /// </summary>
    public float moralesDelta;
    /// <summary>
    /// Zmiana liczby ludności.
    /// </summary>
    public int peoplesDelta;
    /// <summary>
    /// Zmiana liczby zasobów.
    /// </summary>
    public int resourcesDelta;
    /// <summary>
    /// Efekt mapy.
    /// </summary>
    public MapEffect mapEffect;

    /// <summary>
    /// Wykonanie zmian w stanie posiadania gracza zgodnie z parametrami.
    /// Wywołuje efekt mapy, jeśli został przypisany.
    /// </summary>
    public void Apply()
    {
        PlayerData playerData = GameManager.Instance.player.data;
        playerData.Respect.CurrentPoints += respectGain;
        playerData.Morales += moralesDelta;
        playerData.Resources += resourcesDelta;
        playerData.Peoples += peoplesDelta;
        mapEffect?.ApplyEffect(playerData.MapPosition);
        GameManager.Instance.uiManager.RefreshUI();
    }
}
