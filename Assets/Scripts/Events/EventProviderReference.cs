﻿using System;
using UnityEngine;
using UnityEngine.AddressableAssets;

/// <summary>
/// Odwołanie do EventProvider.
/// </summary>
[Serializable]
public class EventProviderReference : AssetReferenceT<EventProvider>
{
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="guid">Identyfikator zasobu.</param>
    public EventProviderReference(string guid) : base(guid)
    {
    }
}

