﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

/// <summary>
/// Klasa grupująca pule zdarzeń losowych niezależnych od typu terenu.
/// </summary>
[CreateAssetMenu(fileName = "CommonEvents", menuName = "CommonEvents")]
public class CommonEvents : ScriptableObject
{
    /// <summary>
    /// Lista zdarzeń przypisywanych do zwykłych kafelków, niezależnie od parametrów terenu.
    /// </summary>
    [SerializeField] List<ThresholdedEvent> generalEvents;
    /// <summary>
    /// Lista zdarzeń występujących w zamkach.
    /// </summary>
    [SerializeField] List<EventProviderReference> castleEvents;
    /// <summary>
    /// Lista zdarzeń fabularnych.
    /// </summary>
    [SerializeField] List<EventProviderReference> storyEvents;

    /// <summary>
    /// Niemodyfikowalna kolekcja zdarzeń ogólnych.
    /// </summary>
    public ReadOnlyCollection<ThresholdedEvent> GeneralEvents => generalEvents.AsReadOnly();

    /// <summary>
    /// Niemodyfikowalna kolekcja zdarzeń zamkowych.
    /// </summary>
    public ReadOnlyCollection<EventProviderReference> CastleEvents => castleEvents.AsReadOnly();

    /// <summary>
    /// Funkcja dostępu do zdarzenia fabularnego przypisanego do danego poziomu.
    /// </summary>
    /// <param name="level">Aktualny poziom, gdzie 0 to pierwszy poziom.</param>
    /// <returns>Zdarzenie fabularne lub wartość pusta po ukończeniu fabuły.</returns>
    public EventProviderReference GetStoryEvent(int level)
    {
        if(level < 0 || level >= storyEvents.Count)
        {
            return null;
        }
        return storyEvents[level];
    }
}
