﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Cysharp.Threading.Tasks;

/// <summary>
/// Opcja wyboru podczas zdarzenia losowego, której prawdopodobieństwo powodzenia akcji jest skalowane z poziomem umiejętności.
/// </summary>
[Serializable]
public class SkillScaledEventOption : EventOptionBase
{
    /// <summary>
    /// Klucz systemu tłumaczącego z opisem skalowania.
    /// </summary>
    const string scalingDescriptionKey = "Events.Scaled";

    /// <summary>
    /// Umiejętność, z której poziomem następuje skalowanie.
    /// </summary>
    public SkillProvider scalingSkill;
    /// <summary>
    /// Bazowa szansa powodzenia akcji.
    /// </summary>
    public float baseChance;
    /// <summary>
    /// Dodatkowa szansa za każdy poziom umiejętności.
    /// </summary>
    public float chancePerLevel;

    /// <summary>
    /// Wygenerowanie opisu szansy na powodzenie akcji.
    /// </summary>
    /// <returns>Opis szansy na powodzenie akcji.</returns>
    public override string GetChanceOfSuccessDescription()
    {
        string scalingDescription = LocalizationManager.Localize(scalingDescriptionKey);
        string skillName = LocalizationManager.Localize(scalingSkill.LocalizationKey);
        return $" [{scalingDescription} {skillName}]{base.GetChanceOfSuccessDescription()}";
    }

    /// <summary>
    /// Wyliczenie szansy na powodzenie akcji. Jest to suma szansy bazowej i szansy za osiągnięty poziom umiejętności ograniczona do przedziału (0,1).
    /// </summary>
    /// <returns>Szansa na powodzenie akcji.</returns>
    protected override async UniTask<float> GetChanceOfSuccess()
    {
        if(scalingSkill == null)
        {
            return -1f;
        }
        int level = await GameManager.Instance.player.data.GetHeroSkillLevel(scalingSkill);
        return Mathf.Clamp01(baseChance + level * chancePerLevel);
    }
}
