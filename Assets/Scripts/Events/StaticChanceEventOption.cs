﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Cysharp.Threading.Tasks;

/// <summary>
/// Opcja wyboru podczas zdarzenia losowego, której prawdopodobieństwo powodzenia akcji jest stałe.
/// </summary>
[Serializable]
public class StaticChanceEventOption : EventOptionBase
{
    /// <summary>
    /// Szansa powodzenia.
    /// </summary>
    [SerializeField] float chanceOfSuccess = -1f;
    
    /// <summary>
    /// Wyliczenie szansy na powodzenie akcji.
    /// </summary>
    /// <returns>Szansa na powodzenie akcji pobrana z parametru.</returns>
    protected override UniTask<float> GetChanceOfSuccess() => new UniTask<float>(chanceOfSuccess);
}
