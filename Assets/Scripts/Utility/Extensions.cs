﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// Klasa rozszerzeń obiektów.
/// </summary>
public static partial class Extensions
{
    /// <summary>
    /// Uzyskanie losowego elementu z kolekcji.
    /// </summary>
    /// <typeparam name="T">Typ elementów w kolekcji.</typeparam>
    /// <param name="enumerable">Elementy kolekcji.</param>
    /// <param name="randomGenerator">Generator liczb pseudolosowych. Generuje losowy indeks.</param>
    /// <returns>Losowo wybrany element. Jeżeli lista jest pusta zwraca null.</returns>
    public static T Random<T>(this IEnumerable<T> enumerable, System.Random randomGenerator)
    {
        return enumerable.ElementAtOrDefault(randomGenerator.Next(0, enumerable.Count()));
    }

    /// <summary>
    /// Odrzucenie z ciągu znaków elementów znajdujących się po przekroczeniu maksymalnej długości.
    /// </summary>
    /// <param name="value">Ciąg znaków.</param>
    /// <param name="maxLength">Maksymalna długość.</param>
    /// <returns>Ciąg znaków z długością mniejszą lub równą maksymalnej.</returns>
    public static string Truncate(this string value, int maxLength)
    {
        if (string.IsNullOrEmpty(value)) return value;
        return value.Length <= maxLength ? value : value.Substring(0, maxLength);
    }

    /// <summary>
    /// Wyliczenie wartości pseudolosowej z przedziału obustronnie zamkniętego.
    /// </summary>
    /// <param name="random">Generator liczb pseudlosowych.</param>
    /// <param name="min">Początek przedziału.</param>
    /// <param name="max">Koniec przedziału.</param>
    /// <returns>Wartość pseudolosowa.</returns>
    public static float NextFloat(this System.Random random, float min, float max)
    {
        double val = random.NextDouble() * (max - min) + min;
        return (float)val;
    }

    /// <summary>
    /// Usunięcie wszystkich dzieci obiektu na scenie.
    /// </summary>
    /// <param name="transform">Transform obiektu.</param>
    public static void KillChildren(this Transform transform)
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    /// <summary>
    /// Pobranie rozmiaru płótna, do którego jest przypisany obiekt. 
    /// </summary>
    /// <param name="monoBehaviour">Komponent.</param>
    /// <returns>Rozmiar płótna.</returns>
    public static Vector2 GetCanvasSize(this MonoBehaviour monoBehaviour) =>
        monoBehaviour.GetComponentInParent<Canvas>().GetComponent<RectTransform>().rect.size;

    /// <summary>
    /// Sformatowanie liczby zmiennoprzecinkowej na postać procentową.
    /// </summary>
    /// <param name="value">Wartość liczby.</param>
    /// <returns>Reprezentacja procentowa.</returns>
    public static string ToProcentString(this float value) => ((int)(value * 100f)).ToString() + '%';

    /// <summary>
    /// Ustawienie wartości kanału alfa koloru.
    /// </summary>
    /// <param name="color">Kolor.</param>
    /// <param name="alpha">Wartość kanału alfa.</param>
    /// <returns>Kolor z ustawionym kanałem alfa.</returns>
    public static Color SetAlpha(this Color color, float alpha)
    {
        color.a = alpha;
        return color;
    }

    /// <summary>
    /// Przekształcenie zmiennej logicznej na całkowitą.
    /// </summary>
    /// <param name="boolean">Warość logiczna.</param>
    /// <returns>1, dla wartości logicznej true. 0, dla wartości logicznej false.</returns>
    public static int To01(this bool boolean) => boolean ? 1 : 0;

    /// <summary>
    /// Usunięcie znaków z końca ciągu znaków.
    /// </summary>
    /// <param name="stringBuilder">Ciąg znaków.</param>
    /// <param name="trimPredicate">Warunek usunięcia znaku. Jeżeli wartość pusta zostaną usunięte tylko białe znaki.</param>
    public static void TrimEnd(this StringBuilder stringBuilder, Predicate<char> trimPredicate = null)
    {
        if(trimPredicate == null)
        {
            trimPredicate = (character) => char.IsWhiteSpace(character);
        }
        if (stringBuilder == null || stringBuilder.Length == 0)
        {
            return;
        }
        int i;
        for (i = stringBuilder.Length - 1; i >= 0; i--)
        {
            if (!trimPredicate.Invoke(stringBuilder[i]))
            {
                break;
            }
        }

        if (i < stringBuilder.Length - 1)
        {
            stringBuilder.Length = i + 1;
        }
    }
}

