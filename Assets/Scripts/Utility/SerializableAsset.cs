﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using System;
using Cysharp.Threading.Tasks;

/// <summary>
/// Zasób z możliwością serializacji odwołania.
/// </summary>
/// <typeparam name="T">Typ zasobu.</typeparam>
[Serializable]
public class SerializableAsset<T> where T : UnityEngine.Object
{
    /// <summary>
    /// Serializowana reprezentacja odwołania.
    /// </summary>
    string referenceJson;
    /// <summary>
    /// Odwołanie do zasobu.
    /// </summary>
    [NonSerialized] AssetReferenceT<T> assetReference;
    /// <summary>
    /// Załadowany zasób.
    /// </summary>
    [NonSerialized] T loadedAsset;

    /// <summary>
    /// Finalizator. Wyładowuje odwołanie.
    /// </summary>
    ~SerializableAsset()
    {
        UnloadReference();
    }

    /// <summary>
    /// Pobranie zasobu.
    /// </summary>
    /// <returns>Zadanie asynchroniczne zwracające załadowany zasób.</returns>
    public async UniTask<T> GetAsync()
    {
        if (loadedAsset == null)
        {
            if (assetReference == null)
            {
                assetReference = JsonUtility.FromJson<AssetReferenceT<T>>(referenceJson);
            }

            await UniTask.WaitUntil(() => assetReference.IsDone);
            if (assetReference.Asset == null)
            {
                try
                {
                    GameLoadManager.beforeSceneChange -= UnloadReference;
                    GameLoadManager.beforeSceneChange += UnloadReference;
                    await assetReference.LoadAssetAsync<T>();
                }
                catch { }
            }
            loadedAsset = (T)assetReference.Asset;
        }
        return loadedAsset;
    }

    /// <summary>
    /// Pobranie odwołania do zasobu.
    /// </summary>
    /// <returns>Odwołanie do zasobu.</returns>
    public AssetReferenceT<T> GetReference() => assetReference;

    /// <summary>
    /// Ustawienie odwołania do zasobu.
    /// </summary>
    /// <param name="asset">Odwołanie do zasobu.</param>
    public void Set(AssetReferenceT<T> asset)
    {
        if (asset == null)
        {
            return;
        }
        assetReference = new AssetReferenceT<T>((string)asset.RuntimeKey);
        loadedAsset = null;
        referenceJson = JsonUtility.ToJson(asset);
    }

    /// <summary>
    /// Wyładowanie odwołania, jeżeli zostało załadowane.
    /// </summary>
    public void UnloadReference()
    {
        if (assetReference != null && assetReference.Asset != null)
        {
            GameLoadManager.beforeSceneChange -= UnloadReference;
            assetReference.ReleaseAsset();
        }
    }
}
