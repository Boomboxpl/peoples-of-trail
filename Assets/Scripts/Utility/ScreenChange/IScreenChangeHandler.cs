﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interfejs obiektu obsługującego zmianę rozmiaru ektranu.
/// </summary>
public interface IScreenChangeHandler
{
    /// <summary>
    /// Obsługa zmiany rozmiaru ekranu.
    /// </summary>
    /// <param name="aspectRatio">Nowe proporcje ekranu.</param>
    void OnScreenChanged(float aspectRatio);
}
