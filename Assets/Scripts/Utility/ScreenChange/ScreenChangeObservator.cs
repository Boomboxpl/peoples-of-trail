﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// Zarządca zmiany rozmiaru ekranu.
/// </summary>
public class ScreenChangeObservator : MonoBehaviour
{
    /// <summary>
    /// Obsługa zmiany rozmiaru ekranu. Wywołanie funkcji obiektów obsługi zmiany rozmiarów ekranu.
    /// </summary>
    void OnRectTransformDimensionsChange()
    {
        IScreenChangeHandler[] handlers = GetComponentsInChildren<IScreenChangeHandler>();
        float aspectRatio = (float)Screen.width / Screen.height;
        foreach (IScreenChangeHandler handler in handlers)
        {
            handler.OnScreenChanged(aspectRatio);
        }
    }
}
