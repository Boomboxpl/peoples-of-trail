﻿using System;
using UnityEngine;
using UnityEngine.AddressableAssets;

/// <summary>
/// Odwołanie do konfiguracji umiejętności.
/// </summary>
[Serializable]
public class SkillProviderReference : AssetReferenceT<SkillProvider>
{
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="guid">Identyfikator odwołania.</param>
    public SkillProviderReference(string guid) : base(guid)
    {
    }
}