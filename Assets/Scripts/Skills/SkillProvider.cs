﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.AddressableAssets;
using System.Linq;
using Cysharp.Threading.Tasks;

/// <summary>
/// Konfiguracja umiejętności.
/// </summary>
[CreateAssetMenu(fileName = "Skill", menuName = "Skill")]
public class SkillProvider : ScriptableObject
{
    /// <summary>
    /// Liczba atrybutów, na który oddziałują bierne efekty umiejęności.
    /// Wykorzystywana jako przesunięcie w umiejętności z losowym przydzielaniem efektu.
    /// </summary>
    const int pasiveEffectFields = 4;

    /// <summary>
    /// Ikona.
    /// </summary>
    public Sprite icon;

    /// <summary>
    /// Klucz systemu tłumaczącego dla nazwy umiejętności.
    /// </summary>
    [SerializeField] string localizationKey;
    /// <summary>
    /// Maksymalny poziom.
    /// </summary>
    [SerializeField] int maxLevel;
    /// <summary>
    /// Informacja, czy umiejętność losowo przydziela swój efekt.
    /// </summary>
    [SerializeField] bool randomizePassiveEffect;
    /// <summary>
    /// Liczba przypadków, w którch umiejętność z losowym przydzielaniem umiejętności nie przydzieli żadnego efektu. 
    /// W przypadku umiejętności z stałym przydzielanym atrybutem parametr jest ignorowany.
    /// </summary>
    [SerializeField] int randomizeEmptyFields = 1;
    /// <summary>
    /// Bierny efekt za każdy poziom rozwoju umiejętności.
    /// </summary>
    [SerializeField] PassiveEffectData passiveEffectPerLevel;

    /// <summary>
    /// Klucz systemu tłumaczącego dla nazwy umiejętności.
    /// </summary>
    public string LocalizationKey => localizationKey;
    /// <summary>
    /// Maksymalny poziom.
    /// </summary>
    public int MaxLevel => maxLevel;

    /// <summary>
    /// Wyliczenie wartości biernego efektu.
    /// </summary>
    /// <param name="level">Poziom umiejętności.</param>
    /// <param name="playerData">Dane gracza.</param>
    /// <returns>Wartość biernego efektu.</returns>
    public PassiveEffectData GetPassiveEffectData(int level, PlayerData playerData)
    {
        PassiveEffectData result = new PassiveEffectData
        {
            resourcesGatherIncrease = passiveEffectPerLevel.resourcesGatherIncrease * level,
            resourcesLoseDecrease = passiveEffectPerLevel.resourcesLoseDecrease * level,
            moralesGainIncrease = passiveEffectPerLevel.moralesGainIncrease * level,
            moralesLoseDecrease = passiveEffectPerLevel.moralesLoseDecrease * level
        };
        if (randomizePassiveEffect)
        {
            result = GetRandomize(result, playerData);
        }
        return result;
    }

    /// <summary>
    /// Przekształcenie efektu biernego na losowo przydzielany.
    /// </summary>
    /// <param name="passiveEffectData">Efekt bierny.</param>
    /// <param name="playerData">Dane gracza.</param>
    /// <returns>Wartość biernego efektu. Wartość różna od 0 jest przypisana do conajwyżej jednego atrybutu.</returns>
    PassiveEffectData GetRandomize(PassiveEffectData passiveEffectData, PlayerData playerData)
    {
        PassiveEffectData result = new PassiveEffectData();
        System.Random random = playerData.EventDiceRandom;
        int randomResult = random.Next(randomizeEmptyFields + pasiveEffectFields);
        switch (randomResult)
        {
            case 0:
                result.resourcesGatherIncrease = passiveEffectData.resourcesGatherIncrease;
                break;
            case 1:
                result.resourcesLoseDecrease = passiveEffectData.resourcesLoseDecrease;
                break;
            case 2:
                result.moralesGainIncrease = passiveEffectData.moralesGainIncrease;
                break;
            case 3:
                result.moralesLoseDecrease = passiveEffectData.moralesLoseDecrease;
                break;
            default:
                break;
        }
        return result;
    }
}
