﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

/// <summary>
/// Zarządca aktywnych konfiguracji umiejętności.
/// </summary>
[CreateAssetMenu(fileName = "SkillProviderManager", menuName = "SkillProviderManager")]
public class SkillProviderManager : ScriptableObject
{
    /// <summary>
    /// Konfiguracje umiejętności.
    /// </summary>
    [SerializeField] List<SkillProviderReference> skills;

    /// <summary>
    /// Pobranie kolekcji konfiguracji umiejętności
    /// </summary>
    /// <returns>Kolekcja konfiguracji umiejętności zabezpieczona przed modyfikacją.</returns>
    public ReadOnlyCollection<SkillProviderReference> Skills => skills.AsReadOnly();
}
