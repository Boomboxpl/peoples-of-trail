﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Komponent ustawiający jednostkę cieniującą dla pojedyncznego elementu tła.
/// </summary>
public class BackgroundRenderer : MonoBehaviour
{
    /// <summary>
    /// Nazwa tekstury z przemiszeczeniem w jednostce cieniującej.
    /// </summary>
    const string textureName = "_DisplacementTex";
    /// <summary>
    /// Nazwa wartości przesunięcia koloru w jednostce cieniującej.
    /// </summary>
    const string displacementTimeName = "_DisplacementTime";
    /// <summary>
    /// Identyfikator wartości przesunięcia koloru w jednostce cieniującej.
    /// </summary>
    static readonly int displacementTimeId = Shader.PropertyToID(displacementTimeName);

    /// <summary>
    /// Informacja, czy element tła ma automatycznie dezaktywować się po wyjściu poza zasięg widoku kamery.
    /// </summary>
    public bool hideOnInvisible = true;
    /// <summary>
    /// Skala czasu animacji.
    /// </summary>
    public float timeScale = 0.2f;
    /// <summary>
    /// Rozdzielczość wygenerowanej tekstury z przemiesczeniem.
    /// </summary>
    public Vector2Int displacementResolution;
    /// <summary>
    /// Generator szumu dla przemieszczenia.
    /// </summary>
    public NoiseGenerator noiseGenerator;

    /// <summary>
    /// Komponent odpowiedzialny za rysowanie.
    /// </summary>
    SpriteRenderer spriteRenderer;
    /// <summary>
    /// Tekstura, w której znajdzie się przemiszczenie.
    /// </summary>
    Texture2D texture;

    /// <summary>
    /// Inicjalizacja. Wysłanie parametrów do jednostki cieniującej.
    /// </summary>
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        MaterialPropertyBlock block = new MaterialPropertyBlock();
        texture = CreateDisplacementTexture();
        spriteRenderer.GetPropertyBlock(block);
        block.SetTexture(textureName, texture);
        spriteRenderer.SetPropertyBlock(block);
    }

    /// <summary>
    /// Aktualizacaja animacji w każdej klatce.
    /// </summary>
    void Update()
    {
        spriteRenderer.material.SetFloat(displacementTimeId, timeScale * Time.time);
    }

    /// <summary>
    /// Ukrywanie nieaktywnego obiektu.
    /// </summary>
    void OnBecameInvisible()
    {
        if (hideOnInvisible)
        {
            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Funkcja generująca teksturę z szumem.
    /// </summary>
    /// <returns>Tekstura z szumem.</returns>
    Texture2D CreateDisplacementTexture()
    {
        noiseGenerator.PrepareNoise(new System.Random());
        Texture2D result = new Texture2D(displacementResolution.x, displacementResolution.y);
        Color[] pixels = new Color[displacementResolution.x * displacementResolution.y];
        for (int i = 0; i < displacementResolution.y; i++)
        {
            for (int j = 0; j < displacementResolution.x; j++)
            {
                float noiseValue = noiseGenerator.Get(j, i);
                pixels[i* displacementResolution.x + j] = new Color(noiseValue, noiseValue, noiseValue);
            }
        }
        result.SetPixels(pixels);
        result.Apply(false);
        return result;
    }
}
