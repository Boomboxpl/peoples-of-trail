﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// Zarządca tła. Przesuwa tło tak by zachować najlepsze możliwe ułożenie kafelków tła.
/// </summary>
public class BackgroundManager : MonoBehaviour
{
    /// <summary>
    /// Prefabrykat kopiowany podczas tworzenia nowych kafelków.
    /// </summary>
    public BackgroundRenderer backgroundTilePrefab;
    /// <summary>
    /// Kamera renderująca tło. Domyślnie główna kamera.
    /// </summary>
    public Camera renderingCamera;

    /// <summary>
    /// Siatka dla tła.
    /// </summary>
    Grid grid;

    /// <summary>
    /// Utworzone kafelki.
    /// </summary>
    List<BackgroundRenderer> backgroundTiles = new List<BackgroundRenderer>();

    /// <summary>
    /// Inicjalizacja. Ustawienie siatki oraz pierwsze ustawienie tła. Przypisanie reakcji na ruch kamery.
    /// </summary>
    void Awake()
    {
        grid = GetComponent<Grid>();
        if (renderingCamera == null)
        {
            renderingCamera = Camera.main;
        }

        CameraMoveManager cameraMoveManager = renderingCamera.GetComponent<CameraMoveManager>();
        cameraMoveManager.cameraMoved += CameraMoved;

        RepositionTiles(Vector2.zero);
    }

    /// <summary>
    /// Zapewnienie w każdej klatce aktywności conajmniej jednego kafelka tła.
    /// </summary>
    void Update()
    {
        CheckIfAnyActive();
    }

    /// <summary>
    /// Usunięcie obiektu. Usunięcie przypisania reakcji na ruch kamery.
    /// </summary>
    void OnDestroy()
    {
        if (renderingCamera != null)
        {
            CameraMoveManager cameraMoveManager = renderingCamera.GetComponent<CameraMoveManager>();
            if (cameraMoveManager != null)
            {
                cameraMoveManager.cameraMoved -= CameraMoved;
            }
        }
    }

    /// <summary>
    /// Reakcja na ruch kamery. Ponowne ustawienie tła.
    /// </summary>
    /// <param name="delta">Zmiana pozycji lub rozmiaru kamery.</param>
    void CameraMoved(CameraDelta delta)
    {
        RepositionTiles(delta.moveDelta);
        if (delta.scaleDelta < 0f)
        {
            ShrinkTiles();
        }
    }

    /// <summary>
    /// Zoptymalizowanie ilości kafelków.
    /// </summary>
    void ShrinkTiles()
    {
        CheckIfAnyActive();
        List<BackgroundRenderer> unusedTiles = backgroundTiles.FindAll(tile => !tile.gameObject.activeSelf);

        foreach (var tile in unusedTiles)
        {
            backgroundTiles.Remove(tile);
            Destroy(tile.gameObject);
        }
    }

    /// <summary>
    /// Rozmieszczenie kafelków tak, by zakrywały całe tło.
    /// </summary>
    void RepositionTiles(Vector2 offset)
    {
        Vector3 leftDownWorld = renderingCamera.ViewportToWorldPoint(Vector2.zero);
        Vector3 rightDownWorld = renderingCamera.ViewportToWorldPoint(Vector2.right);
        Vector3 leftUpperWorld = renderingCamera.ViewportToWorldPoint(Vector2.up);

        if(offset.x > 0)
        {
            rightDownWorld.x += offset.x;
        }
        else
        {
            leftDownWorld.x += offset.x;
            leftUpperWorld.x += offset.x;
        }

        if(offset.y > 0)
        {
            leftUpperWorld.y += offset.y;
        }
        else
        {
            leftDownWorld.y += offset.y;
            rightDownWorld.y += offset.y;
        }

        Vector3Int leftDown = grid.WorldToCell(leftDownWorld);
        Vector3Int rightDown = grid.WorldToCell(rightDownWorld);
        Vector3Int leftUpper = grid.WorldToCell(leftUpperWorld);
        Vector2Int dimensions = new Vector2Int(rightDown.x - leftDown.x, leftUpper.y - leftDown.y);

        for (int i = 0; i <= dimensions.x; i++)
        {
            for (int j = 0; j <= dimensions.y; j++)
            {
                Vector3Int gridPosition = new Vector3Int(i + leftDown.x, j + leftDown.y, 0);
                if (!IsTilled(gridPosition))
                {
                    BackgroundRenderer tileToMove = GetNonVisibleBackgroundRenderer();
                    tileToMove.transform.position = grid.GetCellCenterWorld(gridPosition);
                    tileToMove.gameObject.SetActive(true);
                }
            }
        }
    }

    /// <summary>
    /// Sprawdzenie czy dany fragment siatki jest zasłonięty.
    /// </summary>
    /// <param name="gridPosition">Pozycja w siatce.</param>
    /// <returns>True, jeżeli jeden z kafelków znajduje się już na tej pozycji.</returns>
    bool IsTilled(Vector3Int gridPosition)
    {
        foreach (BackgroundRenderer tile in backgroundTiles)
        {
            if (tile.gameObject.activeSelf && grid.WorldToCell(tile.transform.position) == gridPosition)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Pobranie niewidocznego kafelka do ponownego użycia. W przypaku braku gotowego zostanie utworzony nowy.
    /// </summary>
    /// <returns>Nieużywany kafelek.</returns>
    BackgroundRenderer GetNonVisibleBackgroundRenderer()
    {
        BackgroundRenderer result = backgroundTiles.FirstOrDefault(tile => !tile.gameObject.activeSelf);
        if (result == null)
        {
            result = Instantiate(backgroundTilePrefab, Vector3.zero, Quaternion.identity, transform);
            backgroundTiles.Add(result);
        }
        return result;
    }

    /// <summary>
    /// Zapewnienie poprawności działania tła. 
    /// Jeżeli żaden z elementów tła nie jest aktywny aktywuje wszystkie elementy.
    /// </summary>
    void CheckIfAnyActive()
    {
        if (!backgroundTiles.Any(tile => tile.gameObject.activeSelf))
        {
            foreach (BackgroundRenderer tile in backgroundTiles)
            {
                tile.gameObject.SetActive(true);
            }
        }
    }
}
