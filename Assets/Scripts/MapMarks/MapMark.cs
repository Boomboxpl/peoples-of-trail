﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/// <summary>
/// Znacznik na mapie.
/// </summary>
public class MapMark : MonoBehaviour
{
    /// <summary>
    /// Zarządca mapy.
    /// </summary>
    public MapManager mapManager;
    /// <summary>
    /// Długość animacji przesuwania.
    /// </summary>
    public float animationDuration = 0.5f;

    /// <summary>
    /// Pozycja na mapie.
    /// </summary>
    [HideInInspector] public Vector2Int mapPosition;

    /// <summary>
    /// Ustawienie znacznika na pozycji.
    /// </summary>
    /// <param name="position">Pozycja na mapie.</param>
    /// <param name="animated">Czy ustawienie jest animowane.</param>
    /// <returns>True, gdy ustawienie zostało wykonane</returns>
    public virtual bool SetPosition(Vector2Int position, bool animated = false)
    {
        MapTile tile = mapManager.GetTileAt(position);
        if (tile.IsMovable)
        {
            mapPosition = position;
            Vector3 targetPosition = mapManager.terrarianTileMap.GetCellCenterWorld((Vector3Int)position);
            if (animated)
            {
                transform.DOMove(targetPosition, animationDuration);
            }
            else
            {
                transform.position = targetPosition;
            }
            return true;
        }
        return false;
    }

    /// <summary>
    /// Poruszenie znacznika na pozycję.
    /// </summary>
    /// <param name="position">Pozycja docelowa na mapie.</param>
    /// <param name="animated">Czy ruch jest animowany.</param>
    /// <returns>True, gdy ruch został wykonany.</returns>
    public virtual bool Move(Vector2Int position, bool animated = false)
    {
        mapPosition = position;
        transform.DOMove(mapManager.terrarianTileMap.GetCellCenterWorld((Vector3Int)position), animated ? animationDuration : 0);
        return true;
    }

    /// <summary>
    /// Poruszenie znacznika na pozycję.
    /// </summary>
    /// <param name="worldPosition">Pozycja docelowa w świecie.</param>
    /// <param name="animated">Czy ruch jest animowany.</param>
    /// <returns>True, gdy ruch został wykonany.</returns>
    public bool Move(Vector3 worldPosition, bool animated = false)
    {
        return Move((Vector2Int)mapManager.terrarianTileMap.WorldToCell(worldPosition), animated);
    }
}
