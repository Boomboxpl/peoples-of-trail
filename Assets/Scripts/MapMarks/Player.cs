﻿using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Znacznika gracza na mapie.
/// </summary>
public class Player : MapMark
{
    /// <summary>
    /// Znaczki pozycji następnego ruchu.
    /// </summary>
    public NextMoveMark nextMoveMark;

    /// <summary>
    /// Dane gracza.
    /// </summary>
    public PlayerData data;

    /// <summary>
    /// Zmiana ilości zasobów przy wykonaniu ruchu na aktualnie wybrany kafelek.
    /// </summary>
    public int NextMoveResourcesDelta { get; private set; }

    /// <summary>
    /// Ustawienie gracza na pozycji.
    /// </summary>
    /// <param name="position">Pozycja na mapie.</param>
    /// <param name="animated">Czy ruch jest animowany.</param>
    /// <returns>True, gdy ustawiono pozycję.</returns>
    public override bool SetPosition(Vector2Int position, bool animated = false)
    {
        if (base.SetPosition(position, animated))
        {
            mapManager.ClearEvent(mapPosition);
            nextMoveMark.SetPosition(position);
            GameManager.Instance.mapManager.UpdateFog();
            data.MapPosition = position;
            return true;
        }
        return false;
    }

    /// <summary>
    /// Odświeżenie pozycji gracza.
    /// </summary>
    public void RefreshPosition()
    {
        SetPosition(data.MapPosition);
        GameManager.Instance.uiManager.RefreshUI();
    }

    /// <summary>
    /// Poruszenie gracza na pozycję. Sprawdza czy można poruszyć się na daną pozycję.
    /// </summary>
    /// <param name="position">Pozycja docelowa na mapie.</param>
    /// <param name="animated">Czy ruch jest animowany.</param>
    /// <returns>True, gdy ruch został wykonany.</returns>
    public override bool Move(Vector2Int position, bool animated = false)
    {
        if (!mapManager.IsNeighbour(mapPosition, position))
        {
            return false;
        }
        data.MapPosition = position;
        GameManager.Instance.mapManager.UpdateFog();
        return base.Move(position, animated);
    }

    /// <summary>
    /// Przygotowanie ruchu gracza na pozycję. Ustawia znacznik następnego ruchu.
    /// </summary>
    /// <param name="position">Pozycja docelowa na mapie.</param>
    /// <returns>True, gdy znacznik został ustawiony.</returns>
    public bool PrepareMove(Vector2Int position)
    {
        MapTile tile = mapManager.GetTileAt(position, forceCurrentLevel: true);
        if (!mapManager.IsNeighbour(mapPosition, position) || !tile.IsMovable)
        {
            return false;
        }
        NextMoveResourcesDelta = data.PassiveEffect.ApplyResources(tile.resourcesToGather) + data.PassiveEffect.ApplyResources(-tile.resourcesToLose);
        GameManager.Instance.uiManager.RefreshUI();
        return nextMoveMark.SetPosition(position, true);
    }

    /// <summary>
    /// Przygotowanie ruchu gracza na pozycję. Ustawia znacznik następnego ruchu.
    /// </summary>
    /// <param name="worldPosition">Pozycja docelowa na mapie.</param>
    /// <returns>True, gdy znacznik został ustawiony.</returns>
    public bool PrepareMove(Vector3 worldPosition)
    {
        return PrepareMove((Vector2Int)mapManager.terrarianTileMap.WorldToCell(worldPosition));
    }

    /// <summary>
    /// Próba wykonania ruchu. 
    /// Pozwala na używanie ruchu z poziomu akcji synchronicznych.
    /// </summary>
    public void TryDoMove()
    {
        DoMove().Forget();
    }

    /// <summary>
    /// Wykonanie ruchu na pozycję znacznika.
    /// </summary>
    /// <returns>Zadanie asynchroniczne zwracające true, gdy ruch został wykonany.</returns>
    public async UniTask<bool> DoMove()
    {
        if (nextMoveMark.mapPosition != mapPosition)
        {
            GameManager.Instance.uiManager.SetBarButtonsActive(false);
            GameManager.Instance.ApplyAllLevels();
            Move(nextMoveMark.mapPosition, true);
            MapTile tile = mapManager.GetTileAt(nextMoveMark.mapPosition);
            await data.RefreshPassiveEffect();
            data.Resources += tile.resourcesToGather;
            data.Resources -= tile.resourcesToLose;
            NextMoveResourcesDelta = 0;
            tile.resourcesToGather = 0;
            data.RemovePeopleFromResourcesShortage();
            GameManager.Instance.uiManager.RefreshUI();
            tile.OnStep().Forget();
            return true;
        }
        return false;
    }
}