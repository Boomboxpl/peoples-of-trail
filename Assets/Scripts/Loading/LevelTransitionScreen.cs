﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using Cysharp.Threading.Tasks;
using System;

/// <summary>
/// Ekran przejścia mędzy poziomami gry.
/// </summary>
public class LevelTransitionScreen : MonoBehaviour
{
    /// <summary>
    /// Klucz systemu tłumaczącego tekstu opisującego każdy poziom.
    /// </summary>
    const string localizationKey = "LevelTransition";

    /// <summary>
    /// Informacja, czy ekran jest pokazywany.
    /// </summary>
    public bool IsShown { get; private set; }

    /// <summary>
    /// Czas animacji przeźroczystości ekranu w sekundach.
    /// </summary>
    public float screenFadeAnimationDuration = 1f;
    /// <summary>
    /// Czas animacji przeźroczystości tekstu  w sekundach.
    /// </summary>
    public float textFadeAnimationDuration = 1f;
    /// <summary>
    /// Czas w sekundach, o który zostanie opóźniona animacja przeźroczystości podczas ukrywania.
    /// </summary>
    public float delayBeforeHide = 2f;

    /// <summary>
    /// Komponent wyświetlający tekst na ekranie.
    /// </summary>
    public TextMeshProUGUI textComponent;
    /// <summary>
    /// Obraz wypełniający ekran.
    /// </summary>
    public Image image;

    /// <summary>
    /// Obsługa aktywacji. Ustawienie tekstu i obrazu ekranu na pełną przeźroczystość.
    /// </summary>
    void Awake()
    {
        image.raycastTarget = false;
        image.color = image.color.SetAlpha(0f);
        textComponent.color = textComponent.color.SetAlpha(0f);
    }

    /// <summary>
    /// Animacja pokazania ekranu. Najpierw pokazywany jest obraz, a następnie tekst.
    /// </summary>
    /// <returns>Zadanie asynchroniczne, kończące się po pokazaniu ekranu i przed pokazaniem tekstu.</returns>
    public async UniTask Show()
    {
        image.DOKill();
        textComponent.DOKill();

        image.raycastTarget = true;

        textComponent.text = string.Format(LocalizationManager.Localize(localizationKey),
            GameManager.Instance.mapManager.GetLevelCount());

        UniTask fadeTask = image.DOFade(1f, screenFadeAnimationDuration)
            .OnComplete(() => IsShown = true)
            .AwaitForComplete();
        textComponent.DOFade(1f, textFadeAnimationDuration)
            .SetDelay(screenFadeAnimationDuration)
            .AwaitForComplete().Forget();

        await fadeTask;
    }

    /// <summary>
    /// Animacja ukrycia ekranu. Najpierw ukrywa tekst, a następnie obraz.
    /// </summary>
    /// <returns>Zadanie asynchroniczne, kończące się po ukryciu obrazu.</returns>
    public async UniTask Hide()
    {
        await UniTask.Delay(TimeSpan.FromSeconds(delayBeforeHide));

        image.DOKill();
        textComponent.DOKill();

        await textComponent.DOFade(0f, textFadeAnimationDuration);
        await image.DOFade(0f, screenFadeAnimationDuration)
            .OnComplete(() => image.raycastTarget = false);
        IsShown = false;
    }
}
