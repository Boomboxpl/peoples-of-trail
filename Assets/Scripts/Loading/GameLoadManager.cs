﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Cysharp.Threading.Tasks;
using System;

/// <summary>
/// Zarządca ładowania scen aplikacji.
/// </summary>
public static class GameLoadManager
{
    /// <summary>
    /// Indeks sceny menu głownego.
    /// </summary>
    const int mainMenuScreenSceneIndex = 0;
    /// <summary>
    /// Indeks sceny ekranu ładowania.
    /// </summary>
    const int loadingScreenSceneIndex = 1;
    /// <summary>
    /// Indeks sceny gry.
    /// </summary>
    const int gameSceneIndex = 2;

    /// <summary>
    /// Operacja wykonywana po wyświetleniu ekranu ładowania i przed zmianą sceny.
    /// </summary>
    public static Action beforeSceneChange; 

    /// <summary>
    /// Rozpoczęcie nowej gry.
    /// </summary>
    /// <param name="saveName">Nazwa zapisu.</param>
    /// <param name="skillProviderManager">Zarządca aktywnych konfiguracji umiejętności.</param>
    public static void StartNewGame(string saveName, SkillProviderManager skillProviderManager)
    {
        StartNewGameAsync(saveName, skillProviderManager).Forget();
    }

    /// <summary>
    /// Załadowanie sceny gry.
    /// </summary>
    /// <param name="saveData">Zapis do załadowania.</param>
    /// <param name="checkPoint">Informacja, czy należy wczytać punkt kontrolny.</param>
    public static void LoadGame(SaveData saveData, bool checkPoint = false)
    {
        SaveData saveToLoad = saveData;
        if (checkPoint)
        {
            saveToLoad = saveData.checkpointSave;
            saveToLoad.SetAsCheckPoint();
        }
        LoadGameAsync(saveToLoad).Forget();
    }

    /// <summary>
    /// Załadowanie menu głównego.
    /// </summary>
    public static void LoadMainMenu()
    {
        LoadMainMenuAsync().Forget();
    }

    /// <summary>
    /// Rozpoczęcie nowej gry.
    /// </summary>
    /// <param name="saveName">Nazwa zapisu.</param>
    /// <param name="skillProviderManager">Zarządca aktywnych konfiguracji umiejętności.</param>
    /// <returns>Zadanie asynchroniczne.</returns>
    static async UniTaskVoid StartNewGameAsync(string saveName, SkillProviderManager skillProviderManager)
    {
        await LoadSceneWithLoadingScreen(gameSceneIndex, 
            async () =>
            {
                SaveData saveData = await SaveData.CreateNewSave(saveName, skillProviderManager);
                await GameManager.Instance.SetGame(saveData);
            }
        );
    }

    /// <summary>
    /// Asynchroniczne załadowanie sceny gry.
    /// </summary>
    /// <param name="saveData">Zapis do załadowania.</param>
    /// <returns>Zadanie asynchoniczne.</returns>
    static async UniTaskVoid LoadGameAsync(SaveData saveData) => await LoadSceneWithLoadingScreen(gameSceneIndex, () => GameManager.Instance.SetGame(saveData));

    /// <summary>
    /// Asynchroniczne załadowanie menu głównego.
    /// </summary>
    /// <returns>Zadanie asynchoniczne.</returns>
    static async UniTaskVoid LoadMainMenuAsync() => await LoadSceneWithLoadingScreen(mainMenuScreenSceneIndex);

    /// <summary>
    /// Asynchroniczna procedura ładowania sceny z przejściem przez ekran ładowania.
    /// </summary>
    /// <param name="targetIndex">Indeks sceny.</param>
    /// <param name="onLoadAction">Operacja wywoływana po załadowaniu poziomu. Ukrycie ekranu ładowania nastąpi po zakończeniu tej operacji.</param>
    /// <returns>Zadanie asynchroniczne.</returns>
    static async UniTask LoadSceneWithLoadingScreen(int targetIndex, Func<UniTask> onLoadAction = null)
    {
        Scene activeScene = SceneManager.GetActiveScene();

        await SceneManager.LoadSceneAsync(loadingScreenSceneIndex, LoadSceneMode.Additive);
        LoadingScreen loadingScreen = GameObject.FindObjectOfType<LoadingScreen>();
        Camera loadingScreenCamera = loadingScreen.loadingScreenCamera;
        AudioListener loadingAudioListener = loadingScreenCamera.GetComponent<AudioListener>();
        loadingScreenCamera.gameObject.SetActive(false);
        loadingAudioListener.enabled = false;
        await loadingScreen.Show();
        loadingScreenCamera.gameObject.SetActive(true);

        beforeSceneChange?.Invoke();
        await SceneManager.UnloadSceneAsync(activeScene);
        loadingScreenCamera.GetComponent<AudioListener>().enabled = true;

        await SceneManager.LoadSceneAsync(targetIndex, LoadSceneMode.Additive);
        loadingScreenCamera.GetComponent<AudioListener>().enabled = false;

        if (onLoadAction != null)
        {
            await onLoadAction.Invoke();
        }
        await loadingScreen.Hide();
        await SceneManager.UnloadSceneAsync(loadingScreenSceneIndex);
    }
}
