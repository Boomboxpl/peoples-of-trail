﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;
using DG.Tweening;

/// <summary>
/// Ekran ładowania sceny.
/// </summary>
[RequireComponent(typeof(CanvasGroup))]
public class LoadingScreen : MonoBehaviour
{
    /// <summary>
    /// Kamera na scenie z ekranem ładowania.
    /// </summary>
    public Camera loadingScreenCamera;
    /// <summary>
    /// Czas animacji pokazywania ekranu w sekundach.
    /// </summary>
    public float showingDuration = 1f;
    /// <summary>
    /// Czas opóźnienia ukrywania w sekundach.
    /// </summary>
    public float hidingDelay;
    /// <summary>
    /// Czas animacji ukrywania w sekundach.
    /// </summary>
    public float hidingDuration;
    /// <summary>
    /// Ustawienie krzywej animacji pokazywania ekranu ładowania.
    /// </summary>
    public Ease showEase;
    /// <summary>
    /// Ustawienie krzywej animacji ukrywania ekranu ładowania.
    /// </summary>
    public Ease hideEase;
    /// <summary>
    /// Zarządca przeźroczystości obiektów ekranu.
    /// </summary>
    CanvasGroup canvasGroup;

    /// <summary>
    /// Obsługa aktywacji. Ustawienie pełnej przeźroczystości ekranu.
    /// </summary>
    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        canvasGroup.alpha = 0f;
    }

    /// <summary>
    /// Animacja pokazywania.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async UniTask Show()
    {
        await canvasGroup.DOFade(1f, showingDuration).SetEase(showEase);
    }

    /// <summary>
    /// Animacja ukrywania.
    /// </summary>
    /// <returns>Zadanie asynchroniczne.</returns>
    public async UniTask Hide()
    {
        await canvasGroup.DOFade(0f, hidingDuration).SetDelay(hidingDelay).SetEase(hideEase);
    }
}
