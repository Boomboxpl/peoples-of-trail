﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Zarządca ruchu kamery.
/// </summary>
public partial class CameraMoveManager : MonoBehaviour
{
    /// <summary>
    /// Ustawienie reakcji na akcje z strony użytkownika.
    /// </summary>
    void SetActions()
    {
        InputManager inputManagerInstance = InputManager.Instance;
        inputManagerInstance.SetPressCallback(InputAction.Up, JumpUp);
        inputManagerInstance.SetPressCallback(InputAction.Down, JumpDown);
        inputManagerInstance.SetPressCallback(InputAction.Left, JumpLeft);
        inputManagerInstance.SetPressCallback(InputAction.Right, JumpRight);

        inputManagerInstance.SetHoldCallback(InputAction.Up, MoveUp);
        inputManagerInstance.SetHoldCallback(InputAction.Down, MoveDown);
        inputManagerInstance.SetHoldCallback(InputAction.Left, MoveLeft);
        inputManagerInstance.SetHoldCallback(InputAction.Right, MoveRight);

        inputManagerInstance.SetPressCallback(InputAction.ZoomIn, ZoomInMajor);
        inputManagerInstance.SetPressCallback(InputAction.ZoomOut, ZoomOutMajor);

        inputManagerInstance.SetHoldCallback(InputAction.ZoomIn, ZoomInMinor);
        inputManagerInstance.SetHoldCallback(InputAction.ZoomOut, ZoomOutMinor);
    }

    /// <summary>
    /// Usunięcie reakcji na akcje z strony użytkownika.
    /// </summary>
    void ClearActions()
    {
        InputManager inputManagerInstance = InputManager.Instance;
        if(inputManagerInstance == null)
        {
            return;
        }
        inputManagerInstance.ClearPressCallback(InputAction.Up, JumpUp);
        inputManagerInstance.ClearPressCallback(InputAction.Down, JumpDown);
        inputManagerInstance.ClearPressCallback(InputAction.Left, JumpLeft);
        inputManagerInstance.ClearPressCallback(InputAction.Right, JumpRight);

        inputManagerInstance.ClearHoldCallback(InputAction.Up, MoveUp);
        inputManagerInstance.ClearHoldCallback(InputAction.Down, MoveDown);
        inputManagerInstance.ClearHoldCallback(InputAction.Left, MoveLeft);
        inputManagerInstance.ClearHoldCallback(InputAction.Right, MoveRight);

        inputManagerInstance.ClearPressCallback(InputAction.ZoomIn, ZoomInMajor);
        inputManagerInstance.ClearPressCallback(InputAction.ZoomOut, ZoomOutMajor);

        inputManagerInstance.ClearHoldCallback(InputAction.ZoomIn, ZoomInMinor);
        inputManagerInstance.ClearHoldCallback(InputAction.ZoomOut, ZoomOutMinor);
    }

    /// <summary>
    /// Daleki ruch kamery w górę.
    /// </summary>
    void JumpUp() => MoveCamera(Vector3.up * 100, true);

    /// <summary>
    /// Daleki ruch kamery w dół.
    /// </summary>
    void JumpDown() => MoveCamera(Vector3.down * 100, true);

    /// <summary>
    /// Daleki ruch kamery w lewo.
    /// </summary>
    void JumpLeft() => MoveCamera(Vector3.left * 100, true);

    /// <summary>
    /// Daleki ruch kamery w prawo.
    /// </summary>
    void JumpRight() => MoveCamera(Vector3.right * 100, true);

    /// <summary>
    /// Krótki ruch kamery w górę.
    /// </summary>
    void MoveUp() => MoveCamera(Vector3.up);

    /// <summary>
    /// Krótki ruch kamery w dół.
    /// </summary>
    void MoveDown() => MoveCamera(Vector3.down);

    /// <summary>
    /// Krótki ruch kamery w lewo.
    /// </summary>
    void MoveLeft() => MoveCamera(Vector3.left);

    /// <summary>
    /// Krótki ruch kamery w prawo.
    /// </summary>
    void MoveRight() => MoveCamera(Vector3.right);

    /// <summary>
    /// Duże przybliżenie.
    /// </summary>
    void ZoomInMajor() => ZoomCamera(-1f);

    /// <summary>
    /// Duże oddalenie.
    /// </summary>
    void ZoomOutMajor() => ZoomCamera(1f);

    /// <summary>
    /// Małe przybliżenie.
    /// </summary>
    void ZoomInMinor() => ZoomCamera(-0.1f);

    /// <summary>
    /// Małe oddalenie.
    /// </summary>
    void ZoomOutMinor() => ZoomCamera(0.1f);
}
