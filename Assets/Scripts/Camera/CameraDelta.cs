﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Struktura przenosząca informacje o zmianie pozycji i rozmiaru kamery.
/// </summary>
public struct CameraDelta
{
    /// <summary>
    /// Zmiana pozycji.
    /// </summary>
    public Vector3 moveDelta;
    /// <summary>
    /// Zmiana rozmiaru.
    /// </summary>
    public float scaleDelta;

    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="moveDelta">Zmiana pozycji.</param>
    /// <param name="scaleDelta">Zmiana skali.</param>
    public CameraDelta(Vector3 moveDelta, float scaleDelta)
    {
        this.moveDelta = moveDelta;
        this.scaleDelta = scaleDelta;
    }

    /// <summary>
    /// Konstruktor. Zmiana skali ustawiana na 0.
    /// </summary>
    /// <param name="moveDelta">Zmiana pozycji.</param>
    public CameraDelta(Vector3 moveDelta) : this(moveDelta, 0f)
    {
    }

    /// <summary>
    /// Konstruktor. Zmiana pozycji ustawiana na (0,0,0).
    /// </summary>
    /// <param name="scaleDelta">Zmiana skali.</param>
    public CameraDelta(float scaleDelta) : this(Vector3.zero, scaleDelta)
    {
    }
}
