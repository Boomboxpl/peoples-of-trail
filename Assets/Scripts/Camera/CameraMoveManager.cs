﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;

/// <summary>
/// Zarządca ruchu kamery.
/// </summary>
public partial class CameraMoveManager : MonoBehaviour
{
    /// <summary>
    /// Szybkość poruszania się kamery.
    /// </summary>
    public float cameraSpeed = 1f;
    /// <summary>
    /// Długość animacji przesuwania.
    /// </summary>
    public float animationDuration = 1f;

    /// <summary>
    /// Minimalny rozmiar kamery podczas przybliżania.
    /// </summary>
    public float minCameraSize = 2f;
    /// <summary>
    /// Maksymalny rozmiar kamery podczas przybliżania.
    /// </summary>
    public float maxCameraSize = 20f;

    /// <summary>
    /// Zdarzenie wywoływane po ruchu kamery.
    /// </summary>
    public Action<CameraDelta> cameraMoved;

    /// <summary>
    /// Kamera kontrolowana przez skrypt.
    /// </summary>
    public Camera ControlledCamera { get; private set; }

    /// <summary>
    /// Poziom przybliżenia w przedziale (0,1).
    /// </summary>
    /// <returns>Rozmiar przybliżenia.</returns>
    public float CameraNormalizedZoom => Mathf.InverseLerp(minCameraSize, maxCameraSize, ControlledCamera?.orthographicSize ?? minCameraSize);

    /// <summary>
    /// Inicializaja. Ustawienie operacji po wystąpieniu akcji z strony użytkownika.
    /// </summary>
    void Awake()
    {
        ControlledCamera = GetComponent<Camera>();
        SetActions();
    }

    /// <summary>
    /// Obsługa usunięcia obiektu. Usunięcie operacji po wystąpieniu akcji z strony użytkownika.
    /// </summary>
    void OnDestroy()
    {
        ClearActions();
    }

    /// <summary>
    /// Ustawienie kamery nad pozycją gracza.
    /// </summary>
    /// <param name="animated">Czy ruch jest animowany.</param>
    public void SetCameraOnPlayer(bool animated = false)
    {
        SetCamera(GameManager.Instance.player.transform.position, animated);
    }

    /// <summary>
    /// Ustawienie pozycji kamery.
    /// </summary>
    /// <param name="position">Nowa pozycja kamery.</param>
    /// <param name="animated">Czy ustawienie jest animowane.</param>
    public void SetCamera(Vector2 position, bool animated = false)
    {
        Vector3 positionWithZ = new Vector3(position.x, position.y, transform.position.z);
        Vector3 delta = transform.position - positionWithZ;
        if (delta != Vector3.zero)
        {
            CameraDelta cameraDelta = new CameraDelta(delta);
            if (animated)
            {
                transform.DOMove(positionWithZ, animationDuration).OnUpdate(() => cameraMoved?.Invoke(cameraDelta));
            }
            else
            {
                transform.position = positionWithZ;
                cameraMoved?.Invoke(cameraDelta);
            }
        }
    }

    /// <summary>
    /// Przesunięcie kamery.
    /// </summary>
    /// <param name="direction">Wartość przesunięcia.</param>
    /// /// <param name="animated">Czy ruch jest animowany.</param>
    public void MoveCamera(Vector3 direction, bool animated = false)
    {
        Vector3 delta = direction * Time.deltaTime * cameraSpeed * ControlledCamera.orthographicSize;
        if (delta != Vector3.zero)
        {
            if (animated)
            {
                transform.DOMove(transform.position + delta, animationDuration);
            }
            else
            {
                transform.position += delta;
            }

            cameraMoved?.Invoke(new CameraDelta(delta));
        }
    }

    /// <summary>
    /// Zmiana rozmiaru kamery.
    /// </summary>
    /// <param name="value">Wartość przybliżenia.</param>
    public void ZoomCamera(float value)
    {
        float newSize = Mathf.Clamp(ControlledCamera.orthographicSize + value, minCameraSize, maxCameraSize);
        SetCameraZoom(newSize);
    }

    /// <summary>
    /// Ustawienie rozmiaru kamery.
    /// </summary>
    /// <param name="newSize">Nowy rozmiar kamery.</param>
    /// <param name="normalized">Czy wartość rozmiaru jest znormalizowana do wartości od 0 do 1.</param>
    public void SetCameraZoom(float newSize, bool normalized = false)
    {
        if (normalized)
        {
            newSize = Mathf.Lerp(minCameraSize, maxCameraSize, newSize);
        }

        float delta = ControlledCamera.orthographicSize - newSize;
        if (!Mathf.Approximately(delta, 0f))
        {
            ControlledCamera.orthographicSize = newSize;
            cameraMoved?.Invoke(new CameraDelta(delta));
        }
    }
}
