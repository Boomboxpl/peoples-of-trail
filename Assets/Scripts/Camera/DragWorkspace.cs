﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Przestrzeń robocza, w której następuje przesuwanie kamery/mapy. Może być częściowo zasłonięta przez inne elementy interfejsu.
/// </summary>
public class DragWorkspace : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    /// <summary>
    /// Zarządca ruchu kamery.
    /// </summary>
    public CameraMoveManager cameraMoveManager;

    /// <summary>
    /// Próg dystansu pokonanego podczas jednego naciśnięcia, po którym przytrzymanie uznawane jest za przeciągnięcie mapy a nie za wybranie kafelka.
    /// </summary>
    public float dragDistanceThreshold;

    /// <summary>
    /// Informacja, czy kursor znajduje się nad przestrzenią.
    /// </summary>
    public bool IsPointerOver { get; private set; }

    /// <summary>
    /// Pozycja kursora w ostatniej klatce. W przestrzeni świata.
    /// </summary>
    Vector3? lastPositionWorld;
    /// <summary>
    /// Dystans jaki pokonano podczas naciśnięcia.
    /// </summary>
    float dragDistance;
    /// <summary>
    /// Korutnyas przeciągania mapy.
    /// </summary>
    Coroutine dragCoroutine;

    /// <summary>
    /// Inicjalizacja. Ustawienie operacji po wystąpieniu akcji z strony użytkownika.
    /// </summary>
    void Awake()
    {
        IsPointerOver = false;
        InputManager.Instance.SetReleaseCallback(InputAction.Press, OnPointerUp);
    }

    /// <summary>
    /// Obsługa usunięcia obiektu. Usunięcie operacji po wystąpieniu akcji z strony użytkownika.
    /// </summary>
    void OnDestroy() => InputManager.Instance?.ClearReleaseCallback(InputAction.Press, OnPointerUp);

    /// <summary>
    /// Aktualizacja po zakończeniu klatki. Odświeżenie pozycji kursora. Przybliżenie lub oddalenie kamery, jeżeli poruszono rolką myszy z kursorem nad przestrzenią.
    /// </summary>
    void LateUpdate()
    {
        if (IsPointerOver)
        {
            cameraMoveManager.ZoomCamera(-Input.mouseScrollDelta.y);
        }
    }

    /// <summary>
    /// Obsługa wejścia kursora w przestrzeń roboczą.
    /// </summary>
    /// <param name="eventData">Informacje o zdarzeniu.</param>
    public void OnPointerEnter(PointerEventData eventData)
    {
        IsPointerOver = true;
        if(dragCoroutine != null)
        {
            lastPositionWorld = worldMousePosition;
        }
    }

    /// <summary>
    /// Obsługa wyjścia kursora z przestrzeni roboczej.
    /// </summary>
    /// <param name="eventData">Informacje o zdarzeniu.</param>
    public void OnPointerExit(PointerEventData eventData)
    {
        IsPointerOver = false;
    }

    /// <summary>
    /// Obsługa naciśnięcia przestrzeni roboczej.
    /// </summary>
    /// <param name="eventData">Informacje o zdarzeniu.</param>
    public void OnPointerDown(PointerEventData eventData)
    {
        if (dragCoroutine != null)
        {
            StopCoroutine(dragCoroutine);
        }
        dragCoroutine = StartCoroutine(DragCoroutine());
    }

    /// <summary>
    /// Obsługa puszczenia naciśnięcia.
    /// </summary>
    /// <param name="eventData">Informacje o zdarzeniu.</param>
    public void OnPointerUp()
    {
        if (dragCoroutine != null)
        {
            StopCoroutine(dragCoroutine);
            dragCoroutine = null;
            if (dragDistance < dragDistanceThreshold)
            {
                GameManager.Instance.player.PrepareMove(cameraMoveManager.ControlledCamera.ScreenToWorldPoint(Input.mousePosition));
            }
            lastPositionWorld = null;
            dragDistance = 0f;
        }
    }

    /// <summary>
    /// Obsługa przesuwania kamery.
    /// </summary>
    /// <returns>Korutyna.</returns>
    IEnumerator DragCoroutine()
    {
        while (true)
        {
            if (IsPointerOver)
            {
                if (lastPositionWorld == null)
                {
                    lastPositionWorld = worldMousePosition;
                }
                else
                {
                    Vector3 mouseWorld = worldMousePosition;
                    Vector3 delta = mouseWorld - lastPositionWorld.Value;
                    dragDistance += delta.magnitude;
                    cameraMoveManager.SetCamera(cameraMoveManager.ControlledCamera.transform.position - delta);
                }
            }
            yield return null;
        }
    }

    /// <summary>
    /// Pozycja myszki w przstrzeni świata.
    /// </summary>
    Vector3 worldMousePosition => cameraMoveManager.ControlledCamera.ScreenToWorldPoint(Input.mousePosition);
}
