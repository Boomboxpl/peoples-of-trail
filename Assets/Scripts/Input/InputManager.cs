﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.EventSystems;
using Cysharp.Threading.Tasks;

/// <summary>
/// Zarządca sterowania.
/// </summary>
public class InputManager : BaseInput
{
    /// <summary>
    /// Nazwa pliku z zapisem ustawień.
    /// </summary>
    const string savesFileName = "input.bin";

    /// <summary>
    /// Czas w sekundach pomiędzy przyciśnięciem a przytrzymaniem.
    /// </summary>
    public float defaultHoldThresshold = 0.1f;

    /// <summary>
    /// Instancja wzorca Singleton.
    /// </summary>
    public static InputManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<InputManager>();
            }

            return _instance;
        }
    }
    /// <summary>
    /// Instancja wzorca Singleton.
    /// </summary>
    static InputManager _instance;

    /// <summary>
    /// Akcje zmapowane na przyciski i~operacje wykonywane po zmianie stanu klawiszy.
    /// </summary>
    Dictionary<InputAction, KeyActionData> mappedActions = new Dictionary<InputAction, KeyActionData>();

    /// <summary>
    /// Inicjalizacja. Zapewnienie wzorca singleton. Odczytanie ustawień sterowania lub utworzenie domyślnych.
    /// </summary>
    protected override async void Awake()
    {
        if (Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        if(!await GetSettingFromFile())
        {
            SetDefault();
            SaveSettingsToFile();
        }
        SetHoldThreshold(InputAction.Press, 0f);
    }

    /// <summary>
    /// Aktualizacja stanów akcji w każdej klatce.
    /// </summary>
    void Update()
    {
        foreach (KeyActionData keyAction in mappedActions.Values)
        {
            if (!keyAction.isHolded && !keyAction.isMuted)
            {
                KeyCode callingKey = KeyCode.None;
                if (Input.GetKey(keyAction.alternativeKey))
                {
                    callingKey = keyAction.alternativeKey;
                }
                else if (Input.GetKey(keyAction.key))
                {
                    callingKey = keyAction.key;
                }

                if (callingKey != KeyCode.None)
                {
                    StartCoroutine(HoldingKeyDetectionCoroutine(keyAction, callingKey));
                }
            }
        }
    }

    /// <summary>
    /// Pobranie wartości osi wirtualnej.
    /// </summary>
    /// <param name="axisName">Nazwa osi.</param>
    /// <returns>Wartość wychylenia wybranej osi.</returns>
    public override float GetAxisRaw(string axisName)
    {
        switch (axisName)
        {
            case "Horizontal":
                return GetAction(InputAction.Right).To01() - GetAction(InputAction.Left).To01();
            case "Vertical":
                return GetAction(InputAction.Up).To01() - GetAction(InputAction.Down).To01();
            default:
                return base.GetAxisRaw(axisName);
        }
    }

    /// <summary>
    /// Sprawdzenie czy w danej klatce przycisk został wciśnięty. Pozwala na nadpisanie ustawień nawigacji w menu.
    /// </summary>
    /// <param name="buttonName">Nazwa przycisku.</param>
    /// <returns>True, jeżeli przycisk został wciśnięty w danej klatce.</returns>
    public override bool GetButtonDown(string buttonName)
    {
        switch (buttonName)
        {
            case "Horizontal":
                return false;
            case "Vertical":
                return false;
            case "Submit":
                return GetActionDown(InputAction.Submit);
            case "Cancel":
                return GetActionDown(InputAction.Back);
            default:
                return base.GetButtonDown(buttonName);
        }
    }

    /// <summary>
    /// Sprawdzenie czy przycisk myszy został wciśnięty w danej klatce. Pozwala na nadpisanie ustawień nawigacji w menu.
    /// </summary>
    /// <param name="button">Identyfikator przciksu myszy. Parametr ignorowany.</param>
    /// <returns>True, jeżeli przycisk ,,Naciśnięcie" został wciśnięty w danej klatce.</returns>
    public override bool GetMouseButtonDown(int button)
    {
        return GetActionDown(InputAction.Press);
    }

    /// <summary>
    /// Sprawdzenie czy przycisk myszy jest wciśnięty. Pozwala na nadpisanie ustawień nawigacji w menu.
    /// </summary>
    /// <param name="button">Identyfikator przciksu myszy. Parametr ignorowany.</param>
    /// <returns>True, jeżeli przycisk ,,Naciśnięcie" jest wciśnięty.</returns>
    public override bool GetMouseButton(int button)
    {
        return GetAction(InputAction.Press);
    }

    /// <summary>
    /// Sprawdzenie czy przycisk myszy został puszczony w danej klatce. Pozwala na nadpisanie ustawień nawigacji w menu.
    /// </summary>
    /// <param name="button">Identyfikator przciksu myszy. Parametr ignorowany.</param>
    /// <returns>True, jeżeli przycisk ,,Naciśnięcie" został puszczony w danej klatce.</returns>
    public override bool GetMouseButtonUp(int button)
    {
        return GetActionUp(InputAction.Press);
    }

    /// <summary>
    /// Sprawdzenie czy akcja jest aktywna.
    /// </summary>
    /// <param name="action">Akcja.</param>
    /// <returns>True, jeżeli chociaż jeden z klawiszy przypisanych do akcji jest wciśnięty.</returns>
    public bool GetAction(InputAction action)
    {
        AddIfNoExist(action);
        KeyActionData data = mappedActions[action];
        return Input.GetKey(data.key) || Input.GetKey(data.alternativeKey);
    }

    /// <summary>
    /// Sprawdzenie czy akcja została aktywowana w danej klatce.
    /// </summary>
    /// <param name="action">Akcja.</param>
    /// <returns>True, jeżeli chociaż jeden z klawiszy przypisanych do akcji został wciśnięty w danej klatce.</returns>
    public bool GetActionDown(InputAction action)
    {
        AddIfNoExist(action);
        KeyActionData data = mappedActions[action];
        return Input.GetKeyDown(data.key) || Input.GetKeyDown(data.alternativeKey);
    }

    /// <summary>
    /// Sprawdzenie czy akcja została dezaktywowana w danej klatce.
    /// </summary>
    /// <param name="action">Akcja.</param>
    /// <returns>True, jeżeli chociaż jeden z klawiszy przypisanych do akcji został puszczony w danej klatce.</returns>
    public bool GetActionUp(InputAction action)
    {
        AddIfNoExist(action);
        KeyActionData data = mappedActions[action];
        return Input.GetKeyUp(data.key) || Input.GetKeyUp(data.alternativeKey);
    }

    /// <summary>
    /// Ustawienie przycisków przypisanych do akcji.
    /// </summary>
    /// <param name="action">Akcja.</param>
    /// <param name="key">Przycisk podstawowy</param>
    /// <param name="alternativeKey">Przycisk alternatywny.</param>
    public void SetInputKey(InputAction action, KeyCode key, KeyCode alternativeKey = KeyCode.None)
    {
        if (key == KeyCode.None)
        {
            return;
        }
        AddIfNoExist(action);
        mappedActions[action].key = key;
        mappedActions[action].alternativeKey = alternativeKey;
    }

    /// <summary>
    /// Pobranie klawiszy dla akcji.
    /// </summary>
    /// <param name="action">Akcja.</param>
    /// <param name="key">Klawisz podstawowy.</param>
    /// <param name="alternativeKey">Klawisz alternatywny.</param>
    /// <returns>True, gdy dane zostały poprawnie pobrane.</returns>
    public bool GetInputKeys(InputAction action, out KeyCode key, out KeyCode alternativeKey)
    {
        key = KeyCode.None;
        alternativeKey = KeyCode.None;
        if (!mappedActions.ContainsKey(action))
        {
            return false;
        }
        KeyActionData data = mappedActions[action];
        key = data.key;
        alternativeKey = data.alternativeKey;
        return true;
    }

    /// <summary>
    /// Ustawienie operacji wywoływanej po wystąpieniu naciśnięcia przycisku.
    /// </summary>
    /// <param name="action">Akcja.</param>
    /// <param name="callback">Operacja wywoływana po wystąpieniu akcji.</param>
    public void SetPressCallback(InputAction action, Action callback)
    {
        AddIfNoExist(action);
        mappedActions[action].callbackPress += callback;
    }

    /// <summary>
    /// Usunięcie operacji wywoływanej po wystąpieniu naciśnięcia przycisku.
    /// </summary>
    /// <param name="action">Akcja.</param>
    /// <param name="callback">Operacja wywoływana po wystąpieniu akcji.</param>
    public void ClearPressCallback(InputAction action, Action callback)
    {
        AddIfNoExist(action);
        mappedActions[action].callbackPress -= callback;
    }

    /// <summary>
    /// Ustawienie operacji wywoływanej w każdej klatce podczas przytrzymania przycisku.
    /// </summary>
    /// <param name="action">Akcja.</param>
    /// <param name="callback">Operacja wywoływana w każdej klatce podczas przytrzymania akcji.</param>
    public void SetHoldCallback(InputAction action, Action callback)
    {
        AddIfNoExist(action);
        mappedActions[action].callbackHold += callback;
    }

    /// <summary>
    /// Usunięcie operacji wywoływanej  w każdej klatce podczas przytrzymania przycisku.
    /// </summary>
    /// <param name="action">Akcja.</param>
    /// <param name="callback">Operacja wywoływana w każdej klatce podczas przytrzymania akcji.</param>
    public void ClearHoldCallback(InputAction action, Action callback)
    {
        AddIfNoExist(action);
        mappedActions[action].callbackHold -= callback;
    }

    /// <summary>
    /// Ustawienie operacji wywoływanej po wystąpieniu puszczenia przycisku.
    /// </summary>
    /// <param name="action">Akcja.</param>
    /// <param name="callback">Operacja wywoływana po wystąpieniu akcji.</param>
    public void SetReleaseCallback(InputAction action, Action callback)
    {
        AddIfNoExist(action);
        mappedActions[action].callbackRelease += callback;
    }

    /// <summary>
    /// Usunięcie operacji wywoływanej po wystąpieniu puszczenia przycisku.
    /// </summary>
    /// <param name="action">Akcja.</param>
    /// <param name="callback">Operacja wywoływana po wystąpieniu akcji.</param>
    public void ClearReleaseCallback(InputAction action, Action callback)
    {
        AddIfNoExist(action);
        mappedActions[action].callbackRelease -= callback;
    }

    /// <summary>
    /// Ustawienie innego niż domyślny czasu pomiędzy przyciśnięciem a przytrzymaniem.
    /// </summary>
    /// <param name="action">Akcja.</param>
    /// <param name="threshold">Wartość czasu w sekundach. Jeżeli null zostanie użyty domyślny czas.</param>
    public void SetHoldThreshold(InputAction action, float? threshold)
    {
        AddIfNoExist(action);
        mappedActions[action].customHoldThreshold = threshold;
    }

    /// <summary>
    /// Ustawienie wyciszenia akcji. Wyciszone akcje nie wywołują przypisanych operacji.
    /// </summary>
    /// <param name="action">Akcja.</param>
    /// <param name="isMuted">Informacja, czy akcja będzie wyciszona.</param>
    public void SetMute(InputAction action, bool isMuted)
    {
        AddIfNoExist(action);
        mappedActions[action].isMuted = isMuted;
    }

    /// <summary>
    /// Odczytanie wciśniętego przycisku.
    /// </summary>
    /// <returns>Kod wciśniętego przycisku.</returns>
    public static KeyCode GetDownButton()
    {
        foreach (KeyCode value in Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKeyDown(value))
            {
                return value;
            }
        }
        return KeyCode.None;
    }

    /// <summary>
    /// Zapis ustawień do pliku.
    /// </summary>
    public void SaveSettingsToFile() => BinnarySerializer.Serialize(savesFileName, mappedActions).Forget();

    /// <summary>
    /// Pobranie ustawień z pliku.
    /// </summary>
    /// <returns>Zadanie asynchroniczne zwracające true, gdy operacja odcztu powiodła się.</returns>
    public async UniTask<bool> GetSettingFromFile()
    {
        Dictionary<InputAction, KeyActionData> dictionary = 
            await BinnarySerializer.Deserialize<Dictionary<InputAction, KeyActionData>>(savesFileName);
        if(dictionary != null)
        {
            mappedActions = dictionary;
            return true;
        }
        return false;
    }

    /// <summary>
    /// Ustawienie domyślnych ustawień sterowania.
    /// </summary>
    public void SetDefault()
    {
        SetInputKey(InputAction.Up, KeyCode.W, KeyCode.UpArrow);
        SetInputKey(InputAction.Down, KeyCode.S, KeyCode.DownArrow);
        SetInputKey(InputAction.Left, KeyCode.A, KeyCode.LeftArrow);
        SetInputKey(InputAction.Right, KeyCode.D, KeyCode.RightArrow);
        SetInputKey(InputAction.ZoomIn, KeyCode.Z);
        SetInputKey(InputAction.ZoomOut, KeyCode.X);
        SetInputKey(InputAction.Press, KeyCode.Mouse0);
        SetInputKey(InputAction.Back, KeyCode.Escape);
        SetInputKey(InputAction.Submit, KeyCode.Return);
        SetInputKey(InputAction.NextTurn, KeyCode.Space);
        SetInputKey(InputAction.Menu, KeyCode.M);
        SetInputKey(InputAction.GoToCamp, KeyCode.E);
        SetInputKey(InputAction.Hero, KeyCode.Q);
    }

    /// <summary>
    /// Dodanie pustej akcji do mapowania, jeżeli nie istniała.
    /// </summary>
    /// <param name="action">Akcja.</param>
    void AddIfNoExist(InputAction action)
    {
        if (!mappedActions.ContainsKey(action))
        {
            mappedActions.Add(action, new KeyActionData());
        }
    }

    /// <summary>
    /// Korutyna obsługująca naciśnięcie/przytrzymanie klawisza.
    /// </summary>
    /// <param name="keyAction">Przyciski i operacje.</param>
    /// <param name="callingKey">Klawisz wywołujący akcję.</param>
    /// <returns>Korutyna.</returns>
    IEnumerator HoldingKeyDetectionCoroutine(KeyActionData keyAction, KeyCode callingKey)
    {
        try
        {
            keyAction.isHolded = true;
            float elapsedTime = 0f;
            float threshold = keyAction.customHoldThreshold.HasValue ? keyAction.customHoldThreshold.Value : defaultHoldThresshold;
            while (elapsedTime < threshold)
            {
                if (!Input.GetKey(callingKey))
                {
                    keyAction.callbackPress?.Invoke();
                    yield break;
                }
                elapsedTime += Time.deltaTime;
                yield return null;
            }

            if (keyAction.callbackHold == null)
            {
                keyAction.callbackPress?.Invoke();
            }
            while (Input.GetKey(callingKey))
            {
                keyAction.callbackHold?.Invoke();
                yield return null;
            };
        }
        finally
        {
            keyAction.callbackRelease?.Invoke();
            keyAction.isHolded = false;
        }
    }

    /// <summary>
    /// Dane przycisków i operacji przypisanych do akcji.
    /// </summary>
    [Serializable]
    private class KeyActionData
    {
        /// <summary>
        /// Przycisk podstawowy.
        /// </summary>
        public KeyCode key;
        /// <summary>
        /// Przycisk alternatywny.
        /// </summary>
        public KeyCode alternativeKey;
        /// <summary>
        /// Operacja wciśnięcia przycisku.
        /// </summary>
        [NonSerialized] public Action callbackPress;
        /// <summary>
        /// Operacja przytrzymania przycisku.
        /// </summary>
        [NonSerialized] public Action callbackHold;
        /// <summary>
        /// Operacja puszczenia przycisku.
        /// </summary>
        [NonSerialized] public Action callbackRelease;
        /// <summary>
        /// Informacja, czy klawisz jest podczas obsługiwania wciśnięcia.
        /// </summary>
        [NonSerialized] public bool isHolded = false;
        /// <summary>
        /// Informacja, czy akcja jest wyciszona. Wciszona akcja nie wywołuje operacji.
        /// </summary>
        [NonSerialized] public bool isMuted = false;
        /// <summary>
        /// Czas w sekundach pomiędzy przyciśnięciem a przytrzymaniem. Jeżeli null, korzysta z domyślnego.
        /// </summary>
        public float? customHoldThreshold;
    }
}