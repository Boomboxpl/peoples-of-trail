﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Moduł nadpisujący domyślny moduł sterowania Unity za pomocą akcji klasy InputManager.
/// </summary>
public class InputManagerStandaloneInputModule : StandaloneInputModule
{
    /// <summary>
    /// Obsługa aktywacji obiektu. Przypisanie nadpisania sterowania.
    /// </summary>
    protected override void Start()
    {
        base.Start();
        inputOverride = InputManager.Instance;
    }
}
