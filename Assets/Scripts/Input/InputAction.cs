﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Akcje wywoływane po naciśnięciu odpowiednich przycisków przez użytkownika.
/// </summary>
[Serializable]
public enum InputAction
{
    Up, 
    Down, 
    Left, 
    Right, 
    ZoomIn, 
    ZoomOut, 
    Press, 
    Back, 
    Submit,
    NextTurn,
    Menu,
    GoToCamp,
    Hero
}