\babel@toc {polish}{}
\contentsline {chapter}{Streszczenie}{1}{chapter*.2}%
\contentsline {chapter}{\numberline {1}Wstęp}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Cel pracy}{3}{section.1.1}%
\contentsline {section}{\numberline {1.2}Zawartość pracy}{4}{section.1.2}%
\contentsline {chapter}{\numberline {2}Analiza tematu}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}Założenia rozgrywki}{5}{section.2.1}%
\contentsline {section}{\numberline {2.2}Generacja poziomów}{6}{section.2.2}%
\contentsline {section}{\numberline {2.3}Wymagania}{10}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Funkcjonalne}{10}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Niefunkcjonalne}{10}{subsection.2.3.2}%
\contentsline {section}{\numberline {2.4}Opis narzędzi}{12}{section.2.4}%
\contentsline {subsection}{\numberline {2.4.1}Unity}{12}{subsection.2.4.1}%
\contentsline {subsubsection}{\numberline {2.4.1.1}Język C\#}{13}{subsubsection.2.4.1.1}%
\contentsline {subsubsection}{\numberline {2.4.1.2}Język ShaderLab}{13}{subsubsection.2.4.1.2}%
\contentsline {subsubsection}{\numberline {2.4.1.3}Addressables}{14}{subsubsection.2.4.1.3}%
\contentsline {subsubsection}{\numberline {2.4.1.4}TextMesh Pro}{14}{subsubsection.2.4.1.4}%
\contentsline {subsection}{\numberline {2.4.2}DoTween}{14}{subsection.2.4.2}%
\contentsline {subsection}{\numberline {2.4.3}UniTask}{15}{subsection.2.4.3}%
\contentsline {subsection}{\numberline {2.4.4}Visual studio 2019}{15}{subsection.2.4.4}%
\contentsline {chapter}{\numberline {3}Specyfikacja zewnętrzna}{17}{chapter.3}%
\contentsline {section}{\numberline {3.1}Wymagania sprzętowe i~programowe}{17}{section.3.1}%
\contentsline {section}{\numberline {3.2}Sposób obsługi}{18}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Rozgrywka}{18}{subsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.1.1}Wykonywanie tur}{18}{subsubsection.3.2.1.1}%
\contentsline {subsubsection}{\numberline {3.2.1.2}Granice poziomu}{18}{subsubsection.3.2.1.2}%
\contentsline {subsubsection}{\numberline {3.2.1.3}Mgła wojny}{20}{subsubsection.3.2.1.3}%
\contentsline {subsubsection}{\numberline {3.2.1.4}Koniec gry}{20}{subsubsection.3.2.1.4}%
\contentsline {subsubsection}{\numberline {3.2.1.5}Zasoby}{21}{subsubsection.3.2.1.5}%
\contentsline {subsubsection}{\numberline {3.2.1.6}Morale}{22}{subsubsection.3.2.1.6}%
\contentsline {subsubsection}{\numberline {3.2.1.7}Rozwój dowódcy}{22}{subsubsection.3.2.1.7}%
\contentsline {subsubsection}{\numberline {3.2.1.8}Teren}{23}{subsubsection.3.2.1.8}%
\contentsline {subsection}{\numberline {3.2.2}Obsługa menu}{24}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}Nawigacja}{28}{subsection.3.2.3}%
\contentsline {subsection}{\numberline {3.2.4}Elementy interfejsu}{29}{subsection.3.2.4}%
\contentsline {chapter}{\numberline {4}Specyfikacja wewnętrzna}{33}{chapter.4}%
\contentsline {section}{\numberline {4.1}Elementy gry}{33}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Kafelki}{33}{subsection.4.1.1}%
\contentsline {paragraph}{Metody publiczne : Klasa MapTile}{35}{section*.25}%
\contentsline {paragraph}{Atrybuty publiczne : Klasa MapTile}{35}{section*.26}%
\contentsline {paragraph}{Właściwości : Klasa MapTile}{36}{section*.27}%
\contentsline {paragraph}{Atrybuty prywatne : Klasa MapTile}{36}{section*.28}%
\contentsline {paragraph}{Statyczne atrybuty prywatne : Klasa MapTile}{36}{section*.29}%
\contentsline {subsection}{\numberline {4.1.2}Zdarzenia losowe na mapie}{36}{subsection.4.1.2}%
\contentsline {paragraph}{Metody publiczne : Klasa MapEvent}{37}{section*.30}%
\contentsline {paragraph}{Właściwości : Klasa MapEvent}{37}{section*.31}%
\contentsline {subsection}{\numberline {4.1.3}Poziomy gry}{37}{subsection.4.1.3}%
\contentsline {paragraph}{Metody publiczne : Klasa MapLevel}{37}{section*.32}%
\contentsline {paragraph}{Atrybuty publiczne : Klasa MapLevel}{38}{section*.33}%
\contentsline {paragraph}{Właściwości : Klasa MapLevel}{38}{section*.34}%
\contentsline {paragraph}{Atrybuty prywatne : Klasa MapLevel}{38}{section*.35}%
\contentsline {subsection}{\numberline {4.1.4}Postęp gracza}{39}{subsection.4.1.4}%
\contentsline {paragraph}{Metody publiczne : Klasa PlayerData}{39}{section*.36}%
\contentsline {paragraph}{Statyczne metody publiczne : Klasa PlayerData}{39}{section*.37}%
\contentsline {paragraph}{Właściwości : Klasa PlayerData}{39}{section*.38}%
\contentsline {paragraph}{Atrybuty prywatne : Klasa PlayerData}{40}{section*.39}%
\contentsline {paragraph}{Statyczne atrybuty prywatne : Klasa PlayerData}{40}{section*.40}%
\contentsline {subsection}{\numberline {4.1.5}Rozwój dowódcy}{40}{subsection.4.1.5}%
\contentsline {paragraph}{Metody publiczne : Klasa RespectData}{41}{section*.41}%
\contentsline {paragraph}{Atrybuty publiczne : Klasa RespectData}{41}{section*.42}%
\contentsline {paragraph}{Właściwości : Klasa RespectData}{41}{section*.43}%
\contentsline {paragraph}{Atrybuty prywatne : Klasa RespectData}{41}{section*.44}%
\contentsline {paragraph}{Statyczne atrybuty prywatne : Klasa RespectData}{41}{section*.45}%
\contentsline {subsection}{\numberline {4.1.6}Umiejętności dowódcy}{42}{subsection.4.1.6}%
\contentsline {paragraph}{Metody publiczne : Klasa HeroSkill}{42}{section*.46}%
\contentsline {paragraph}{Atrybuty publiczne : Klasa HeroSkill}{42}{section*.47}%
\contentsline {paragraph}{Atrybuty prywatne : Klasa HeroSkill}{42}{section*.48}%
\contentsline {subsection}{\numberline {4.1.7}Zapis gry}{42}{subsection.4.1.7}%
\contentsline {paragraph}{Metody publiczne : Klasa SaveData}{42}{section*.49}%
\contentsline {paragraph}{Statyczne metody publiczne : Klasa SaveData}{42}{section*.50}%
\contentsline {paragraph}{Atrybuty publiczne : Klasa SaveData}{43}{section*.51}%
\contentsline {section}{\numberline {4.2}Implementacja rozgrywki}{43}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Główny zarządca}{43}{subsection.4.2.1}%
\contentsline {paragraph}{Metody publiczne : Klasa GameManager}{43}{section*.52}%
\contentsline {paragraph}{Atrybuty publiczne : Klasa GameManager}{44}{section*.53}%
\contentsline {paragraph}{Właściwości : Klasa GameManager}{44}{section*.54}%
\contentsline {paragraph}{Metody prywatne : Klasa GameManager}{44}{section*.55}%
\contentsline {paragraph}{Atrybuty prywatne : Klasa GameManager}{44}{section*.56}%
\contentsline {paragraph}{Statyczne atrybuty prywatne : Klasa GameManager}{45}{section*.57}%
\contentsline {subsection}{\numberline {4.2.2}Zarządca mapy}{45}{subsection.4.2.2}%
\contentsline {paragraph}{Metody publiczne : Klasa MapManager}{45}{section*.58}%
\contentsline {paragraph}{Atrybuty publiczne : Klasa MapManager}{46}{section*.59}%
\contentsline {paragraph}{Właściwości : Klasa MapManager}{46}{section*.60}%
\contentsline {paragraph}{Metody prywatne : Klasa MapManager}{46}{section*.61}%
\contentsline {paragraph}{Statyczne metody prywatne : Klasa MapManager}{47}{section*.62}%
\contentsline {paragraph}{Atrybuty prywatne : Klasa MapManager}{47}{section*.63}%
\contentsline {subsection}{\numberline {4.2.3}Zarządca interfejsu użytkownika}{48}{subsection.4.2.3}%
\contentsline {paragraph}{Metody publiczne : Klasa UIManager}{48}{section*.64}%
\contentsline {paragraph}{Atrybuty publiczne : Klasa UIManager}{48}{section*.65}%
\contentsline {paragraph}{Właściwości : Klasa UIManager}{49}{section*.66}%
\contentsline {paragraph}{Metody prywatne : Klasa UIManager}{49}{section*.67}%
\contentsline {paragraph}{Atrybuty prywatne : Klasa UIManager}{49}{section*.68}%
\contentsline {subsection}{\numberline {4.2.4}Sterowanie}{49}{subsection.4.2.4}%
\contentsline {paragraph}{Metody publiczne : Klasa InputManager}{49}{section*.69}%
\contentsline {paragraph}{Statyczne metody publiczne : Klasa InputManager}{51}{section*.70}%
\contentsline {paragraph}{Atrybuty publiczne : Klasa InputManager}{51}{section*.71}%
\contentsline {paragraph}{Właściwości : Klasa InputManager}{52}{section*.73}%
\contentsline {paragraph}{Metody prywatne : Klasa InputManager}{52}{section*.74}%
\contentsline {paragraph}{Atrybuty prywatne : Klasa InputManager}{52}{section*.75}%
\contentsline {subsection}{\numberline {4.2.5}System tłumaczący}{54}{subsection.4.2.5}%
\contentsline {subsubsection}{\numberline {4.2.5.1}Plik z~tłumaczeniem}{54}{subsubsection.4.2.5.1}%
\contentsline {paragraph}{Metody publiczne : Klasa LocalizationReaderCSV}{54}{section*.77}%
\contentsline {paragraph}{Atrybuty publiczne : Klasa LocalizationReaderCSV}{54}{section*.78}%
\contentsline {paragraph}{Metody prywatne : Klasa LocalizationReaderCSV}{55}{section*.79}%
\contentsline {subsubsection}{\numberline {4.2.5.2}Zarządzanie tłumaczeniem}{55}{subsubsection.4.2.5.2}%
\contentsline {paragraph}{Statyczne metody publiczne : Klasa LocalizationManager}{55}{section*.80}%
\contentsline {paragraph}{Statyczne atrybuty publiczne : Klasa LocalizationManager}{55}{section*.81}%
\contentsline {paragraph}{Właściwości : Klasa LocalizationManager}{55}{section*.82}%
\contentsline {paragraph}{Statyczne atrybuty prywatne : Klasa LocalizationManager}{56}{section*.83}%
\contentsline {section}{\numberline {4.3}Konfiguracja generacji poziomów}{56}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Konfiguracja zdarzeń losowych}{56}{subsection.4.3.1}%
\contentsline {paragraph}{Atrybuty publiczne : Klasa EventProvider}{57}{section*.86}%
\contentsline {subsection}{\numberline {4.3.2}Opcje zdarzeń losowych}{57}{subsection.4.3.2}%
\contentsline {subsubsection}{\numberline {4.3.2.1}EventOptionBase}{62}{subsubsection.4.3.2.1}%
\contentsline {paragraph}{Metody publiczne : Klasa EventOptionBase}{62}{section*.89}%
\contentsline {paragraph}{Atrybuty publiczne : Klasa EventOptionBase}{62}{section*.90}%
\contentsline {paragraph}{Metody chronione : Klasa EventOptionBase}{62}{section*.91}%
\contentsline {paragraph}{Właściwości : Klasa EventOptionBase}{62}{section*.92}%
\contentsline {paragraph}{Atrybuty prywatne : Klasa EventOptionBase}{62}{section*.93}%
\contentsline {paragraph}{Metody prywatne : Klasa EventOptionBase}{63}{section*.94}%
\contentsline {subsubsection}{\numberline {4.3.2.2}StaticChanceEventOption}{63}{subsubsection.4.3.2.2}%
\contentsline {paragraph}{Atrybuty prywatne : Klasa StaticChanceEventOption}{63}{section*.95}%
\contentsline {paragraph}{Metody chronione : Klasa StaticChanceEventOption}{63}{section*.96}%
\contentsline {subsubsection}{\numberline {4.3.2.3}SkillScaledEventOption}{63}{subsubsection.4.3.2.3}%
\contentsline {paragraph}{Metody publiczne : Klasa SkillScaledEventOption}{63}{section*.97}%
\contentsline {paragraph}{Atrybuty publiczne : Klasa SkillScaledEventOption}{63}{section*.98}%
\contentsline {paragraph}{Metody chronione : Klasa SkillScaledEventOption}{64}{section*.99}%
\contentsline {paragraph}{Statyczne atrybuty prywatne : Klasa SkillScaledEventOption}{64}{section*.100}%
\contentsline {subsection}{\numberline {4.3.3}Konfiguracja umiejętności}{64}{subsection.4.3.3}%
\contentsline {paragraph}{Metody publiczne : Klasa SkillProvider}{64}{section*.103}%
\contentsline {paragraph}{Atrybuty publiczne : Klasa SkillProvider}{64}{section*.104}%
\contentsline {paragraph}{Właściwości : Klasa SkillProvider}{64}{section*.105}%
\contentsline {paragraph}{Metody prywatne : Klasa SkillProvider}{64}{section*.106}%
\contentsline {paragraph}{Atrybuty prywatne : Klasa SkillProvider}{66}{section*.107}%
\contentsline {paragraph}{Statyczne atrybuty prywatne : Klasa SkillProvider}{67}{section*.108}%
\contentsline {subsection}{\numberline {4.3.4}Konfiguracja kafelków}{67}{subsection.4.3.4}%
\contentsline {subsubsection}{\numberline {4.3.4.1}TileSet}{69}{subsubsection.4.3.4.1}%
\contentsline {paragraph}{Metody publiczne : Klasa TileSet}{69}{section*.111}%
\contentsline {paragraph}{Atrybuty publiczne : Klasa TileSet}{69}{section*.112}%
\contentsline {paragraph}{Metody prywatne : Klasa TileSet}{69}{section*.113}%
\contentsline {subsubsection}{\numberline {4.3.4.2}HeightSet}{69}{subsubsection.4.3.4.2}%
\contentsline {paragraph}{Atrybuty publiczne : Klasa HeightSet}{69}{section*.114}%
\contentsline {subsection}{\numberline {4.3.5}Konfiguracja zdarzeń wspólnych}{70}{subsection.4.3.5}%
\contentsline {paragraph}{Metody publiczne : Klasa CommonEvents}{70}{section*.115}%
\contentsline {paragraph}{Właściwości : Klasa CommonEvents}{70}{section*.116}%
\contentsline {paragraph}{Atrybuty prywatne : Klasa CommonEvents}{70}{section*.117}%
\contentsline {subsection}{\numberline {4.3.6}Generator poziomów}{70}{subsection.4.3.6}%
\contentsline {paragraph}{Metody publiczne : Klasa MapGenerator}{70}{section*.119}%
\contentsline {paragraph}{Atrybuty publiczne : Klasa MapGenerator}{72}{section*.120}%
\contentsline {paragraph}{Metody prywatne : Klasa MapGenerator}{72}{section*.121}%
\contentsline {paragraph}{Atrybuty prywatne : Klasa MapGenerator}{73}{section*.122}%
\contentsline {subsection}{\numberline {4.3.7}Generator szumów}{73}{subsection.4.3.7}%
\contentsline {paragraph}{Metody publiczne : Klasa NoiseGenerator}{73}{section*.123}%
\contentsline {paragraph}{Atrybuty publiczne : Klasa NoiseGenerator}{73}{section*.124}%
\contentsline {paragraph}{Atrybuty chronione : Klasa NoiseGenerator}{74}{section*.125}%
\contentsline {paragraph}{Statyczne atrybuty prywatne : Klasa NoiseGenerator}{74}{section*.126}%
\contentsline {section}{\numberline {4.4}Algorytm generacji poziomów}{74}{section.4.4}%
\contentsline {section}{\numberline {4.5}Zaimplementowane jednostki cieniujące}{75}{section.4.5}%
\contentsline {subsection}{\numberline {4.5.1}ColorDisplacementShader}{75}{subsection.4.5.1}%
\contentsline {subsection}{\numberline {4.5.2}SpriteStencilMask}{76}{subsection.4.5.2}%
\contentsline {chapter}{\numberline {5}Weryfikacja i~walidacja}{77}{chapter.5}%
\contentsline {section}{\numberline {5.1}Sposób testowania}{77}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Testowanie komponentów}{77}{subsection.5.1.1}%
\contentsline {subsection}{\numberline {5.1.2}Testy integracji}{77}{subsection.5.1.2}%
\contentsline {subsection}{\numberline {5.1.3}Testy systemu}{78}{subsection.5.1.3}%
\contentsline {section}{\numberline {5.2}Wykryte i~usunięte błędy}{78}{section.5.2}%
\contentsline {chapter}{\numberline {6}Podsumowanie i~wnioski}{81}{chapter.6}%
\contentsline {section}{\numberline {6.1}Możliwości rozwoju}{81}{section.6.1}%
\contentsline {chapter}{Bibliografia}{84}{chapter*.128}%
\contentsline {chapter}{Lista dodatkowych plików, uzupełniających tekst pracy}{87}{appendix*.129}%
\contentsline {chapter}{Spis rysunków}{90}{appendix*.130}%
\contentsline {chapter}{Spis tablic}{91}{appendix*.131}%
